# Day 24 Part 1

It's christmas eve and what better time to get cracking on advent of 
code day 24! Today's challenge is about hexagonal tiles! A very exciting
topic if you are part of the [church of the hexagon](https://www.youtube.com/watch?v=thOifuHs6eY)

Working with hexagonal tiles might seem like a very tricky problem but
thankfully, very smart people have already spent a lot of time
[thinking about this](https://www.redblobgames.com/grids/hexagons/) and
thanks to them we are going to solve this problem easily.

The problem to solve reads as follow:

Given a hexagonal grid, with black and white tile, and a list of instructions that tell which tile to flip from black to white or white to black, how
many black tiles are left on the board once all the instructions have been
carried out, assuming the board starts all white.

This problem sets up a number of tricky challenges: How do we navigate
our hexagonal grid? How to we process instructions? How to we count
our tiles?

In order to navigate the hexagonal grid we are going to use the 
[3d interpretation](https://www.redblobgames.com/grids/hexagons/#coordinates-cube)
of a hexagonal grid with each axis corresponding to
each axis of a hexagon. Each direction now corresponds to a vector of
magnitude √2, with a 0 coordinate. Which means we need a type for each of the
6 different directions, and we need 3d coordinates:

```
data Direction = W | SE | SW | E | NE | NW

-- first is X axis (E-W)
-- second in Y axis (NE-SW) and the
-- third is Z axis (NW-SE)
Coordinate : Type
Coordinate = Vect 3 Int

toCoord : Direction -> Coordinate
toCoord W  = [0, 1, -1] -- W-E axis stays 0, the other two change
toCoord E  = [0, -1, 1] -- W-E axis stays 0, the other two change
toCoord SW = [1, 0, -1] -- SW-NE axis stays 0, the other two change
toCoord NE = [-1, 0, 1] -- SW-NE axis stays 0, the other two change
toCoord SE = [1, -1, 0] -- SE-NW axis stays 0, the other two change
toCoord NW = [-1, 1, 0] -- SE-NW axis stays 0, the other two change
```

`toCoord` maps each direction to a vector that moves to the correct coordinate.
This allows us to answer the next question, how to process each series of
instructions. Instructions are a list of `Direction` in order to find out which
coordinate they point to in our hexagonal grid, we are going to map each 
`Direction` to its vector representation and then add all the vectors together
to find the final destination.

```
add : Coordinate -> Coordinate -> Coordinate
add = zipWith (+)

walkInstructions : List Direction -> Coordinate
walkInstructions = foldr (add . toCoord) [0,0,0]
```

Finally, in order to count or tiles, we could use a 3d board that is initially 
all white and then lookup and update the tiles that the instructions point to.
But this approach has the inconvenience that we need to offset all our coordinates
to avoid negative coordinates, a very error-prone and inelegant soltuion. Instead 
we are going to store all the coordinates
that are black in a `SortedSet` and the coordinates that are white will simply
be _absent_ from this set.

That means we start with an _empty_ `SortedSet`, which means all tiles are 
white. Then we _add_ the first coordinate we find to the set to indicate that
it turned black. If an instruction points to a coordinate already in the set
of black tiles, we remove to indicate that the tile turned white.We continue 
this process for each coordinate we need to flip,
adding coordinates that are missing from the set, or removing coordinates
from the set if they are already present.

```
Board : Type
Board = SortedSet Coordinate

flipCoord : Coordinate -> Board -> Board
flipCoord coord board = if contains coord board
                           then delete coord board
                           else insert coord board

solve : List (List Direction) -> Nat
solve instructions =
  let coordinates = map walkInstructions instructions
      flipped = foldr flipCoord empty coordinates
   in length (SortedSet.toList flipped)
```

To tie this all up, we count the number of element in the black set by
transforming it into a list and counting its number of elements.


# Part 2 Another cellular automaton

Who would have thought? Part 2 is another celllar automaton. This time we
are asked to run 100 iterations on the board that resulted from flipping
the files in part 1.

The rules to update the board are as follow:

- if a black tile is surrounded by 1 or 2 other black tiles they remain black, 
otherwise they flip to white
- if a white tile is surrounded by exactly 2 black tiles it flips to black, 
otherwise they remain white

As usual this is a function from tiles and neighboring tiles to the new tile 
state. However since we are not using a board with tiles but we are using a set of coordinates we cannot use our typical type signature 
`Tile -> List Tile -> Tile`. Instead we are going to count the number of
coordinates that are in the set of black tiles and use that to apply our rules.
The function is now instead `Tile -> Nat -> Tile`:

```
data Tile = Black | White

-- the Nat is the number of black neighbors
updateTile : Tile -> Nat -> Tile
updateTile Black 1 = Black
updateTile Black 2 = Black
updateTile Black _ = White
updateTile White 2 = Black
updateTile White _ = White
```

As for every other cellular automaton, we need an `iterate` function that will
perfom a number of state updates on the board of tiles:

```
iterate : Nat -> (f : a -> a) -> a -> a
iterate 0 f x = x
iterate (S k) f x = iterate k f (f x)
```

As for other cellular automaton we also need a function to update the state of 
the board, this function is simply a `Board -> Board` function that will
look at each black tile, each neighbors of each black tile and update them.
This works because white tiles that are suurounded by white tiles do not change
so we do not need to look at them:

```
updateBoardState : Board -> Board
updateBoardState board =
  let tiles : List (Coordinate, Tile, Nat)
      tiles = getTiles board -- get all the tiles and their neighbors
   in foldr (\(coord, tile, count) => case updateTile tile count of
                                           Black => insert coord
                                           White => id) empty tiles
```
Here you see that we update the board by creating a new `SortedSet` of
black tiles by looking at which tiles become black after beeing updated and
we ignore the white ones.

The crux of this function is hidden in the `getTiles` function which takes a
set of black tiles and returns a list of all the tiles that are subject to 
change. This includes all the black tiles, as well as all the white tiles
that are beside a black tile. Aka the neighboring white neighboring tiles
from our set of black tiles. Looking at the first line of the `let` statement
it indicates that this function return a list of triples. Each triple 
represents the coordinate of the tile, the color of the tile and the number
of black tiles around it.

```
getTiles : Board -> List (Coordinate, Tile, Nat)
getTiles board =
  let blackTiles : List (Coordinate, Tile)
      blackTiles = map (, Black) (SortedSet.toList board)

      neighbors : List Coordinate
      neighbors = nub . concat . map getNeighborCoord $ (SortedSet.toList board)

      whiteTiles : List (Coordinate, Tile)
      whiteTiles = map (, White) $ List.filter (not . (`contains` board)) neighbors

      allTiles : List (Coordinate, Tile)
      allTiles = blackTiles ++ whiteTiles
   in map (\(coord, tile) => (coord, tile, countBlackNeighbors coord board))
          (allTiles)
```

`blackTiles` is a list of all the tiles that are black, we obtain it by mapping
each coordinate in the set of black tiles to a pair containing the coordinate
and the tile `Black`.

`neighbors` is the list of all coordinates that are surrounding black tiles,
it is worth noting that if two black tiles are next to each other they
will both appear in this list. `getNeighborCoord` is a function 
`Coordinate -> List Coordinate` retuning
all the neighboring coordinates from our hexagonal grid.

In order to get all the _white_ tiles that are subbject to change we remove
all the tiles in the `neighbors` list that appear in our set of black tiles.
The remaining elements are the coordinates of the white tiles we're interested
in, we pair them up with the `White` tag.

Finally we concatenate the list of white and black tiles, and we map them with
their number of black neighbors. Counting the number of black tiles around
a coordinate is done by counting how many of the neighboring coordinates
are present in the set of black tiles.

```
countBlackNeighbors : Coordinate -> Board -> Nat
countBlackNeighbors coord board =
  count (`contains` board) (getNeighborCoord coord)
```

Now that we can update the state of the board using the rules given we will
modify our `solve` function to iterate a 100 times on this update function
using the result from part 1 as initial state:

```
solve : List (List Direction) -> Nat
solve instructions =
  let coordinates = map walkInstructions instructions
      tiles = foldr flipCoord empty coordinates
   in length (SortedSet.toList (iterate 100 updateBoardState tiles))
   -- the last line is the only one that changes
```

Running this takes a little while, probably becase of the `Set` operations,
but we get our final result after about a minute.

# A note about parsing

In those posts I avoid talking about parsing but today's problem is a great
opportunity to showcase the `Parser` library from `contrib`. It allows us to
parse lists of instructions very easily by defining a mapping between strings
and `Direction` constructors and then calling `parse`:

```
parseDirection : Parser Direction
parseDirection = map (const W) (char 'w')
             <|> map (const E) (char 'e')
             <|> map (const SE) (string "se")
             <|> map (const SW) (string "sw")
             <|> map (const NE) (string "ne")
             <|> map (const NW) (string "nw")

parseFile : String -> Either String (List (List Direction))
parseFile input =
  map (map fst) $ traverse (parse (some parseDirection <* eos)) (lines input)
```

Which is pretty painless compared to other days.
