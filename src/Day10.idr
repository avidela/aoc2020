module Main

import System
import System.File
import Data.Strings
import Data.List
import Data.List1

mapDifferences : List Nat -> List Nat
mapDifferences xs = zipWith (minus) xs (0 :: xs)

countDifferences : List Nat -> Nat
countDifferences xs =
      let diffs = mapDifferences xs
          ones = length $ filter (==1) diffs
          thrs = length $ filter (==3) diffs
       in ones * (S thrs)

parseFile : String -> Maybe (List Nat)
parseFile input = (sort <$> traverse parsePositive (lines input))

partial
solveFile : String -> IO ()
solveFile filename = do
  Right fileContent <- readFile filename
  let Just parsed = parseFile fileContent
  printLn $ countDifferences parsed

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just parsed = parseFile fileContent
  putStrLn ("valid password count: " ++ show (countDifferences parsed))
