# Day 13 part 1

Day 13 was quite a ride, the first part took me 30 seconds to solve in Excel
but an hour on stream. Part 2 was also interesting becase I learned a new trick!

In part 1 we are given a list of numbers and a target. The question is which
`n` number in this list is the closest to the target, after multiplitying by a number
`k` such that `k * n >= target`.

This is slightly different than being asked "which is the closest number modulo the
target" because we are only interested in numbers that are bigget than the target.

For this we are going to write a function that computes "the next number over the target"
for each candidate by dividing the target by the candidate and then adding one, this is
like rounding up the result of the division.

Then with this rounded up number we multiply by the candidate itself and find a number that
is slightly above the target. Doing this for each candidate and finding the smallest
difference with the target is the goal of today's exercise and can be sumarized with this
code:

```

-- find the multiplier that bring the candidate over the target
findMult : Int -> Int -> Int
findMult target n = (target `div` n) + 1

-- find the smallest value in a list of pairs, only using the first element of the pair
findSmallest : Ord a => List (a, b) -> Maybe (a, b)
findSmallest [] = Nothing
findSmallest [x] = Just x
findSmallest ((x, y) :: z :: xs) = map (\(a, b) => if a < x then (a, b)
                                                            else (x, y))
                                       (findSmallest (z :: xs))

solve : (Int, List Int) -> Maybe Int
solve (target, numbers) =
                     -- compute the difference with the value over the target
  let computed = map (\x => (x * findMult target x) - target) numbers in
                      -- find the smallest one and return their multiplication with the candidate
      uncurry (*) <$> findSmallest (zip computed numbers)
```

The last line ensure we actually answer the question which is returning the multiplication
of the smallest difference with the candidate itself.

# Part 2

Part 1 was quite easy, but Part 2 required quite a lot of reading and googling, so much
that I couldn't finish it on stream and it kept me up quite late.

The problem is stated as such:

The numbers represent bus cycles, bus number `7` leaves every `k * 7` minutes from its
original bus station. The bus number `19` every `k * 19` minutes. Our input contains a
list which has both `x` and numbers, the question is: What time `t` in the schedule has
the first bus in the list leave at time `t ≡ 0 mod 7`, or _t is congruent to 0 modulo 7_.
Then if the second bus has a cycle of `19` then `t+1 ≡ 0 mod 19`. Generally it must be true
for the `i`th number that `t+i ≡ 0 mod n_i`. Or equivalently `t ≡ n-i mod n_i`, we are going
to use this second interpretation for the solution.

This number can be found by applying the [Chinese reminder theorem](https://en.m.wikipedia.org/wiki/Chinese_remainder_theorem)
a theorem that states that there is one and only one `t` such that solves the following
system of equations

```
t ≡ a_0 mod n_0
t ≡ a_1 mod n_1
...
t ≡ a_i mod n_i
```

In order to find such `t` we need the [extended greatest common divisor](https://en.m.wikipedia.org/wiki/Extended_Euclidean_algorithm)
algorithm in order to find the inverse mod of our numbers. The wikipedia page for the chinese
reminder theorem provide a proof by construction that we can reuse to construct `t`:

```
N_i = map (product / n_i) ns) -- the product of all ns except for ni
M_i * N_i + m_i * n_i = 1 -- M_i is the `invMod` of n_i and N_i, found with Extended euclidean algorithm
t = ∑_(i=1 to k) a_i M_i N_i -- a_i is the modulo paired up with n_i
```

In our case our input is a list of numbers and some `x`

```
13, x, 17, 19
```

first of all we are going to find the corresponding `a_i` for each of those numbers. This is simply
`n_i - i`

```
ns : List (Maybe Integer) = [Just 13, Nothing, Just 17, Just 19]
withIndex : List (Integer, Integer) = catMaybes $ map (\(val, idx)=> (,cast idx) <$> val) (enumerate ns)
-- [(13, 0), (17,2), (19, 3)]
as : List Integer = map (uncurry (-)) withIndex
```

We now have our `ns` and our `as`, we just need the `Ms` and the `Ns`s. The `Ms` are derived from
the `ns`, by mapping each number of `ns` into the product of the `ns` and removing each element from
the product using division. The `Ns` are obtained by using `invMod` on `ns` and `Ms`

```
prod = product ns
Ms = map (\ni => product / ni)  ns
Ns = zipWith invMod Ms ns
```

The final result is the sum of the product of those three lists element-wis:

```
result = sum (zipWith3 a, b, c => a * b * c) as Ms Ns
final = result `mod` prod
```

We `mod` the final result, the chinese remainder theorem ensures uniqueness of the result modulo
the products of the `ns`. This solution is a bit ineficient since the value before the `mod` might
be really high and might overflow but since we are using `Integer` which are unbound by size then
we're okay.

# Conclusion

This post took me a long time to write because I spend a lot of time figuring out how the chinese
reminder theorem worked and understand how to use its parameters. In particular understanding how
the condition was that `t + i = 0 mod n_i` and not `t = 0 mod n_i` stopped me from finding the
answer quickly. Thankfully writing down the rules on paper and spending time trying out the numbers
by hand really help solifying my understanding. For once I am quite happy that I have learned some
new trick with the chinese reminders theorem even if that took a while to read through and understand.

Another, and more important reason why this post is so late is that my hands have been quite uncooperative
since day13 of AOC. I was writing this very blog post when the pain in my right hand became too much
to keep typing. I gave myself two days of rest with minimal typing and I now feel better. But not
completely recovered. Those issues are definitely due to typing for too long in suboptimal situations.
My laptop keyboard is comfy but repeating the same hand movements for hours days on end
is not good for your body.

