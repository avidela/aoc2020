module Main

import System
import System.File
import Data.SortedMap
import Data.Stream
import Data.Maybe
import Data.Strings
import Data.List1
import Data.List
import Debug.Trace

-- the key is the number that has been spoken
-- the value is its last index
CountState : Type
CountState = SortedMap Int Int

next : (prevValue, prevIndex : Int) -> CountState -> Int
next prevValue prevIndex state = maybe 0 (prevIndex -) (lookup prevValue state)

length : List1 a -> Nat
length (_ ::: xs) = S (length xs)

generateNumbers : CountState -> (prevValue, prevIndex: Int)  -> Stream Int
generateNumbers state prev prevIdx =
  let newNumber = next prev prevIdx state
      newState = insert prev prevIdx state in
      newNumber :: generateNumbers newState newNumber (1 + prevIdx)

enumerate : (Num i ) => List a -> List (a, i)
enumerate xs = fst $ foldr (\x, (ls, n) => ((x, n) :: ls, n + 1)) ([], 0) xs

buildState : List Int -> CountState
buildState xs = fromList (enumerate xs)

dropLast : List a -> List a
dropLast xs = take (length xs `minus` 1) xs

generateStream : List1 Int -> Stream Int
generateStream xs = let last = last xs
                        elems = dropLast (forget xs)
                        startingState = buildState (List.reverse elems)
                        index = (cast {to=Int} $ length elems)
                     in generateNumbers startingState last index

solve : List1 Int -> Nat -> Int
solve initial count = Stream.head $ drop (count `minus` (S $ length initial))
                                  $ generateStream initial



-- TODO: add `traversable` to List1
Traversable List1 where
  traverse g (head ::: xs) = [| g head ::: traverse g xs|]

parseFile : String -> Maybe (List1 Int)
parseFile = traverse parsePositive . split (==',')

partial
main : IO ()
main = do
  [_, input, length] <- getArgs
  let Just parsed = parseFile input
  let Just l = parsePositive length
  putStrLn ("Solution : " ++ show (solve parsed l))
