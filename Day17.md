# Day 17 Part 1

Today was probably my favorite problem to solve of this year's advent of code: A
3 dimensional game of life.

We've already had a cellular automata with [day 11](Day11.md) but this one is much
more familiar, it is plainly [conway's game of life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)
but in 3D. The great thing about this problem set is that is shows off the great benefits
about static typing and dependent typing. Namely: harder things don't get much harder once
you've implemented the fundamentals. In this case, generalising from 2D to 3D and from 3D
to 4D isn't much easier than implementing the game for n-dimension to begin with. Allowing
us to work with a much more powerful abstraction, which reveals the inner-workings of our
program much more explicitly in the type.

Let us start with the things that do not change with higher dimensions. The rules of
the game of life itself won't change. Each cell can be _active_ or _inactive_ and each
active cell remains active if it has 2 or 3 neighbors and becomes inactive otherwise. Each
Inactive cell becomes active when it is surrounded by 3 active cells. For this we are going
to use the same mechanism as on Day11 in order to get a list of surrounding cells and
then pass this list to a functio that will apply the rules of the game to a single cell.


```
getClosest : Board n a -> (curr, direction : Coordinate n) -> Maybe a
getClosest board curr direction =
  let newPosition = zipWith (+) curr direction in
      indexN newPosition board

getAdjacent : {n : Nat} -> (cordinates : Coordinate n) -> Board n a -> List a
getAdjacent coord board =
  let directions : List (Coordinate n)
      directions = filter (/= zero n) (genCoordN n) -- we show those two functions later
   in catMaybes $ map (getClosest board coord) directions

updateCube : Cube -> (neighbors : List Cube) -> Cube
updateCube Active n = let actives = count (== Active) n in
                          if actives == 2 || actives == 3
                             then Active
                             else Inactive
updateCube Inactive xs = if count (==Active) xs == 3 then Active
                                                     else Inactive
```

This program makes use of three types that we have not introduced yet: `Cube`, `Board` and
`Coordinate`. `Cube` is the type given to cells, they can either be active or inactive.
Board is the type representing an _n-dimensional_ board and as such is parameterized over
`n : Nat` representing the number of dimensions we're working with. `Coordinate` is a vector
of `n` indices where `n` is the number of dimensions in the space that the coordinate points
to.


```
data Cube = Active | Inactive

Eq Cube where
  Active == Active = True
  Inactive == Inactive = True
  _ == _ = False

Board : Nat -> Type -> Type
Board Z elem = elem
Board (S n) elem = List (Board n elem)

Coordinate : Nat -> Type
Coordinate n = Vect n Int
```

Here we use `Int` for our coordinates rather than `Nat` because of how we generate the list
of adjacent coordinates. This is the first example of n-dimensional function in which we
generate exactly the list of all adjacent coordinates irrespective of the dimension we're in:

```
genCoordN : (n : Nat) -> List (Coordinate n)
genCoordN 0 = [[]]
genCoordN (S k) = let rec = genCoordN k
                      n1 = map (-1::) rec
                      n2 = map ( 0::) rec
                      n3 = map ( 1::) rec in n1 ++ n2 ++ n3

zero : (n : Nat) -> Coordinate n
zero n = replicate n 0
```

`zero` returns the coordinate `0` for any n-dimensional space by returning a coordinate
filled with zeroes. Those two functions allow to generate all coordinates around a single
point and then remove the coordinate that does not point to an adjacent cell and then
use those coordinates to get the list of neighboring cells in order to perform our game
update. In both those cases notice we take `n`, the number of dimensions as an explicit
argument, and return a type that depends on it. That also makes the implementation of
those functions extremely easy since the types can direct us toward a correct implementation.

Surprisingly we're almost done with our game, we just need to implement the board update
function and the parsing. The board update function simply takes a board and updates
each cell using the rules of `updateCube`, while this should only be a `map` function
we are going to need to get access to the coordinate of each cell in order to find its
neighbors and update the cell correctly. For this we are not going to think too hard
and call `zipWithIndex : List a -> List (Int, a)` which maps each element with its index
as an `Int`

```
zipWithIndex : List a -> List (Int, a)
zipWithIndex xs = zip ([0 .. (cast $ length xs)]) xs

playGame : Board 3 Cube -> Board 3 Cube
playGame board =
  map (\(i, row) => map (\(j, line) => map (\(l, elem) =>
                   updateCube elem (getAdjacent [i,j,l] board) )
      (zipWithIndex line))
      (zipWithIndex row))
      (zipWithIndex board)
```

The repetition is due to the fact that we are _not_ abstracting over the number of
dimensions here. That is something that we will fix for _Part 2_!

Before we jump onto part 2 I want to touch on the parsing. In this challenge we are given
a square of 8 by 8 cells, this square will be the starting state of our game of life.
Additionally we are only required to run 6 iteration of the game, this means we only need
to work in a 20 by 20 grid. Since we work in 3D we are looking at a 20x20x20 cube.
Technically since our initial state is a plane in 3D we can get away with only using a
20x20x13 volume but that makes the math slightly more complicated in a way that isn't
helpful.

Given that situation, parsing our data means two things, we need to convert the given
string into a 2d board, and then we need to embed that 2d board into a bigger 3D cube.
Those steps are nice to present in terms of type signature, the first level of parsing
goes from `String` to `Board 2 Cube`, the second level of parsing goes from
`Board 2 Cube` to `Board 3 Cube`. And itermediate step is to center the 8x8 board into
a 20x20 board with padding of inactive cubes. This step is simply `Board 2 Cube` to
`Board 2 Cube`. Combining those steps allows to create a cube of 20x20 with our starting
board embedded in the middle of it. Unfortunately, the function signatures do not
particularly reflect how the board is being moves and sandwiched around.

- `String -> Maybe (Board 2)`
- `Board 2 -> Board 2`
- `Board 2 -> Board 3`

This is something we will re-visit for part 2 as well.

# Part 2

Part 2 asks to run the game this time in 4 dimensions, this is the perfect opportunity
to fix and update the bits that rely on our 3d implementation.

Previously we've been using lists of lists to record and manipulate our 3d space,
however this approach has the unfortunate property that we cannot ensure in the
type that each list spans the entire "width" of our space. If our space is a
5 x 5 square and our type is `List (List Cube)`, there is nothing stopping our
list from being malformed as such:

```
[ [ x, x, x, x, x ]
, [ x ]
, []
, [ x, x, x, x, x, x]
]
```

The first row is well formed, the second row is malformed because it only has one
element, the thrid row is also malformed because its empty, the last row is also
malformed because its too long. Finally, the list of rows is malformed because its
only got 4 rows instead of 5. Despite this, this value would have the type
`List (List a)` which is what we decided our 5x5 square should be, but clearly
our `List` type is not enough. Instead we are going to use `Vect` and work in
cubes. A 5x5 square can be represented with `Vect 5 (Vect 5 a)` and a 10x10x10
cube with `Vect 10 (Vect 10 (Vect 10 a))`. To make this change we are going to update
the `Board` type with an additional index, indicating the size of each side of the
board:

```
Board : Nat -> Nat -> Type -> Type
Board s Z elem = elem
Board s (S d) elem = Vect s (Board s d elem)
```

Now `Board 5 2 a` represents a 5x5 square board, `Board 200 5` represents a
5-dimensional cube with a 200-long side, or a 200x200x200x200x200 sided cube.

In our case we're interested in a 20-sided, 4-dimensional cube. 20-sided because
we need at least 6 squares around our 8x8 input so we can run 6 iterations of
our simulation, and 4-dimensional because that's what the challenge asks us. This
is simply a `Board 20 4 Cube` value.

We now need to update our parsing functions, recall at the end of part 1 we had
to map from an 8x8 board to a 20x20 board and then embed it into a 20x20x20 cube,
but the types hardly reflected this. Now we need to add one more step and embed
our 20x20x20 cube into a 20x20x20x20 hypercube. For this we are going to perform
the following steps:

- Center our 8x8 board in a 20x20 board : `Board 8 2 Cube -> Board 20 2 Cube`
- Embed this 2d plane in a 3d cube: `Board 20 2 Cube -> Board 20 3 Cube`
- Embed this 3d plane in a 3d hypercube: `Board 20 3 Cube -> Board 20 4 Cube`

With our new board type we can now clearly see how the size of the space changes
we also see that we only need a single _generic_ `embed` function that wraps
a plane into a cube using empty layers. Let us start with this one:

```
embed : (n : Nat) -> {x, y : Nat} -> Board (x + S y) n Cube -> Board (x + S y) (S n) Cube
embed n board = let postfix = Vect.replicate y (emptyCube (x + S y) n)
                    prefixx = Vect.replicate x (emptyCube (x + S y) n)
                 in prefixx ++ (board :: postfix)
```

`embed` is parameterised over a number of arguments, `n` is the number of dimensions
of the plane and `S n` the number of dimensions of the cube that embed the plane.
`x` is the number of layers below the plane and `y` the number of layers above it.
Notice that `x + S y` needs to be the size of the size of the plane that we want
to embed. This way, embedding a 10x10 plane will always result in a 10x10x10 cube.

Changing our board type from `List` to `Vect` only require a few changes for
centering the original plane. The biggest difference is that now the bounds are
checked by the compiler:

```
center : Board 8 2 Cube -> Board 20 2 Cube
center xs = let inner = map (\vs => replicate 6 Inactive ++ vs ++ replicate 6 Inactive) xs
                emptyRow = emptyCube 20 1
             in replicate 6 emptyRow ++ inner ++ replicate 6 emptyRow
```

We can now put everything together in order to setup our input properly by
centering it first and embedding it twice:

```
processInput : Board 8 2 Cube -> Board 20 4 Cube
processInput xs =
  let center = center xs -- center the square
      emb3 = embed {x=9} {y=10} 2 center -- embed in 3D
   in embed 3 {x=9} {y=10} emb3 -- embed in 4D
```

Again, notice how the type signature tells us explicitly that we are going from an
8x8 plane to a 20x20x20x20 hypercube!

Now we also need to change our `playGame` function, it is now parametric over
the dimensions of the game : `Board s d Cube -> Board s d Cube`. Our previous
implementation used 3 nested call to `zipWithIndex` which was slightly inefficient
and a bit inelegant here because we would need to call `zipWithIndex` as many
times as there are dimensions in our game. Instead we are going to _pre-bake_ the
coordinate with each cell of our board and map across the board to find the
neighbors. Our idealised version of `playGame` looks something like this:

```
playGame : Board s d (Coordinate d, Cube) -> Board s d (Coordinate d, Cube)
playGame board = map (\coordinate, cell) => (coordinate, update cell)) board
```

We cannot use `map` here because we need to write a n-dimensional `map` first,
but the final version is very close to this ideal version, let us start
with the n-dimensional `map`. And n-dimensional map looks at the current
dimension, and if we are working in dimension `0` it means we are done looking
at nested vectors and are looking at a value, therefore we can simply apply
our function to the value we found. Otherwise, if our dimension is greater than
0, it means we're looking at a vector of vectors of smaller dimensions. In that
case we simply map across each lower dimension vector using a recursive call
to our n-dimensional map, except this time it works at a dimmension 1 lower than
the current one.

```
mapBoard : (d : Nat) -> (f : a -> b) -> Board s d a -> Board s d b
mapBoard Z f x = f x
mapBoard (S k) f x = map (mapBoard k f) x
```

Going back to our `playGame` function, we can replace our idealised version that
uses `map` by one that uses `mapBoard`. Additionally, we can reuse our previous
update function to make the game work just like before:

```
playGame :  Board s d (Coordinate d, Cube) -> Board s d (Coordinate d, Cube)
playGame board =
  mapBoard d (\(coord, elem) =>
               (coord, updateCube elem (getAdjacent coord board))) board
```

We had to slightly change `getAdjacent` to work on boards that contain
elements paired with their coordinate `(Coordinate d, Cube)` but the changes are
minimal. I will also note that I removed some of the implicit argument annotations
to make the code easier on the eyes. Indeed the source code has a couple more
implicit-argment passing in order to please the type-checker.

Earlier I said that we are pairing up each element with their coordinates,
again this process should work in n-dimensions. Such a function would take
a `Board s d a` in argument and return a `Board s d (Coordinate d, a)` where
`s` is the size of the edge of the space, `d` is the number of dimensions and
`a` is the content of the board. `Coordinate d` is a vector of coordinate that
points to a cell in our n-dimensional space. The strategy for implementing it is
the same as other n-dimensional functions: First inspect the dimension, if it's
`0` then do something to the value, otherwise perform a recursive call. For this
function, if the dimension is `0` that means our coordinates are also 0-dimensional,
aka a vector of size `0`, of which there is only one : `[]`. Therefore we simply
pair up our value with an empty vector.

If there are more dimensions under the one we're looking at we need to zip
our current vector with an index using the previous `zipWithIndex` and recursively
assign an index to the lower-dimension cubes using `generateIndices` on them.
For this we re-use our n-dimensional `mapBoard` in order to update all the
lower-dimensional cubes with their respective index.

```
generateIndices : (d : Nat) -> Board s d a -> Board s d (Coordinate d, a)
generateIndices 0 = ([],)
generateIndices (S k) =
   map (\(i, b) => mkIndex k i (generateIndices k b)) . zipWithIndex
  where
    mkIndex : (d : Nat) -> Int -> Board s d (Vect k Int, a)
                               -> Board s d (Vect (S k) Int, a)
    mkIndex d i = mapBoard d (\(vs, a) => (i :: vs, a))
```

Finally the last change to perform is to count how many `Active` cubes are in an
n-dimensional board. For this we again inspect the dimension of our board. If we
are working in dimension `0` this means we are looking at a plain `Cube`, if that
is the case we return `1` if it is active or `0` if it is `Inactive`.
Otherwise, if our dimension is `S n` we count the number of `Active` cubes by
performing a recursive call on the lower dimension cubes and summing up their total.

```
countActives : {d : Nat} -> Board s d Cube -> Nat
countActives {d=Z} x = if x == Active then 1 else 0
countActives {d=S k} vects =
  foldr (\rec => (+ countActives rec)) Z vects
```

We can now put all this together by updating our main function a little bit
to use the new n-dimensional functions and we are set:

```
let result = countActives
           $ mapBoard 4 snd
           $ iterate 6 playGame
           $ generateIndices 4
           $ processInput parsed
```

Again I've removed the implicit arguments to make the control flow more clear.
First we process the input to embed it into a 4D cube of size 20, then we
generate the index for each coordinate of this 4D cube, then we iterate 6 times
using our `playGame` function. and finally we get the active cells and count them.


