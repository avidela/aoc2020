module Main

import System
import System.File
import Data.Strings
import Data.List
import Data.List1
import Data.SortedSet

push : a -> List a -> List a
push x = (++ [x])

pop : List a -> Maybe (a, List a)
pop [] = Nothing
pop (x :: xs) = Just (x, xs)

Decks : Type
Decks = (List Nat, List Nat)

data Player = P1 | P2

playGame : Decks -> SortedSet Decks -> Maybe (Player, List Nat)
playGame now@(x, y) history with (contains now history, pop x, pop y)
  playGame now@(x, y) history | (True, _, _)  = Just (P1, x)
  playGame now@(x, y) history | (False, Nothing, Nothing) = Nothing
  playGame now@(x, y) history | (False, Just z, Nothing) = Just (P1, x)
  playGame now@(x, y) history | (False, Nothing, Just z) = Just (P2, y)
  playGame now@(x, y) history | (False, Just (e1, q1), Just (e2, q2)) =
      if e1 <= length q1 && e2 <= length q2
         then playGame (take e1 q1, take e2 q2) empty
            >>= pickWinner e1 e2 (q1, q2) (insert now history)
         else normalGame e1 e2 (q1, q2) (insert now history)
  where
    normalGame : (e1, e2 : Nat) -> Decks -> SortedSet Decks -> Maybe (Player, List Nat)
    normalGame e1 e2 (q1, q2) history =
      if e1 > e2
         then playGame (push e2 (push e1 q1), q2) history
         else playGame (q1, push e1 (push e2 q2)) history

    pickWinner : (e1, e2 : Nat) -> Decks -> SortedSet Decks
                 -> (Player, List Nat) -> Maybe (Player, List Nat)
    pickWinner e1 e2 (q1, q2) history (P1, _) =
      playGame (push e2 (push e1 q1), q2) history
    pickWinner e1 e2 (q1, q2) history (P2, _) =
      playGame (q1, push e1 (push e2 q2)) history

solve : List Nat -> Nat
solve xs = sum $ zipWith (*) (reverse xs) [1 .. S (length xs)]

parseFile : String -> Maybe (List Nat, List Nat)
parseFile input = let lines = lines input
                      ((_ :: p1) ::: [(_ :: p2)]) = split (=="") lines
                        | _ => Nothing
                   in do cards1 <- traverse parsePositive p1
                         cards2 <- traverse parsePositive p2
                         pure (cards1, cards2)

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
    | Left err => printLn err
  let Just parsed = parseFile fileContent
    | _ => putStrLn "cannot parse file"
  putStrLn ("Solution : " ++ show (solve . snd <$> playGame parsed empty))
