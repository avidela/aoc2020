module Main

import System
import System.File
import Data.String.Parser
import Data.Strings
import Data.Either
import Data.List
import Data.Vect
import Data.Bool
import Data.SortedMap
import Data.SortedSet

count : Foldable f => (a -> Bool) -> f a -> Nat
count g = foldr (\elem => if g elem then S else id) Z

data Tile = Black | White

data Direction = W | SE | SW | E | NE | NW

Show Direction where
  show W = "W"
  show SE = "SE"
  show SW = "SW"
  show E = "E"
  show NE = "NE"
  show NW = "NW"

-- first is X axis, second in Y axis (NE-SW) and third is Z axis with (NW-SE)
Coordinate : Type
Coordinate = Vect 3 Int

toCoord : Direction -> Coordinate
toCoord W  = [0, 1, -1]
toCoord SE = [1, -1, 0]
toCoord SW = [1, 0, -1]
toCoord E  = [0, -1, 1]
toCoord NE = [-1, 0, 1]
toCoord NW = [-1, 1, 0]

add : Coordinate -> Coordinate -> Coordinate
add = zipWith (+)

-- true means result is black, false means result is white
updateTile : Tile -> Nat -> Tile
updateTile Black 1 = Black
updateTile Black 2 = Black
updateTile Black _ = White
updateTile White 2 = Black
updateTile White _ = White

getNeighborCoord : Coordinate -> List Coordinate
getNeighborCoord xs = map (add xs)
    [[0, 1, -1]
    ,[1, -1, 0]
    ,[1, 0, -1]
    ,[0, -1, 1]
    ,[-1, 0, 1]
    ,[-1, 1, 0]]

iterate : Nat -> (f : a -> a) -> a -> a
iterate 0 f x = x
iterate (S k) f x = iterate k f (f x)

Board : Type
Board = SortedSet Coordinate

countBlackNeighbors : Coordinate -> Board -> Nat
countBlackNeighbors coord board =
  count (`contains` board) (getNeighborCoord coord)

getTiles : Board -> List (Coordinate, Tile, Nat)
getTiles board =
  let blackTiles : List (Coordinate, Tile)
      blackTiles = map (, Black) (SortedSet.toList board)

      neighbors : List Coordinate
      neighbors = nub . concat . map getNeighborCoord $ (SortedSet.toList board)

      whiteTiles : List (Coordinate, Tile)
      whiteTiles = map (, White) $ List.filter (not . (`contains` board)) neighbors

      allTiles : List (Coordinate, Tile)
      allTiles = blackTiles ++ whiteTiles
   in map (\(coord, tile) => (coord, tile, countBlackNeighbors coord board))
          (allTiles)

updateBoardState : Board -> Board
updateBoardState board =
  let tiles : List (Coordinate, Tile, Nat)
      tiles = getTiles board
   in foldr (\(coord, tile, count) => case updateTile tile count of
                                           Black => insert coord
                                           White => id) empty tiles

flipCoord : Coordinate -> Board -> Board
flipCoord coord board = if contains coord board
                           then delete coord board
                           else insert coord board

walkInstructions : List Direction -> Coordinate
walkInstructions = foldr (add . toCoord) [0,0,0]

solve : List (List Direction) -> Nat
solve instructions =
  let coordinates = map walkInstructions instructions
      tiles = foldr flipCoord empty coordinates
   in List.length (SortedSet.toList (iterate 100 updateBoardState tiles))

parseDirection : Parser Direction
parseDirection = map (const W) (char 'w')
             <|> map (const E) (char 'e')
             <|> map (const SE) (string "se")
             <|> map (const SW) (string "sw")
             <|> map (const NE) (string "ne")
             <|> map (const NW) (string "nw")

parseFile : String -> Either String (List (List Direction))
parseFile input =
  map (map fst) $ traverse (parse (some parseDirection <* eos)) (lines input)

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Right parsed = parseFile fileContent
    | Left err => printLn err
  putStrLn ("Solution : " ++ show (solve parsed))
