module Main

import System
import System.File
import Data.Vect
import Data.Nat
import Data.SortedMap
import Data.Strings
import Data.List1
import Debug.Trace

Mask : Type
Mask = Vect 36 (Maybe Bool)

Binary : Type
Binary = Vect 36 Bool

interface BinaryConversion t where
  fromBin : Binary -> t
  toBin : t -> Binary

infixl 4 ^

(^) : (Eq a, Num a, Neg a) => a -> a -> a
(^) x y = if y == 0 then 1 else x * (x ^ (y - 1))

toBinary : Int -> (size : Nat) -> Vect size Bool
toBinary x Z = []
toBinary x (S size) = let i : Int= cast (S size) in (x `mod` 2 == 1) :: toBinary (x `div` 2) size

BinaryConversion Int where
  toBin x = reverse $ toBinary x 36
  fromBin b =
    let indices : Vect 36 Int = (reverse $ map (cast {to=Int} . finToNat) range)
        v = zipWith (\b, idx => if b then 2 ^ idx else 0) b indices
     in sum v

BinaryConversion Nat where
  toBin k = toBin {t=Int} (cast k)
  fromBin b =
    let indices : Vect 36 Nat = (reverse $ map (finToNat) range)
        v = zipWith (\b, idx => if b then 2 `power` idx else 0) b indices
     in sum v

generateFloating : Vect n (Maybe Bool) -> List (Vect n Bool)
generateFloating [] = [[]]
generateFloating (Nothing :: xs) = let rec = generateFloating xs in
                                       map (True ::) rec ++ map (False ::) rec
generateFloating ((Just x) :: xs) = map (x :: ) (generateFloating xs)

applyMask : Mask -> Binary -> List Binary
applyMask mask bin =
  let masked = zipWith (\m, b => map (\case True => True; False => b) m) mask bin
   in generateFloating masked

data Instruction = NewMask Mask | MemOp Int Binary

solve : List Instruction -> Nat
solve instructions = foldr (\x, acc => acc + fromBin x) 0
                           (runProgram instructions (empty, replicate 36 Nothing))
  where
    runProgram : List Instruction -> (SortedMap Int Binary, Mask)
              -> SortedMap Int Binary
    runProgram [] x = fst x
    runProgram ((NewMask m) :: xs) (x, z) = runProgram xs (x, m)
    runProgram ((MemOp addr val) :: is) (mem, mask) =
      let addresses : List Binary = applyMask mask (toBin addr )
          newMemory = foldr (\addr => insert (fromBin addr) val) mem addresses in
          runProgram is (newMemory, mask)

parseFile : String -> Maybe (List Instruction)
parseFile input = traverse parseLine (lines input)
  where
    parseMask : String -> Maybe Mask
    parseMask x = map (\case '0' => Just False
                             '1' => Just True
                             _ => Nothing) <$> toVect 36 (unpack x)

    parseLine : String -> Maybe Instruction
    parseLine line =
      case words line of
           ["mask", _, vec] => trace "parsing mask" $ NewMask <$> parseMask vec
           [mem, _, val] => let ("mem" ::: (n :: _)) = split (\c => c == '[' || c == ']') mem
                                   | _ => trace "could not parse memory instruction" Nothing
                             in [| MemOp (parsePositive n) (toBin {t=Int} <$> parsePositive val)|]

           _ => trace ("could not parse line" ++ show line)  Nothing


partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
    | _ => putStrLn "could not read File"
  let Just parsed = parseFile fileContent
    | _ => putStrLn "could not parse File"
  putStrLn ("Solution : " ++ show (solve parsed))
