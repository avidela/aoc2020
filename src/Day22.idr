module Main

import System
import System.File
import Data.Strings
import Data.List
import Data.List1

push : a -> List a -> List a
push x = (++ [x])

pop : List a -> Maybe (a, List a)
pop [] = Nothing
pop (x :: xs) = Just (x, xs)

Decks : Type
Decks = (List Nat, List Nat)

playGame : Decks -> Maybe (List Nat)
playGame (p1, p2) with (pop p1, pop p2)
  playGame (p1, p2) | (Nothing, Nothing) = Nothing
  playGame (p1, p2) | (Nothing, (Just z)) = Just p2
  playGame (p1, p2) | ((Just z), Nothing) = Just p1
  playGame (p1, p2) | ((Just (e1, q1)), (Just (e2,q2))) =
      if e1 > e2 then playGame (push e2 (push e1 q1), q2)
                 else playGame (q1, push e1 (push e2 q2))

solve : List Nat -> Nat
solve xs = sum $ zipWith (*) (reverse xs) [1 .. S (length xs)]

parseFile : String -> Maybe (List Nat, List Nat)
parseFile input = let lines = lines input
                      ((_ :: p1) ::: [(_ :: p2)]) = split (=="") lines
                        | _ => Nothing
                   in do cards1 <- traverse parsePositive p1
                         cards2 <- traverse parsePositive p2
                         pure (cards1, cards2)


partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
    | Left err => printLn err
  let Just parsed = parseFile fileContent
    | _ => putStrLn "cannot parse file"
  putStrLn ("Solution : " ++ show (solve <$> playGame parsed))
