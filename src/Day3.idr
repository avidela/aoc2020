module Main

import Data.Stream
import Data.List
import Data.Strings
import System
import System.File

Tree : Type
Tree = Bool

isTree : (Stream Tree) -> (col : Nat) -> Bool
isTree treeStream index = Stream.head $ drop index treeStream

enumerate : List a -> List (Nat, a)
enumerate ls = zip [0 .. (length ls)] ls

count : (predicate : a -> Bool) -> List a -> Nat
count p = length . (List.filter p)

getTree : List (Stream Tree) -> List Bool
getTree rows = map (\(index, row) => isTree row (index * 3)) (enumerate rows)

countTree : List (Stream Tree) -> Nat
countTree trees = count (== True) (getTree trees)

maybeCycle : List a -> Maybe (Stream a)
maybeCycle [] = Nothing
maybeCycle (x :: xs) = Just $ cycle (x :: xs)

convert : String -> Maybe (List (Stream Tree))
convert input = let rows = map unpack (lines input )
                    toTrees = map (map (== '#')) rows
                 in traverse (maybeCycle) toTrees

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just parsed = convert fileContent
  putStrLn ("number of trees encountered : " ++ show (countTree parsed))
