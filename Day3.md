# Day 3 Part 1

## The problem

This time we are given a string that represents a 2 dimensional space. This string
is composed of `.` and `#` which represent "empty space" and "tree" respectively. We are asked to
return the number of trees we encounter after going through the space described by our input string.

Just to get a better idea of what the input looks like here is the example provided:

```
..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
```

The steps we are asked to perform is "move to the right 3 times and one down" repeatedly until
we reach the bottom of the input.

```
S.##.......
#..E#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
```

In the previos example I've marked with `S` the position where we start and `E` the position where
we end, after 1 step.

The instructions tell us that if we reach the edge of the screen we should keep going as if the pattern
repeated infinitely towars the right:

```
..##.........##.........##........ etc...
#...#...#..#...#...#..#...#...#..# etc...
.#....#..#..#....#..#..#....#..#.. etc...
..#.#...#.#..#.#...#.#..#.#...#.#. etc...
.#...##..#..#...##..#..#...##..#.. etc...
..#.##.......#.##.......#.##...... etc...
.#.#.#....#.#.#.#....#.#.#.#....#. etc...
.#........#.#........#.#........#. etc...
#.##...#...#.##...#...#.##...#...# etc...
#...##....##...##....##...##....## etc...
.#..#...#.#.#..#...#.#.#..#...#.#. etc...
```

This allows us to keep going right 3 times and down once without worrying about reaching the edge.

## Toward a solution

This kind of problem is most often solved using [modulo arithmetic]() in order to simulate the infinite
repetition of the pattern toward the right. The trick is to see the string as a 2-dimensional array and
the process as "updating a coordinate" in this 2-dimensional array. With the subtlety that the index
selecting the column is always computed "modulo the width" of the array.

That is, if the array is 8 elements wide, and we need to access the 11th element, we do `11 mod 8` or
`11 % 8` and get `3` as a result. which means, the 11th element of our pattern is the same as element
3 of the array.

This, however, is boring, it requires a lot of manual tracking of the two indices, and requires to update
them correctly before going onto the next step, so today we're doing things a bit differently.

Anotherway to see our input is as a list of infinite streams:


```
[ ..##.........##.........##........ etc...
, #...#...#..#...#...#..#...#...#..# etc...
, .#....#..#..#....#..#..#....#..#.. etc...
, ..#.#...#.#..#.#...#.#..#.#...#.#. etc...
, .#...##..#..#...##..#..#...##..#.. etc...
, ..#.##.......#.##.......#.##...... etc...
, .#.#.#....#.#.#.#....#.#.#.#....#. etc...
, .#........#.#........#.#........#. etc...
, #.##...#...#.##...#...#.##...#...# etc...
, #...##....##...##....##...##....## etc...
, .#..#...#.#.#..#...#.#.#..#...#.#. etc... ]
```

And going "3 right and 1 down" means "get the (n * 3)th element of the `nth` infinite stream".

In terms of type signature we need something like this:

```
getTree : List (Stream Tree) -> List Bool
```

This function would take our input as a list of infinite streams and return `True` if we've met a tree
at the corresponding coordinate and `False` if we have not. It also uses a stream of `Tree` instead of
another pre-existing type. This is because I have created a type alias for booleans named `Tree` in
order to help understand what the data represents. The Boolean in the streams are `Trees` and the
booleans in the return list are "true or false" statements about the presence of a tree or not.

Type aliases in idris are plain functions that return a type, in our case `Bool`

```
Tree : Type
Tree = Bool
```

In addition we need to find a way to get if a position in the stream is a tree or not. That's the purpose
of the function `isTree`

```
isTree : Stream Tree -> (col : Nat) -> Bool
isTree treeStream index = Stream.head $ drop index treeStream
```

Its implementation is pretty straight forward, you will notice we are using `Stream.head` instead of `head`
tbat is because there is another `head` functions for lists and we need to disambiguate.

Using this function we can implement `getTree` by pairing up each element of our input with its index
and calling `isTree` with the index multiplied by `3`. In order to pair up each element we are going
to use an auxilary function `enumerate`:

```
enumerate : List a -> List (Nat, a)
enumerate ls = zip [0 .. (length ls)] ls

getTree : List (Stream Tree) -> List Bool
getTree rows = map (\(index, row) => isTree row (index * 3)) (enumerate rows)
```

The final step is to count the number of `True` we have in the resulting array and call our function with
our input. For this we can reuse the same `count` function we defined in yesterday's challenge

```
countTree : List (Stream Tree) -> Nat
                  -- this can also be understood as `count id (getTree trees)`
                  --      v
countTree trees = count (== True) (getTree trees)
```

Parsing the data requires us to split each line and then create a stream using the function `cycle` on
each row. `cycle` creates an infinite stream by endlessly repeating the same list passed in argument. The
only quirk about it is that it need the list to be non-empty. In order to ensure this I've written a short
helper function that creates a stream and returns `Nothing` if the list is empty:

```
maybeCycle : List a -> Maybe (Stream a)
maybeCycle [] = Nothing
maybeCycle (x :: xs) = Just $ cycle (x :: xs)

convert : String -> Maybe (List (Stream Tree))
convert input = let rows = map unpack (lines input )
                    toTrees = map (map (== '#')) rows
                 in traverse (maybeCycle) toTrees
```

As usual the program input is parsed as a command line argument but you can also just paste it inside
of your file.


## Part 2

For part 2 we are require to do 2 more things:

- Like before, count the number of trees encountered using a series of steps, except with different
step sizes
- Once we found the number of trees each of those strategies encounter us we need to multply all those
numbers are use this as our output

The steps that we are asked to use are

- 1 right, 1 down
- 3 right, 1 down
- 5 right, 1 down
- 7 right, 1 down
- 1 right, 2 down

Each of those steps define a new "slope" that we are going to use to count the number of trees, our
counting function must then be parameterised over a sort of "SlopeStrategy" that we will use to traverse
our list:


```
countTree : (slope : SlopeStrategy) -> List (Stream Tree) -> Nat
```

If we look at our existing `getTree` function we notice that the "3 right, 1 down" behaviour is handled
by the function that maps across our list `\(index, row) => isTree row (index * 3)`

That means that `SlopeStrategy` are simply functions that take into argument an index, an infinite stream
and tell us if we find a tree at that index inside this stream:

```
SlopeStrategy : Type
SlopeStrategy = (Nat, Stream Tree) -> Bool
```

The `1 down` behaviour is handled by the fact that the map goes down line by line. But how to represent a
skipping for the last strategy which is "1 right, 2 down"? One way to do this would be keep the `map`
behaviour but do some pre-processing on the list before passing it to `countTree`. However, this would be
extremely inelegant since it would mean the argument would need to be changed depending on the strategy
(Try it! You will see how annoying that is). Another way to approach this is to notice that a line that
we skip is indistiguishable from a line that always returns `False` (this might require a bit of
thinking about it in the shower, but since lines are only relevant when they return True, a line
that always returns false is line a line that doesn't exist). Therefore, the last strategy could
be implemented as the following procedure:

- if the index is even, then return the tree that we find at the given index
- if the index is odd, then return false (skip the line)

as a function it is implemented as an `if-statement`

```
slope12 : SlopeStrategy
slope12 (index, row) = if isEven index then isTree row index
                                       else False
```

We haven't implemented `countTree` yet but I'm sure you can figure out what's gonna happen, we are going
to map across our list of streams using the given strategy and then sum up the number `True` we found

```
countTree : List (Stream Tree) ->(slope : SlopeStrategy) ->  Nat
countTree trees slope =
  let foundTrees = map slope (enumerate trees)
   in count id foundTrees
```

The last bit consists in multipliying all the results together from a list of strategies. First we are
going to generate all the strategies that don't skip with a function called `generateSlope` and then put
all our strategies in a list `slopes`.


```
generateSlope : (run : Nat) -> SlopeStrategy
generateSlope run (index, row) = isTree row (index * run)

slopes : List SlopeStrategy
slopes = [generateSlope 1, generateSlope 3, generateSlope 5, generateSlope 7, slope12]
```

Now that we have this we can map across our list of strategies to extract the number of trees that
each strategy encounters for our input. And then use a `fold` in order to multiplity each of them.

```
multiplyAll : List (Stream Tree) -> Nat
multiplyAll trees = foldl (*) 1 (map (countTree trees) slopes)
```

And that is it! Now we just call `multiplyAll` with our input and we're set.

## Conclusion

I purposely tried to do things a bit differently than expected, overall I'm happy with how this
turned out, I'm a bit annoyed at the amount of helper functions I had to defined but the core logic
remained simple and small. Which is a property that you often find in good programs. Just like Day2
you can find the video version of this over at [twitch](https://www.twitch.tv/identitygs/videos)

As usual, don't hesitate to open an issue if you have any question or request!
