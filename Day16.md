# Day 16 Part 1

Day 16 part 1 was a breeze but part two had me stumped for a bit. Part one requires
us to compute which tickets are _invalid_ according to a set of rules that is given
in the input.

The rules have a very consistent format (though a bit annoying to parse), the name
of the rule is given first, then a first range, then a second range. For a number
to be valid it has to be within either of the two ranges of the rule. We
start by implementing a `isValid` function that checks for a range. Then we
use it to extract which numbers are invalid

```
Range : Type
Range = (Int, Int)

isValid : Int -> Range -> Bool
isValid x (y, z) = y <= x && x <= z

getInvalid : List Int -> List Range -> List Int
getInvalid tickets ranges = filter (\t => not $ any (isValid t) ranges) tickets
```

This requires us to extract the list of all ranges as well as the list of
all numbers. We can easily do that by flattening the input given after we parsed
it:


```
Ticket : Nat -> Type
Ticket n = Vect n Int

Rules : Nat -> Type
Rules n = Vect n (String, (Range, Range))

record Input (n : Nat) where
  constructor MkInput
  rules : Rules n String
  myTicket : Ticket n
  nearby : List (Ticket n)
```

Here we have an index `n` which represents the number of rules we expect to match
each ticket also has an index `n` ensuring that the ticket has as many fields as
there are rules to match.

Given this input we need to flatten all the rules into a list of range, flatten
all the tickets into a list of numbers and then sum the list of invalid numbers
we find in order to get our answer:

```
solve : {n : Nat} -> Input n -> Int
solve (MkInput rules myTicket nearby) =
  let flatRules : List Range = concatMap (\(_, (x, y)) => [x,y]) (toList rules)
      numbers : List Int = concat $ map toList nearby
   in sum $ getInvalid numbers flatRules
```

The first line of the function flattens the ranges by ignoring the label of the
rule and mapping the ranges in a two-element list. This results in a list of
two-elements list that we then flatten. We proceed similarly for `number` though
the process is easier since we are not burdened with the rule label.

And we're done! Onto part 2!

# Part 2

This part gave me lots of trouble but here are the key things to notice that allowed
me to solve it:

- We need to find which rules correspond to which field
- Some tickets are invalid, thanks to part1 we can detect those
- With the remaining tickets we can build a list of "candidates rules" for each field
- Here is the kicker: one of the field has only 1 rule that matches all the tickets, another one has only 2
, etc until the last one which is valid for all rules.

Given this information we can find which field has only one candidate rule. Assign it,
remove it from the list of candidates for the other fields and repeat. Since the
field with 2 candidates will have the previous field removed, it will only have 1
candidate left. We can then repeat the same process by assigning the rule to this
second field and in turn removing it from the other lists of candidates until
all fields have been assigned.

Once all the fields have been assigned a unique rule we can find the ones that
start with "departure" get the field values from our ticket and multiply them
to get our final solution:

```
getSolution : List (String, Int) -> Int
getSolution = product . map snd . filter (isPrefixOf "departure" . fst)
```

Here the first argument is a mapping between each field of our ticket and the name
of the rule it corresponds to as a string. We see that we filter using the first
element of each pair and then take the product of the second elements.

In order to get to this function we need to compute which rules goes with which
field using the information laid out earlier. For this we collect all the tickets
,including ours, in a big vector with `fromList (myTicket :: nearby)` and
from it we assign each field a list of possible rules that match it.

We achieve this with the following:


```
solve : { n : Nat} -> Input n -> Int
solve (MkInput rules myTicket nearby) =
   let asVect = Vect.fromList (myTicket :: nearby)
       withRules = map (getValidRules (toList rules)) asVect
       uniq = filterDownAssignement withRules
    in getSolution (toList $ zip uniq myTicket)
```

We see that this function calls `getSolution` after zipping `uniq` and `myTicket`
together, indicating that `uniq` contains the unique assignement of each rule to
each field. Let's see how we deal with that with `filterDownAssignement`.

First we need to remove all the tickets that have fiels that matche 0 rules, just like
in part 1. We achieve that with a filter on the list of tickets:
`let (S k ** rows) = filter (not . any isNil) tickets`. Then we _transpose_ our list
of tickets into a list of fields. This works because we represent our tickets
with a vector of length `m` and the fields for each ticket with a vector of length `n`.

This means that looking at our list of tickets is like looking at an `m x n` matrix.
But it also means we could be looking at a list of _columns_ each of which contains
a list of _fields_ that all must obey a single rule.

Now that we can look at every field and every rule each field can match we can compute
the intersection of all rules satisfied by all values of the same field so that we can
find the one (or the few) rules that they all have in common. After this process we
should find ourselves with a list of fields and a list of rules that has been greatly
reduced.

Now the last information about our data comes in. If we sort this list of columns
by the length of the rules that match the entire column, the first element should
contain only 1 rule. The second element should contain the first rule and a second
rule, the 3rd one should contain the first two rules and a third one, etc. We can then
assign and remove every rule that we find that is unique until we assigned every rule
to every field.

Since we previously sorted our list of columns by the number of candidate rules
they all have, their ordering does not represent the assignement of rules to fields
anymore. In order to recover the assignement we simply sort the list again but
using the original indices as the source for the ordering. We can then return
the list of rules and its ordering will reflect the assignement of each rule with
each field. That is, the rule for the first field with be in first position, etc.

This was quite wordy so here is the function in full:


```
filterDownAssignement : (Show a, Ord a, Eq a) => {n : Nat}
                     -> Vect m (AssignedTicket n a) -> (Vect n a)
filterDownAssignement tickets =
      -- first we remove the rows that contain values that don't match anything
  let (S k ** rows) = filter (not . any isNil) tickets
      -- Then we transpose from list of tickets to list of columns of field values
      columns = transpose rows
      -- then we find the minimum amount of rules that match all the values in
      -- each column of fields
      mapped = map (filterField . snd) (enumerated columns)
      -- Then we sort by the number of possible candidates for each column
      indexedCandidates = sortBy (\l, r => length (snd l) `compare` length (snd r)) (enumerated mapped)
      -- Then we assign them, removing the assigned rules from the other candidates lists
      filteredDown = mapCandidates indexedCandidates
      -- Then we recover the original assignement by sorting by the index of the
      -- columsn before we shuffled them around
      sortedCandidates = sortBy (\a, b => fst a `compare` fst b) filteredDown
      -- And finally we discard the index and return only the name of the rule
   in map snd sortedCandidates
  where
    -- This pics the rule if its the only one and remove it
    -- from the list of candidates in the other columns until
    -- all columns have been assigned a unique rule
    mapCandidates : forall l, a. Eq a => Vect l (Int, List a) -> Vect l (Int, a)
    mapCandidates [] = []
    mapCandidates ((index, [x]) :: xs) = (index, x) :: mapCandidates (map (map $ filter (/= x)) xs)

    -- This takes the interesection of all the valid rules for a given column
    filterField : Vect (S l) (List a) -> List a
    filterField (head :: tail) = foldr (\ls, ks => ks `intersect` ls) head tail
```

This program makes use of another number of auxillary functions like `enumerated` and
`sortBy` on vectors but they are not relevant to the core logic of this program so I
will leave them out.

Running this program will indeed return the value we are looking for by applying
the assignment found to our ticket and multiplying the values that are associated
with fields starting with the string "departure".
