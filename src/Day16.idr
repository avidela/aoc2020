module Main

import System
import System.File
import Data.SortedMap
import Data.SortedSet
import Data.Strings
import Data.List
import Data.Vect
import Data.List1
import Decidable.Equality
import Debug.Trace

Range : Type
Range = (Int, Int)

Ticket : Nat -> Type
Ticket n = Vect n Int

Rules : Nat -> Type
Rules n = Vect n (String, (Range, Range))

record Input (n : Nat) where
  constructor MkInput
  rules : Rules n
  myTicket : Ticket n
  nearby : List (Ticket n)

isValid : Int -> Range -> Bool
isValid x (y, z) = y <= x && x <= z

getInvalid : List Int -> List Range -> List Int
getInvalid tickets ranges = filter (\t => not $ any (isValid t) ranges) tickets

solve : { n : Nat} -> Input n -> Int
solve (MkInput rules myTicket nearby) =
  let flatRules : List Range = concatMap (\(_, (x, y)) => [x,y]) (toList rules)
      numbers : List Int = concat $ map toList nearby
   in sum $ getInvalid numbers flatRules

parseFile : String -> Maybe (n ** Input n)
parseFile input = let lines = lines input
                      [rules, [_, ticket], (_ :: nearby)] = forget $ split (=="") lines
                        | _ => Nothing
                   in do (m ** rules) <- parseRules rules
                         ticket <- parseTicket m ticket
                         nearby <- parseNearby nearby m
                         pure (_ ** MkInput rules ticket nearby)
  where
    parseRange : String -> Maybe Range
    parseRange x = let [low, high] = forget $ split (=='-') x
                         | _ => Nothing
                    in [| (parsePositive low, parsePositive high) |]

    parseRule : String -> Maybe (String, (Range, Range))
    parseRule x = let [name, ranges] = forget $ split (==':') x
                        | _ => Nothing
                      [r1, _, r2] = words ranges
                        | _ => Nothing in
                      (name, ) <$> [| MkPair (parseRange r1) (parseRange r2) |]

    parseRules : List String -> Maybe (m ** Vect m (String, (Range, Range)))
    parseRules xs = do rules <- traverse parseRule xs
                       pure $ (_ ** fromList rules)

    parseTicket : (k : Nat) -> String -> Maybe (Ticket k)
    parseTicket size x = (traverse parsePositive $ forget $ split (==',') x)
                     >>= toVect size

    parseNearby : List String -> (k : Nat) -> Maybe (List (Ticket k))
    parseNearby lines size = traverse (parseTicket size) lines

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just (_ ** parsed) = parseFile fileContent
  putStrLn ("Solution : " ++ show (solve parsed))
