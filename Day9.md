# Day 9 Part 1

Today we look at how to find an element within a list. This list is structured in
a very particular way: Given a number of elements `n`, each element _must_ be
the sum of two number from the sublist of `n` preceding elements of the list. The
first `n` characters don't have to follow this rule.

Imagine `n` is 3 then

```
[1,2,3,4,5]
```

is a valid list beacuse `4` is the sum `1+3` and `5` is the sum `2+3`.

```
[1, 3, 2, 3, 6, 4]
[1, 3, 2]:3 = 2 + 1
   [3, 2, 3]:6 = 3+3
      [2, 3, 6]:4 = ???
```

This is an example where `n = 3` and the list is has one invalid number: `4`. Indeed
`4` is not the sum of any two numbers in the preceeding sublist of length 3


Our job is then to find which element in our input does not follow this rule. For
this we are going to use an interesting data structure: A Big Zipper (TM).

A Zipper is a data structure that allows to focus on one element of a list
and provides functions to move forward this list or backward:

```
record Zipper (a : Type) where
  constructor MkZipper
  prev : List a
  curr : a
  next : List a

moveNext : Zipper a -> Zipper a
movePrev : Zipper a -> Zipper a
```

`moveNext` and `movePrev` allow to move the cursor forward or backward in the list,
returning the same zipper as before if we've already consumed it.

In our case we want to build a `Zipper`-like structure which has the list of preceding
elements as "cursor" and we would traverse it using `next`.

```
data BigZipper : Nat -> Type -> Type where
  MkZipper : (prev : List a) -> (curr : Vect n a) -> (next : List a) -> BigZipper n a

fromList : (n : Nat) -> List a -> Maybe (BigZipper n a)
fromList k xs = pure $ MkZipper [] !(toVect k (take k xs)) (drop k xs)

nextz : BigZipper (S n) a -> BigZipper (S n) a
nextz (MkZipper prev curr []) = MkZipper prev curr []
nextz (MkZipper prev (y :: ys) (x :: xs)) =
  MkZipper (y :: prev) (rewrite sym $ plusCommutative n 1 in ys ++ [x]) xs
```

This zipper is indexed over the length of the cursor as well. This means that our
`BigZipper` has the same semantics of a normal list when the cursor's length is `0`, and
the same semantics of a normal zipper when the cursor's length is `1`.

The `nextz` function allows to move the cursor forward, there is some type rewriting

magic to ensure the index is propagated correctly, but otherwise the implementation is
straightforward.

Finding the number that does not follow the rule amounts to traversing our `BigZipper` with
`nextz`, checking if the next value has a corresponding sum in the cursor, and return the
value if it does not. If we run out of elements to search we return `Nothing`, indicating
we failed to find an element that does not follow the rule. If the current element follows
the rule we recursively call ourselves by advancing the zipper by 1 with `nextz`:

```
findNumber : BigZipper 25 Int -> Maybe Int
findNumber (MkZipper prev curr []) = Nothing
findNumber zip@(MkZipper prev curr (x :: xs)) =
  case checkSum curr x of
       Nothing => Just x
       (Just _) => findNumber (nextz zip)
```

This implementation depends on one final auxillary function `checkSum` which ensures that the value
`x` has a corresponding sum in the current cursor `curr`, it's implementation is pretty naive,
generating all pairs of numbers in the cursor, summing them and checking if the value `x` is
in this array of sums. This is a bit dangerous since the size of the list is `n choose 2` where
`n` is the size of the cursor, but in our case, 25 choose 2 is "only" 300, and we can live with
that.

```
checkSum : {n : Nat} -> Vect n Int -> Int -> Maybe Int
checkSum xs x = find (==x) (allSums (toList xs) [])
  where
    allSums : List Int -> List Int -> List Int
    allSums [] acc = acc
    allSums (x :: xs) acc = allSums xs (acc ++ map (+ x) xs)
```

Idealy we would avoid the duplicate work due to recomputing sums we've already computed by keepipng
the old list around, only adding non-duplicates but this is a lot more work for very little
payoff.

We can now run `findNumber` on our input and move to part 2!

# Part 2

I am really glad that part 2 builds upon part 1 directly, asking us to use the result of Part 1
as an argument to our next problem: Finding the _weakness_ in our list.

The "weakness number" is defined as the sum of the biggest value and the smallest value of a
contiguous list of number from the input, such that the sum of this sublist of elements
is equal to the output of part 1.

In other words, part1 allowed us to find the number that "sticks out" from our list. We now need
to find a sublist of the input list, which sum amounts to this "sticking out" number.
Once we found that sublist we can compute the "weakness number" by adding together its
biggest and smallest element.

We are going to keep all the code from part 1 unmodified and add two functions in order to deal with
the two different parts of this new problem:

1. We need to find the sublist
2. We need to compute the weakness number from it

A contiguous sublist can be interpreted as two indices within our input list that represent its
begining and its end. Therefore, a function trying to find a contiguous sublist would return
a pair of `Nat` wihch represent the index of begining and end.

```
findContiguous : (target : Int) -> (xs : List Int)
              -> (lo, hi : Nat)
              -> (Nat, Nat)
```

In order to find those two indices, we are going to use the fact that if we do the sum of our
current sublist and find out that it is smaller than our target, it means we need more elements.
If the sum of the current sublist is bigger than the target, we need to remove some elements.

In our case "growing the list" can only mean "increasing the upper bound" and "shrinking the list"
means increasing the lower bound; So we simply computer the sum of our sublist and them compare it
with the target, and perform the correct recursive call if we have not found our sublist,

```
parameters (target : Int, xs : List Int)
  findContiguous : (lo, hi : Nat)
                -> (Nat, Nat)
  findContiguous lo hi =
    let sum = sumRange lo hi xs  in
        case compare target sum of
             EQ => (lo, hi)
             GT => findContiguous lo (S hi)
             LT => findContiguous (S lo) hi
```

In this implementation we moved the arguments `target` and `xs` into `paramters` block which allow
us to pass around those arguments in the recursive call implicitly. One could understand `parameters`
block as some syntax sugar for a reader monad.

The second part involves computing the weakness number from a sublist, our implementation might
fail if the sublist provided is empty. This is handled by the `fromList` function which
attempts to translate a `List a` into a `List1 a`. In addition, our `minMax` function searches
for the minimum and the maximum value in a non-empty list and returns both as a pair.

```
computeWeakness : List Int -> Maybe Int
computeWeakness = map (uncurry (+) . minMax) . fromList
  where
    minMax : Ord a => List1 a -> (a, a)
    minMax = foldl1 (\x => (x, x)) (\(mi, ma), y => (min mi y, max ma y))
```

Finally we put everything together with `findWeakness` which calls both functions and combine
their results together to extract the "weakness number" from our list.

```
findWeakness : (target : Int) -> List Int -> Maybe Int
findWeakness target xs = computeWeakness . uncurry (subList xs) $ findContiguous target xs 0 1
```

Before we go, I forgot to mention two auxillary functions we had to implement, `subList` and
`sumRange` which get the sublist of a list given a start and an end index, and return the
sum of a sublist given the start and end index:

```
subList : List a -> (lo, hi : Nat) ->  List a
subList ls lo hi = take (hi `minus` lo) (drop lo ls)

sumRange : (lo, hi : Nat) -> List Int -> Int
sumRange lo hi xs = sum (subList xs lo hi)
```

We can now call `findWeakness` with our number from part 1 and our list of numebers to find the
"weakness number"!
