# Day 5 Part 1

Today's problem is an excercise in decyphering obscure instructions.

The problem set asks us to find the "biggest id" where the id is computed from
a `row` and `col` value : `id = row * 8 + col`

Each `row` and `col` is extracted from a string composed of 10 characters,
the first 7 define the `row` and the last 3 the `col`

The row is composed of `F` or `B`, the way the problem is stated is really
convoluted but what you are supposed to understand is that this is a 7 digit
binary number and `row` is simply its value.

Similarly, `col` is a 3 digit binary number with `R` for `1` and `L` for 0,

Finally the ID `row * 8 + col` is equivalent to shifting the `row` as a binary
number by `3` and `xor`ing with the `col`

```
original data: FBBFBFBLRR
as binary : [0,1,1,0,1,0,1, 0, 1, 1]
row: [0,1,1,0,1,0,1]
col: [0,1,1]
row shifted: [0,1,1,0,1,0,1, 0, 0, 0] -- equivalent to '* 8'
`xor`ing the col: [0,1,1,0,1,0,1, 0, 1, 1] -- equivalent to '+ col'
```

In this short list of transformations you can see that computing the ID is
no different than interepreting the original string as a 10 digit binary number
with `L` and `F` as `0`, and, `R` and `B` as `1`.

Therefore we only need a couple of functions:

- `max : Ord a => (ls : List a) -> {auto check : NonEmpty ls} -> a` a maximum for lists
- `toNumber : List Nat -> Nat` a function to interpret lists of binary numbers as Nats.
Note that we assume the input list only has `0` and `1`s in it.
- `computeID : List Char -> Nat` a function to compute the corresponding ID from a list of char,
aka it get the binary number from it.


Let's start with `max` since it the one that's least related to our problem. We need to
implement it because idris doesn't have a max function for lists of ordered values

```
max : Ord a => (ls : List a) -> {auto check : NonEmpty ls} -> a
max [x] = x
max (x :: (y :: xs)) = Prelude.max x (Main.max (y :: xs))
```

Notice we use an argument `NonEmpty ls`  to ensure the list given in argument is not empty.
The rest of the implementation returns the only value we find if there is only 1 item in the
list, and returns the maximum of the current value and the maximum of the rest of the list
if there are more items to iterate through.

Next is `toNumber`, this function takes a list of `0` and `1` and returns the `Nat` that
represents the value from the binary number. Today we use `Nat` instead of `Bool` or
`data Binary = I | O` simply because we want to perform arithmetic operation on them.

In my brain, I convert from boolean to decimal by associating each position
with the power of 2 they represent, then take each associated power of 2 that corresponds
to a `1`, and add them together. Here is short illustration of that algorithm:

```
original number: 0110101011

binary digit     :  0   1   1   0   1   0   1   0   1   1
associated value : 512 256 128  64  32  16  8   4   2   1
filtered values  :  0  256 128  0   32  0   8   0   2   1
total sum        : 256 + 128 + 32 + 8 + 2 + 1 = 427
```

So that's we're gonna do here, first generate all the powers of 2 with
```(map (\x => 2 `pow` x) [0 .. (length binary)])```, then `zipWith (*) (reverse binary)` in order to
get the "filtered values" from the algorithm, and finally we sum the array with `sum`, the entire
program fits in one line:

```
toNumber : List Nat -> Nat
toNumber binary =
  sum $ zipWith (*) (reverse binary) (map (\x => 2 `pow` x) [0 .. (length binary)])
```
Since we are reading binary from right to left we need to reverse the `binary` list. This is called
[big endian](https://en.wikipedia.org/wiki/Endianness) because the "most significant bit" is at the end. "Most significant" here means
"bit that represents the highest power", here we have 10 digits so the last one represents a power of 9,
so `512` since we are working in binary.

Now we just need to parse our file, thankfully this step is really easy:

```
main : IO ()
main = do
  fileContent <- … -- get the file
  let (x :: xs) = map (computeID . unpack) (lines fileContent)
  putStrLn (show (max (x :: xs)))
```

We just need to define our array as `x :: xs` in order for the compiler to prove that the list is not
empty and therefore allow to call `max`.

# Part 2

Part 2 is equally as confusing in terms of problem statement but can be surmarised as such:

> Given a list of consecutive integer, find the one missing from the list


We are informed that our input is a list of all numbers from a minimum to a maximum and in this
list of numbers, only one is missing.

Finding a missing number from a sorted input can be done by simply by iteratively checking if
the next one equals the current one after being incremented by one. If it is, then we're not
missing any number and we need to keep looking. If it is not, then the incremented number is
the missing one.

```
findHole : List Nat -> Maybe Nat
findHole (x :: (y :: ys)) =
  if S x /= y
     then Just (S x)
     else findHole (y :: xs)
findHole _ = Nothing
```

If the list does not contain as least 2 elements we return `Nothing` meaning that we haven't
found the missing value.

Now the only thing that's left is to sort the list, thankfully `Data.List` has a `sort` function.
Getting our result amount to parsing the data, sorting it and then running `findHole` on it:

```
fileContent <- ...
let missing = findHole $ sort $ map (computeID . unpack) $ lines fileContent
putStrLn (show missing)
```

And that's it!

As usual the VOD is on [youtube](https://youtu.be/Lij8tby6aGk), the stream is on
[twitch](https://www.twitch.tv/identityGS ) and you can open an
[issue](https://gitlab.com/avidela/aoc2020/-/issues) if you
have any question or remark


