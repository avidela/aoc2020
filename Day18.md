# Day 18, Part 1

Today's problem revolves around parsing. Usually I leave out the parsing part
of each problem but this time we have a nice solution available to use by importing
the standard `Parser` libray. And more specifically `Parser.Expression`

`Parser` is a [parser combinator](https://en.wikipedia.org/wiki/Parser_combinator)
library with basic combinators like `>>=`, `<|>`,
`string` and `char`. In our case we are asked to parse a series of infix
expressions with two possible operators `+` and `*`. Those operators
both have the same priority, so they are simply parsed from left to right,
They also both have the same associativity, they associate to the left. That means
that `a * b * c` is parsed as `(a * b) * c`. This rule is crucial to corrrectly
interpret the rules of evaluation. Indeed, the problem set states that the
correct order of evalutation is:

```
1 + 2 * 3 + 4 * 5 + 6
  3   * 3 + 4 * 5 + 6
      9   + 4 * 5 + 6
         13   * 5 + 6
             65   + 6
                 71
```

Which is equivalent from evalutating `((((1+2) * 3) + 4) * 5) + 6` from the inside
out.

In order to parse infix expressions we are going to use the `Expression` module
from the `Parser` library. But before that, let us split up the work between
parsing and evaluating.

It is customary when parsing to convert our file into an intermediate data structure
and then work on that data structure to evaluate our program.

This has multiple benefits, it's easier for testing, it's easier to implement
evaluation for and for our case it makes it way easier to debug our parser since
we can see what it parsed rather than only see the end result as a number.

This kind of data structure is commonly refered to an _abstract syntax tree_, or
AST for short. Our AST has 3 components, either we are parsing an addition, or
we are parsing a multiplication, or we are parsing a number. Each side of
the addition and multiplication refer to sub-syntax trees, which brings us to the
following definition:

```
data Expr = Num Integer | Add Expr Expr | Mult Expr Expr
```

`Add` and `Mult` both refer to nested sub-expressions that are themselves additions,
multiplications or numbers.
This makes the evalutation very simple since is simply a function `Expr -> Integer`.

```
evaluate : Expr -> Integer
evaluate (Num x) = x
evaluate (Add x y) = evaluate x + evaluate y
evaluate (Mult x y) = evaluate x * evaluate y
```

Assuming the parsing gives us a `List Expr`, getting to the final result can be done by mapping
each of the expressions we parsed into their integer value and summing up
the result:

```
solve : List Expr -> Integer
solve = sum . map evaluate
```

So how do we do the parsing? `Parser.Expression` has a function
`buildExpressionParser : (a : Type) -> OperatorTable a -> Parser a -> Parser a`
which builds a parser out of an `OperatorTable` and a `Parser` for `Num`.

An `OperatorTable` is a list of parsers for operators which are sorted by
priority and given an associativity. Here we only have two operators and they
have the same priority. Additionally they associate to the left:

```
parseMult : Parser (Expr -> Expr -> Expr)
parseMult = const Mult <$> string " * "

parseAdd : Parser (Expr -> Expr -> Expr)
parseAdd = const Add <$> string " + "

operators : OperatorTable Expr
operators = [[ Infix parseAdd AssocLeft
             , Infix parseMult AssocLeft
            ]]
```

As you can see the two operators are declared using the `Infix` constructor it
takes their respective parsers for their symbols as strings as well as their
associativity, here we associate to the left. Notice we use spaces around our `+` and `*`
symbol because the parsers do not strip out spaces.

We are now ready to use `buildExpressionParser` with our `operators`, the last
argument is a parse for the each sub-trees around each operators. Since we can
nest arbitrarily deep expressions to the left or right we are going to allow
either a `Num`, our terminal symbol, or a nested parenthesised expression:

```
parseExpr : Parser Expr
parseExpr = buildExpressionParser Expr operators
                                  ((Num <$> integer) <|> parens parseExpr)
```

The final step is to parse the file for real, the `Parser` library has a
`parse : Parser a -> String -> Either String (a, Int)` function but you will
notice it returns a pair instead of a plain value. The `Int` in the pair is the
position at which we stopped parsing, since we do not use this value we discard it
in our definition of the function `parseAll : Parser a -> String -> Either String a`.
The `Left` case of our `Either` result contains the error message from the parser
in case the parsing failed.

```
parseAll : forall a. Parser a -> String -> Either String a
parseAll p = (fst <$>) . Parser.parse p

parseFile : String -> Either String (List Expr)
parseFile = traverse (parseAll parseExpr) . lines
```

`ParseFile` uses `traverse` to apply the parser to each line of the input. We could
have done without it and used `some` in order to parse a list of expressions as well
but we would have to take into account the fact that each expression
is separated by a new line, except the last one. This can be achieved with the
following function:

```
parseFile : String -> Either String (List Expr)
parseFile = parseAll (some (parseExpr <* (eol <|> eos)))
  where
    eol : Parser ()
    eol = const () <$> char '\n'
```

Where `eol` is the combinator for _end of line_, `eos` for _end of string_ and
`a <* b` means "sequence `a` and `b` but only return the result for `a`".
Effectively this is saying "parse a list of expression terminated by `eol` or `eos`.

# Part 2

Surprisingly the [diff](https://en.wikipedia.org/wiki/Diff)
between part 1 and part 2 is extremely small:

```
-operators = [[ Infix parseAdd AssocLeft
-             , Infix parseMult AssocLeft
-            ]]
+operators = [ [Infix parseAdd AssocLeft]
+            , [Infix parseMult AssocLeft]
+            ]
```

The only change is to re-order our list of operators such that addition has a higher
priority than multiplication.

If we rearrange the brackets a little be to see what's going on we have

```
-- Old
[ -- a single level of priority for our operators
  [ -- it contains two operators of the same priority
  Infix parseAdd AssocLeft,
  Infix parseMult AssocLeft
  ]
]

-- New
[ -- Two levels of priority
  [Infix parseAdd AssocLeft], -- The first one only contains addition
  [Infix parseMult AssocLeft] -- The second one only contains multiplicatoin
]
```

Re-running our program after this simple change gets us the correct result. This
was a great exercise in knowing how to use the tools that are available to us
and demonstrates the power of parser combinators in the real world.
