module Main

import System
import System.File
import Data.List
import Data.Vect
import Data.Strings
import Debug.Trace

%default total

data Cube = Active | Inactive

Eq Cube where
  Active == Active = True
  Inactive == Inactive = True
  _ == _ = False

Show Cube where
  show Active = "#"
  show Inactive = "."

Board : Nat -> Nat -> Type -> Type
Board s Z elem = elem
Board s (S d) elem = Vect s (Board s d elem)

Coordinate : Nat -> Type
Coordinate n = Vect n Int

count : Eq a => (a -> Bool) -> List a -> Nat
count f = length . filter f

indexInt : Int -> Vect n a -> Maybe a
indexInt 0 (x :: xs) = Just x
indexInt n (x :: xs) = indexInt (n - 1) xs
indexInt n _ = Nothing

indexN : Coordinate n -> Board s n a -> Maybe a
indexN [] x = Just x
indexN (idx :: xs) board =
 indexInt idx board >>= indexN xs

getClosest : Board s n (a, Cube) -> (curr, direction : Coordinate n) -> (Maybe Cube)
getClosest board curr direction =
  let newPosition = zipWith (+) curr direction in
      snd <$> indexN newPosition board

zipWithIndex : Vect n a -> Vect n (Int, a)
zipWithIndex xs = zipIndexInit xs 0
  where
    zipIndexInit : Vect m a -> Int -> Vect m (Int, a)
    zipIndexInit [] x = []
    zipIndexInit (y :: xs) x = (x, y) :: zipIndexInit xs (x + 1)

genCoordN : (n : Nat) -> List (Coordinate n)
genCoordN 0 = [[]]
genCoordN (S k) = let rec = genCoordN k
                      n1 = map (-1::) rec
                      n2 = map ( 0::) rec
                      n3 = map ( 1::) rec in n1 ++ n2 ++ n3

zero : (n : Nat) -> Coordinate n
zero n = replicate n 0

getAdjacent : {d : Nat} -> (cordinates : Coordinate d) -> Board s d (a, Cube) -> List Cube
getAdjacent coord board =
  let directions : List (Coordinate d)
      directions = filter (/= zero d) (genCoordN d)
   in catMaybes $ map (getClosest board coord) directions

updateCube : Cube -> (neighbors : List Cube) -> Cube
updateCube Active n = let actives = count (== Active) n in
                          if actives == 2 || actives == 3
                             then Active
                             else Inactive
updateCube Inactive xs = if count (==Active) xs == 3 then Active
                                                     else Inactive

generalisedFold : {d : Nat} -> (a -> acc -> acc) -> acc -> Board s d a -> acc
generalisedFold f x y {d = 0} = x
generalisedFold f x y {d = (S k)} = foldr (\board, acc => generalisedFold f acc board) x y

total
mapBoard : (d : Nat) -> (f : a -> b) -> Board s d a -> Board s d b
mapBoard Z f x = f x
mapBoard (S k) f x = map (mapBoard k f) x

playGame : {s, d : Nat} -> Board s d (Vect d Int, Cube) -> Board s d (Vect d Int, Cube)
playGame board =
  mapBoard d {s=s} {a=(Vect d Int, Cube)}
           (uncurry computeStep) board
    where
      computeStep : Coordinate d -> Cube -> (Coordinate d, Cube)
      computeStep coord elem =
        let adjacent = getAdjacent {d} {s} coord {a = Vect d Int} board in
            (coord, updateCube elem adjacent)

iterate : Nat -> (a -> a) -> a -> a
iterate 0 f x = x
iterate (S k) f x = let () = trace (show k ++ " iterations left") ()
                  in (Main.iterate k f (f x))

iterF : (n : Nat) -> (f : forall a. Board s a Cube -> Board s (S a) Cube) -> Board s n Cube
iterF Z f = Inactive
iterF (S n) f = f (iterF n f)

emptyCube : (a, b : Nat) -> Board a b Cube
emptyCube a b = iterF b (replicate a)

countActives : {d : Nat} -> Board s d (Vect d Int, Cube) -> Nat
countActives =
  generalisedFold {s} {acc=Nat} {a=(Vect d Int, Cube)} (\case (_, Active) => S; _ => id) Z

-- countActives xs = (foldr . flip . foldr . flip . foldr . flip . foldr) (\case Active => (+ 1)
--                                                                               _ => id) Z xs

center : Board 8 2 Cube -> Board 20 2 Cube
center xs = let inner = map (\vs => replicate 6 Inactive ++ vs ++ replicate 6 Inactive) xs
                emptyRow = emptyCube 20 1
             in replicate 6 emptyRow ++ inner ++ replicate 6 emptyRow

embed : (n : Nat) -> {x, y : Nat} -> Board (x + S y) n Cube -> Board (x + S y) (S n) Cube
embed n board {x} = let postfix = Vect.replicate y (emptyCube (x + S y) n)
                        prefixx = Vect.replicate x (emptyCube (x + S y) n)
                     in prefixx ++ (board :: postfix)

insertInput : Board 8 2 Cube -> Board 20 4 Cube
insertInput xs =
  let center = center xs
      emb3 = embed {x=9} {y=10} 2 center
   in embed 3 {x=9} {y=10} emb3

parseFile : String -> Maybe (Board 8 2 Cube)
parseFile = (>>= traverse parseLine) . (toVect 8) . lines
  where
    parseLine : String -> Maybe (Vect 8 Cube)
    parseLine = map (map (\case '#' => Active; _ => Inactive)) . toVect 8 . unpack


generateIndices : {d : Nat} -> Board s d a -> Board s d (Vect d Int, a)
generateIndices x {d = 0} = ([], x)
generateIndices x {d = (S k)} =
  let rec = zipWithIndex x
   in map (\(i, b) => let f = generateIndices {d=k} b
                       in mkIndex k f i) rec
  where
    mkIndex : (d : Nat) -> Board s d (Vect k Int, a) -> Int -> Board s d (Vect (S k) Int, a)
    mkIndex d board i = mapBoard d {s} {a=(Vect k Int, a)} {b=(Vect (S k) Int, a)} (\(vs, a) => (i :: vs, a)) board

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just parsed = parseFile fileContent
  putStrLn ("Solution : " ++ show (countActives {s=20} {d=4}
           $ Main.iterate 6 (playGame {s=20} {d=4}) (generateIndices {s=20} {d=4}
                                                           $ insertInput parsed)))
