module Main

import Data.SortedMap
import Data.SortedSet
import Data.Maybe
import Data.Strings
import Data.List
import Data.List1
import System
import System.File
import Debug.Trace

Dag : Type
Dag = SortedMap String (SortedSet (Nat, String))

-- -- map utilities
-- insertIfMissing : Eq a => (a , b) -> SortedMap a b ->  SortedMap a b
-- insertIfMissing (key, value) map =
--   case lookup key map of
--        Nothing => insert key value map
--        Just v => map
--
-- update : key -> (updateFn : value -> value) -> SortedMap key value -> SortedMap key value
-- update key updateFn smap =
--   let value = map updateFn $ lookup key smap in
--       fromMaybe smap $ map (\v => insert key v smap) value
--
-- addParent : (parent : String) -> (node : String) -> Dag -> Dag
-- addParent parent node = update node (insert parent)
--
-- -- counting
-- listOfParents : Dag -> (element : String) -> SortedSet String -> SortedSet String
-- listOfParents dag elem acc =
--   case lookup elem dag of
--        Nothing => acc
--        Just xs => if null xs
--                      then acc
--                      else let acc' = (acc `union` xs) in
--                               concatMap (\child => listOfParents dag child acc') (xs `difference` acc)
--
-- numberOfParents : Dag -> (element : String) -> Nat
-- numberOfParents dag elem = length $ SortedSet.toList $ listOfParents dag elem empty
--
-- -- data mangement
-- construct : (parent : String) -> (children : List String) -> Dag -> Dag
-- construct parent children dag =
--   let updatedMap = foldr (\key => insertIfMissing (key, empty)) dag (parent :: children)
--       withParents = foldr (\child => addParent parent child ) updatedMap children in
--       withParents

countNested : String -> Dag -> Nat
countNested elem smap with (lookup elem smap)
  countNested elem smap | Nothing = 0
  countNested elem smap | (Just children) =
    foldr (\(multiple, n), acc => acc + multiple * (S $ countNested n smap)) Z children


constructAll : List (String, List (Nat, String)) -> Dag
constructAll = SortedMap.fromList . map (map SortedSet.fromList)

-- parsing

parseChild : List String -> Maybe (Nat, String)
parseChild [n, a, b, c] = (, a ++ b) <$> parsePositive n
parseChild [a, b, c] = Just (0, a ++ b)
parseChild _ = Nothing

parseChildren : List (List String) -> Maybe (List (Nat, String))
parseChildren = traverse parseChild

partial
getPair : (line : String) -> (String, List (Nat, String))
getPair line =
  let (a :: b :: _ :: "contain" :: cs) = words line
      parent = concat [a,b]
      Just children = parseChildren
                        $ forget $ map Strings.words
                        $ Strings.split (==',') $ unwords cs
   in (parent, children)

partial
parseLines : String -> List (String, List (Nat, String))
parseLines = map getPair . lines

-- main

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let parsed = constructAll $ parseLines fileContent
  printLn (map (map SortedSet.toList) $ SortedMap.toList parsed)
  putStrLn ("numberOf shiny gold: " ++ show (countNested "shinygold" parsed))
