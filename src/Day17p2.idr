module Main

import System
import System.File
import Data.List
import Data.Vect
import Data.Strings

data Cube = Active | Inactive

Eq Cube where
  Active == Active = True
  Inactive == Inactive = True
  _ == _ = False

Board : Nat -> Nat -> Type -> Type
Board s Z elem = elem
Board s (S d) elem = Vect s (Board s d elem)

Coordinate : Nat -> Type
Coordinate d = Vect d Int

count : Foldable f => (a -> Bool) -> f a -> Nat
count predicate = foldr (\v => if predicate v then S else id) Z

indexInt : Int -> Vect d a -> Maybe a
indexInt 0 (x :: xs) = Just x
indexInt n (x :: xs) = indexInt (n - 1) xs
indexInt n _ = Nothing

indexN : Coordinate d -> Board s d a -> Maybe a
indexN [] x = Just x
indexN (idx :: xs) board =
 indexInt idx board >>= indexN xs

getClosest : Board s d a -> (curr, direction : Coordinate d) -> Maybe a
getClosest board curr direction =
  let newPosition = zipWith (+) curr direction in
      indexN newPosition board

zipWithIndex : Vect n a -> Vect n (Int, a)
zipWithIndex xs = zipIndexInit xs 0
  where
    zipIndexInit : Vect m a -> Int -> Vect m (Int, a)
    zipIndexInit [] x = []
    zipIndexInit (y :: xs) x = (x, y) :: zipIndexInit xs (x + 1)

genCoordN : (n : Nat) -> List (Coordinate n)
genCoordN 0 = [[]]
genCoordN (S k) = let rec = genCoordN k
                      n1 = map (-1::) rec
                      n2 = map ( 0::) rec
                      n3 = map ( 1::) rec in n1 ++ n2 ++ n3

zero : (n : Nat) -> Coordinate n
zero n = replicate n 0

getAdjacent : {d : Nat} -> (cordinates : Coordinate d) -> Board s d (Vect d Int, Cube) -> List Cube
getAdjacent coord board =
  let directions : List (Coordinate d)
      directions = filter (/= zero d) (genCoordN d)
   in catMaybes $ map (map snd .  (getClosest {s} {d} {a=(Vect d Int, Cube)} board coord)) directions

updateCube : Cube -> (neighbors : List Cube) -> Cube
updateCube Active n = let actives = count (== Active) n in
                          if actives == 2 || actives == 3
                             then Active
                             else Inactive
updateCube Inactive xs = if count (==Active) xs == 3 then Active
                                                     else Inactive

total
mapBoard : (d : Nat) -> (f : a -> b) -> Board s d a -> Board s d b
mapBoard Z f x = f x
mapBoard (S k) f x = map (mapBoard k f) x

playGame : {d : Nat} -> Board s d (Coordinate d, Cube) -> Board s d (Coordinate d, Cube)
playGame board =
  mapBoard {a=(Coordinate d, Cube)} d (\(coord, elem) =>
             (coord, updateCube elem (getAdjacent coord board))) board

iterate : Nat -> (a -> a) -> a -> a
iterate 0 f x = x
iterate (S k) f x = iterate k f (f x)

emptyCube : (a, b : Nat) -> Board a b Cube
emptyCube a 0 = Inactive
emptyCube a (S k) = replicate a (emptyCube a k)

countActives : {d : Nat} -> Board s d Cube -> Nat
countActives {d=Z} x = if x == Active then 1 else 0
countActives {d=S k} vects =
  foldr (\rec => (+ countActives rec)) Z vects

center : Board 8 2 Cube -> Board 20 2 Cube
center xs = let inner = map (\vs => replicate 6 Inactive ++ vs ++ replicate 6 Inactive) xs
                emptyRow = emptyCube 20 1
             in replicate 6 emptyRow ++ inner ++ replicate 6 emptyRow

embed : (n : Nat) -> {x, y : Nat} -> Board (x + S y) n Cube -> Board (x + S y) (S n) Cube
embed n board = let postfix = Vect.replicate y (emptyCube (x + S y) n)
                    prefixx = Vect.replicate x (emptyCube (x + S y) n)
                 in prefixx ++ (board :: postfix)

processInput : Board 8 2 Cube -> Board 20 4 Cube
processInput xs =
  let center = center xs
      emb3 = embed {x=9} {y=10} 2 center
   in embed 3 {x=9} {y=10} emb3

parseFile : String -> Maybe (Board 8 2 Cube)
parseFile = (>>= traverse parseLine) . (toVect 8) . lines
  where
    parseLine : String -> Maybe (Vect 8 Cube)
    parseLine = map (map (\case '#' => Active; _ => Inactive)) . toVect 8 . unpack

generateIndices : (d : Nat) -> Board s d a -> Board s d (Coordinate d, a)
generateIndices 0 = ([],)
generateIndices (S k) =
   map (\(i, b) => mkIndex k i (generateIndices k b)) . zipWithIndex
  where
    mkIndex : (d : Nat) -> Int -> Board s d (Vect k Int, a)
                               -> Board s d (Vect (S k) Int, a)
    mkIndex d i = mapBoard d {a=(Vect k Int, a)} (\(vs, a) => (i :: vs, a))

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just parsed = parseFile fileContent
  let result = countActives {d=4}
             $ mapBoard 4 {a=(Vect 4 Int, Cube)} {b=Cube} snd
             $ iterate 6 (playGame {s=20} {d=4})
             $ generateIndices {a=Cube} {s=20} 4 (processInput parsed)
  putStrLn ("Solution : " ++ show result)
