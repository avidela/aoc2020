module Main

import System
import System.File
import Data.Strings

data Turn = Left | Right | Around

data Cardinal = N | S | E | W

data Move = C Cardinal Nat | T Turn | F Nat

-- The state is defined by the coordinates of the ship
-- and the coordinates of the waypoint relative to the ship
-- and the direction its facing
BoatState : Type
BoatState = ((Int, Int), (Int, Int))

addP : Num n => (a, b : (n, n)) -> (n, n)
addP (x, y) (z, w) = (x + z, y + w)

scalar : Num n => n -> (n, n) -> (n, n)
scalar k (a, b) = (k * a, k * b)

getVector : Cardinal -> Nat -> (Int, Int)
getVector N k = (0              , cast k)
getVector S k = (0              , negate (cast k))
getVector E k = (cast k         , 0)
getVector W k = (negate (cast k), 0)

turnWaypoint : Turn -> (Int, Int) -> (Int, Int)
turnWaypoint Left (x, y) = (negate y, x)
turnWaypoint Right (x, y) = (y, negate x)
turnWaypoint Around wayp = (-1) `scalar` wayp

moveBoat : Move -> BoatState -> BoatState
moveBoat (C c l) (coord, wayp) = (coord, getVector c l `addP` wayp)
moveBoat (T turn) (coord, wayp) = (coord, turnWaypoint turn wayp)
moveBoat (F k) (coord, wayp) = (((cast k) `scalar` wayp) `addP` coord, wayp)

solve : List Move -> Int
solve moves =
  let ((x, y), _) = foldr moveBoat ((0,0), (10, 1)) moves in
      abs x + abs y

parseFile : String -> Maybe (List Move)
parseFile input = traverse (parseMove . unpack) (lines input)
  where
    parseMove : (List Char) -> Maybe Move
    parseMove ('N' :: n) = C N <$> parsePositive (pack n)
    parseMove ('S' :: n) = C S <$> parsePositive (pack n)
    parseMove ('W' :: n) = C W <$> parsePositive (pack n)
    parseMove ('E' :: n) = C E <$> parsePositive (pack n)
    parseMove ('L' :: n) = do num <- the (Maybe Int) (parsePositive (pack n))
                              T <$> case num of
                                   90 => pure Left
                                   180 => pure Around
                                   270 => pure Right
                                   _ => Nothing
    parseMove ('R' :: n) = do num <- the (Maybe Int) (parsePositive (pack n))
                              T <$> case num of
                                   90 => pure Right
                                   180 => pure Around
                                   270 => pure Left
                                   _ => Nothing
    parseMove ('F' :: n) = F <$> parsePositive (pack n)
    parseMove _ = Nothing

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just parsed = parseFile fileContent
  putStrLn ("Solution : " ++ show (solve parsed))
