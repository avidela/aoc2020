module Main

import System
import System.File
import Data.List
import Data.Strings

%default partial

-- we don't need to change the policy record but the names don't match anymore
record Policy where
  constructor MkPolicy
  minimum : Nat
  maximum : Nat
  letter : Char

-- count doesn't change
count : (a -> Bool) -> List a -> Nat
count p = length . (filter p)

-- This indexes a list from 1 and doesn't check for bounds
unsafeIndex1 : Nat -> List a -> a
unsafeIndex1 (S Z) (x :: xs) = x
unsafeIndex1 (S k) (x :: xs) = unsafeIndex1 k xs

-- here we changed what is a valid password
isValidPassword : Policy -> List Char -> Bool
isValidPassword (MkPolicy firstIndex secondIndex letter) word =
  let firstMatch = (unsafeIndex1 firstIndex word) == letter
      secondMatch = (unsafeIndex1 secondIndex word) == letter
   in firstMatch /= secondMatch

-- This function hasn't changed
countPasswords : List (Policy, String) -> Nat
countPasswords = count (uncurry isValidPassword) . map (map unpack)

-- parsing zzzzz (doesn't change either)

parseFile : String -> Maybe (List (Policy, String))
parseFile input =
      traverse parsePair (lines input)
  where
    parseRange : String -> Maybe (Nat, Nat)
    parseRange input = let (mn, mx) = Strings.break (=='-') input in
                           [| (parsePositive mn, parsePositive (strTail mx)) |]

    parseLetter : String -> Maybe Char
    parseLetter = head' . unpack

    parsePair : String -> Maybe (Policy , String)
    parsePair input = do let [range, letter, word] = words input
                         (mn, mx) <- parseRange range
                         l <- parseLetter letter
                         pure (MkPolicy mn mx l, word)

main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just parsed = parseFile fileContent
  putStrLn ("valid password count: " ++ show (countPasswords parsed))


