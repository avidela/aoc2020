module Main

import Data.Stream
import Data.List
import Data.Strings
import System
import System.File

-- Utility functions

Tree : Type
Tree = Bool

isEven : Nat -> Bool
isEven Z = True
isEven (S k) = not (isEven k)

enumerate : List a -> List (Nat, a)
enumerate ls = zip [0 .. (length ls)] ls

count : (predicate : a -> Bool) -> List a -> Nat
count p = length . (List.filter p)

-- slopes
SlopeStrategy : Type
SlopeStrategy = (Nat, Stream Tree) -> Bool

isTree : (Stream Tree) -> (col : Nat) -> Bool
isTree treeStream index = Stream.head $ drop index treeStream

generateSlope : (run : Nat) -> SlopeStrategy
generateSlope run (index, row) = isTree row (index * run)

slope12 : SlopeStrategy
slope12 (index, row) = if isEven index then isTree row index
                                       else False

slopes : List SlopeStrategy
slopes = [generateSlope 1, generateSlope 3, generateSlope 5, generateSlope 7, slope12]

countTree : List (Stream Tree) ->(slope : SlopeStrategy) ->  Nat
countTree trees slope =
  let foundTrees = map slope (enumerate trees)
   in count id foundTrees

multiplyAll : List (Stream Tree) -> Nat
multiplyAll trees = foldl (*) 1 (map (countTree trees) slopes)

-- parser functions

maybeCycle : List a -> Maybe (Stream a)
maybeCycle [] = Nothing
maybeCycle (x :: xs) = Just $ cycle (x :: xs)

convert : String -> Maybe (List (Stream Tree))
convert input = let rows = map unpack (lines input )
                    toTrees = map (map (== '#')) rows
                 in traverse (maybeCycle) toTrees

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just parsed = convert fileContent
  putStrLn ("number of trees encountered : " ++ show (multiplyAll parsed))
