module Main

import Control.Monad.Identity
import Data.SortedMap
import Data.Bool
import Data.Maybe
import Data.Either
import Data.Fin
import Data.List
import Data.List1
import Data.Strings
import System
import System.File
import Data.String.Parser

count : (p : a -> Bool) -> List a -> Nat
count p = length . (List.filter p)

record PassportData where
  constructor MkPassport
  birthYear : Int
  issueYear : Int
  expirationYear : Int
  height : Int
  hairColor : String
  eyeColor : String
  passportID : Int -- List (Fin 10)with (_)
  countryID : Maybe String

checkNumber : (lower, upper : Int) -> (digit : Nat) -> String -> Maybe Int
checkNumber lower upper digit str =
  let chars = unpack str in
      parsePositive str
      >>= (\i => toMaybe (lower <= i && i <= upper && (length chars == digit)) i)

checkEyeColor : String -> Maybe String
checkEyeColor "amb" = Just "amb"
checkEyeColor "brn" = Just "brn"
checkEyeColor "blu" = Just "blu"
checkEyeColor "gry" = Just "gry"
checkEyeColor "grn" = Just "grn"
checkEyeColor "hzl" = Just "hzl"
checkEyeColor "oth" = Just "oth"
checkEyeColor _ = Nothing

checkHeight : String -> Maybe Int
checkHeight str =
  let (digits, unit) = break (not . isDigit) str in
      case unit of
           "cm" => checkNumber 150 193 3 digits
           "in" => checkNumber 59 76 2 digits
           _ => Nothing

checkColor : String -> Maybe String
checkColor str = case unpack str of
                      ('#' :: color) => toMaybe (all isHexDigit color && length color == 6) str
                      _ => Nothing

checkPassport : SortedMap String String -> Maybe PassportData
checkPassport dataMap = do
  byr <- lookup "byr" dataMap >>= checkNumber 1920 2002 4
  iyr <- lookup "iyr" dataMap >>= checkNumber 2010 2020 4
  eyr <- lookup "eyr" dataMap >>= checkNumber 2020 2030 4
  hgt <- lookup "hgt" dataMap >>= checkHeight
  hcl <- lookup "hcl" dataMap >>= checkColor
  ecl <- lookup "ecl" dataMap >>= checkEyeColor
  pid <- lookup "pid" dataMap >>= checkNumber 0 999999999 9
  let cid = lookup "pid" dataMap
  pure $ MkPassport byr iyr eyr hgt hcl ecl pid cid

countValidPassports : List (SortedMap String String) -> IO Nat
countValidPassports passports = do
   array <- traverse (\x => let v = checkPassport x in when (isNothing v) (printLn x) *> pure v) passports
   pure $ count isJust array

processPassports : String -> List (SortedMap String String)
processPassports input =
  let lines = lines input
      passportFields = extractPassport lines
   in map fromList passportFields
      where
        getKeyValue : String -> (String, String)
        getKeyValue input = let (l , r) = break (==':') input in
                                (l, assert_total $ strTail r)
        -- get the list of passport compotnents out of a list of lines
        getFields : List String  -> List (String, String)
        getFields = map getKeyValue . concatMap words

        extractPassport : List String -> List (List (String, String))
        extractPassport lines = let passportLines = split (=="") lines in
                                    map getFields (forget passportLines)

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let passports = processPassports fileContent
  count <- countValidPassports passports
  putStrLn ("valid password count: " ++ show count)










