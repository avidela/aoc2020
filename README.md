# Advent Of Code 2020 in Idris

In this repository I will attempt every [Advent Of Code](https://adventofcode.com) challenge and write down how I went about solving it
using the [Idris programming language](https://www.idris-lang.org).

If you have any questions or feedback, please feel free to open an [issue](https://gitlab.com/avidela/aoc2020/-/issues)!

Live coding can be found at the [identityGS twitch channel](http://twitch.tv/identitygs/videos),
VODs can be found on the [identityGS youtube channel](https://www.youtube.com/watch?v=pRIf3UBqYXQ&list=PLBEZvEpNgUMbSW2Z-Tbis3AVecozzWkIO)

- [Day 1](Day1.md): Finding a pair in a list.
- [Day 2](Day2.md): Counting elements that satisfy a condition in a list - [livestream](https://youtu.be/RlKfbU-OtI8)
- [Day 3](Day3.md): Counting the number of trees we hit, sliding down a slope - [livestream](https://youtu.be/pRIf3UBqYXQ)
- [Day 4](Day4.md): Parsing passport data.
- [Day 5](Day5.md): Just binary - [livestream](https://youtu.be/Lij8tby6aGk)
- [Day 6](Day6.md): Union and Intersection - [livestream](https://youtu.be/sS94JW2yG8I)
- [Day 7](Day7.md): Scouring graphs.
- [Day 8](Day8.md): Interpreting instructions - [livestream](https://youtu.be/RCcyA-JOn-M)
- [Day 9](Day9.md): Searching a list - [livestream](https://youtu.be/crbZbUcIR8Y)
- [Day 10](Day10.md): Searching a list - [livestream](https://youtu.be/a2USbu8SJlE)
- [Day 11](Day11.md): Cellular automata & fix point - [livestream](https://youtu.be/Uuag0J2aWfo)
- [Day 12](Day12.md): It's a boat! - [livestream](https://youtu.be/Kq4h7whHk60)
- [Day 13](Day13.md): Chinese Reminders theorem
- [Day 14](Day14.md): Masking binary tape
- [Day 15](Day15.md): Endless streams
- [Day 16](Day16.md): Unexpected Transpose - [livestream](https://youtu.be/ftyQ4C1LKoc)
- [Day 17](Day17.md): n-dimensional madness - [livestream](https://youtu.be/F9g4rJ2X940)
- [Day 18](Day18.md): Easy parsers
- [Day 22](Day22.md): Recursive fiesta
- [Day 24](Day24.md): Hexagons are bestagons
- [Conclusion](Conclusion.md): The end of the ride
