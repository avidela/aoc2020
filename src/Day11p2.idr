module Main

import System
import System.File
import Data.Strings
import Data.List
import Data.Nat

count : Eq a => (a -> Bool) -> List a -> Nat
count f = length . filter f

data Tile = Floor | Free | Occupied

Show Tile where
  show Floor = "."
  show Free = "L"
  show Occupied = "#"

Eq Tile where
  Floor == Floor = True
  Free == Free = True
  Occupied == Occupied = True
  _ == _ = False

Board : Type
Board = List (List Tile)

isFree : Tile -> Bool
isFree Floor = True
isFree Free = True
isFree Occupied = False

updateTile : Tile -> List Tile -> Tile
updateTile Floor xs = Floor
updateTile Free xs = if all isFree xs then Occupied
                                      else Free
updateTile Occupied xs = if count (==Occupied) xs >= 5
                            then Free
                            else Occupied

mIndex : Nat -> List a -> Maybe a
mIndex 0 [] = Nothing
mIndex 0 (x :: xs) = Just x
mIndex (S k) [] = Nothing
mIndex (S k) (x :: xs) = mIndex k xs

toNat : Int -> Maybe Nat
toNat x = case compare x 0 of
               EQ => Just Z
               GT => S <$> toNat (x - 1)
               LT => Nothing

getCoord : (row, col : Int) -> Board -> Maybe Tile
getCoord row col xs = do getRow <- mIndex !(toNat row) xs
                         mIndex !(toNat col) getRow

plusC : (Int, Int) -> (Int, Int) -> (Int, Int)
plusC (a, b) (c, d) = (a + c, b + d)

getClosestSeat : Board -> (pos, dir : (Int, Int)) -> Maybe Tile
getClosestSeat board position direction  =
  let newPos = position + direction in
  uncurry getCoord newPos board >>=
    \case Floor => getClosestSeat board newPos direction
          x => Just x

getAdjacent : (row, col : Int) -> Board -> List Tile
getAdjacent row col board =
  let directions : List (Int, Int)
      directions =  [(x, y) | x <- [-1, 0, 1], y <- [-1, 0, 1], x /= 0 || y /= 0]
   in catMaybes $ map (getClosestSeat board (row, col)) directions

zipWithIndex : List a -> List (Int, a)
zipWithIndex xs = zip [0 .. (cast $ length xs)] xs

playGame : Board -> Board
playGame board =
  map (\(i, row) => map (\case (j, Floor) => Floor
                               (j, tile) => updateTile tile (getAdjacent i j board))
                        (zipWithIndex row))
  (zipWithIndex board)

repeat : Nat -> (a -> a) -> a -> a
repeat 0 f x = x
repeat (S k) f x = repeat k f (f x)

partial
fix : (Show a, Eq a) => (a -> a) -> a -> a
fix f oldValue = let newValue = f oldValue in
                     if oldValue == newValue then newValue
                                             else fix f newValue

partial
solve : Board -> Nat
solve board = let foundFixed = fix playGame board in
                  count (==Occupied) (concat foundFixed)

parseFile : String -> Board
parseFile input = map parseLine (lines input)
  where
    parseLine : String -> (List Tile)
    parseLine line = map (\case 'L' => Free
                                _ => Floor) (unpack line)
printBoard : Board -> IO ()
printBoard xs = putStrLn $ unlines (map show xs)

partial
getBoard : String -> IO Board
getBoard file = do
  Right fileContent <- readFile file
  pure $ parseFile fileContent

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let parsed = parseFile fileContent
  putStrLn ("Solution : " ++ show (solve parsed))
