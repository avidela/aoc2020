# Day 8 Part 1

This time we're looking at how to execute instructions using a very simple processor.
The processor's state has 3 parts:

- An instruction pointer, which tells us which instruction we are looking at
- An "accumulator" value representing the value the program is computing.
- A history of instructions already visited.

In order to refer this complex state we are going to use a type alias. Notice
we are using a `Nat` argument, we'll come back to this a bit later.

```
-- The state is an `Fin n` instruction pointer
-- An `Int` for the current accumulator state
-- a list of `Fin n` of already exected instructions
ProgramState : Nat -> Type
ProgramState n = (Fin n, Int, List (Fin n))
```

The instruction set only has 3 instructions: `acc`, `jmp` and `nop`. `acc` allows
the processor to modify the "accumulator value", `jmp` allows to jump to another
instruction within the program, `nop` does nothing.

The first thing to do is to define a data type to describe our instructions along with
the argment they use.

```
data Instruction = Acc Int | Nop Int | Jmp Int
```

`acc`'s argument is used to increment or decrement the current state and `jmp`'s argument is used
to select how far to skip forward or backward within the program.

Interpreting programs in Idris is a bit tricky because the compiler really want you to write
total functions, that is, functions that terminate for all inputs. In this case we need to interpret
the input program until we find a looping expression; technically speaking this process
terminates. However proving this fact is out of scope for this small exercise, instead, we
are going to use Idris's escape hatch and annotate our interpreter with `partial`.

Interpreting a program is pretty easy conceptually, it means taking a program, taking
the program state and executing the current instruction. When we run out of instructions we simply
return the current value of the accumulator.

```
interpret : {n : Nat} -> Program n -> ProgramState n -> Int
```

Before we implement this functions let's look at the `n` index. We use it to record the _size_ of
the program this is used in order to find which instrction to use without having to check if the
instruction is in bounds. When we defined the state of our program we used `Fin n` to define our
function pointers, this means every function pointer is bounded by `n`, and conveniently, we setup
our type signature such that `n` is also the size of our program, this way, every instruction we
jump to will be within the bounds of the array of instructions.

This means that _indexing_ in the array of instructions will never fail but _adding_ instruction
offsets might fail. In practice this allows us to separate different failure conditions and only
worry about the relevant ones when they matter.

With that, the implementation is still a bit tricky though conceputually simple:

- Get the current instruction from the program using the instruction pointer
- Execute the instruction to compute the new state of the accumulator
- Find the next instruction pointer.
    - If the next instruction pointer is out of bounds, the program is over, return the new state.
- If the instruction pointer has already been explored return the old state.
- Otherwise, keep executing using the new instruciton pointer and the new state.

The tricky bits consist in carefully returning the correct state, and comparing the correct
instruction pointer. If we were to return the old state in the last statement our program would
be incorrect (but would typecheck!) ditto if we compared the current instruction pointer rather than
the next one. With all that said, here is the code:

```
interpret : {n : Nat} -> Program n -> ProgramState n -> Int
interpret prog (ip, state, hist) =
  let instruction = index ip prog -- get the current instruction
      newState = execute instruction state -- get the new state
      Just next = nextPointer ip instruction -- get the next instruction pointed
      | Nothing => newState in -- return if we're done
  if next `elem` hist -- if the next instruction has already been executed
     then newState -- return the newState
     else interpret prog (next, newState, ip :: hist) -- else keep executing
```

This program makes use of 2 auxilary functions which are pretty easy to implement, `execute` to
get the new state from an instruction and a state, and `nextPointer` which computes the
next instruction pointer given the current instruction pointer and an offset. Note that
`nextPointer` is the only function returning a `Maybe`, indicating that it is the only function
that might fail, when it does, it indicates the program is finished.

```
-- given a current instruction pointer, and an instruction get the new
-- instruction pointer. Return `Nothing` if the next pointer is out of bounds.
nextPointer : {n : Nat} -> (ip : Fin n) -> Instruction ->  Maybe (Fin n)
nextPointer ip (Acc x) = safeAdd ip 1
nextPointer ip (Nop x) = safeAdd ip 1
nextPointer ip (Jmp x) = safeAdd ip x
  where
    -- safely add an `Int` to a `Fin n`
    safeAdd : {n : Nat} -> Fin n -> Int -> Maybe (Fin n)
    safeAdd x y = integerToFin (cast x + cast y) n


-- Given an instruction and a state, get the new state
execute : Instruction -> Int -> Int
execute (Acc x) acc = acc + x
execute (Nop x) acc = acc
execute (Jmp x) acc = acc
```

We had to implement a function to add an `Int` to a `Fin n` and check the bounds, other than
that the implementation are pretty straightforward.

We can now call `interpret` with our program as input and an empty initial state of `(0, 0, [])`
to get our result.

# Part 2

Part 2 took me a long time because I misunderstood the program statement, a situation that reminded
me ~~dreadfully~~ _dearly_ of school time when I would misunderstand a question during a test,
answer something completely unrelated but just as complicated, and get rewarded with 0 points.

Thankfully nowadays nobody can judge my performance when I spend an hour implementing
something that isn't the problem at hand (except for Twitch chat). So what is the problem at hand?

In this verion we need to modify our program in order to ensure termination. Since there is [no way
to tell when a program halts in the general case](https://en.wikipedia.org/wiki/Halting_problem) we
have to use some additional information about our program, here is what we know:

- There is only 1 instruction responsible for making the program non-terminating
- This instruction is either a `nop` or a `jump`
- The change to perform is to replace a `nop` to a `jmp` or vice-versa
- We know the program is non-terminating if we re-visit an instrction that has already been executed

With that information we have both a way to fix our program and a way to test if our fix worked.
Since our program is quite small in size (about 600 instructions) what we can do is generate an
array of all the programs which have exactly 1 instruction changed, and run them in sequence to
find the one that is terminating. For this we need to change our `interpret` function and we
need a function to generate modified programs. Let's start with the second one.

```
generateModifiedPrograms : {n : Nat} -> Program n -> List (Program n)
generateModifiedPrograms prog =
  let indices : List (Fin n) = filter (\idx => case index idx prog of
                                                    (Acc _) => False
                                                    _ => True)
                                      (toList $ range {len=n}) in
      map (\idx => updateAt idx fixup prog) indices
```

This function takes a program and generate a list of programs that have a single `jmp` or
`nop` instruction flipped. The implementation generates a vector of `Fin n` containing
all the possible indices of the program, then removes every index that points to an `acc`
instruction (the instructions that we do not change, and therefore, do not need to test
for termination). Once the list of indicies has been filtered down we map it to a list of
programs that each have the instruction at the coresponding index fliped. The flipping
occurs with a `fixup` functions which simply flips `nop` and `jmp`.

```
fixup : Instruction -> Instruction
fixup (Acc x) = Acc x
fixup (Nop x) = Jmp x
fixup (Jmp x) = Nop x
```

We now have access to a list of programs, our goal is now to find the first program in this list
that terminates. We achieve that using a fold over the list of program and executing them one
by one, returning `Nothing` if they loop and `Just` with the result of the program if they
terminate sccessfully. This allows to avoid interpreting programs once we found one that works.

```
partial
findTerminating : {n : Nat} -> Program (S n) -> Maybe Int
findTerminating prog = foldr (\prog, r  => maybe (doesItTerminate prog (0,0,[])) Just r)
                             Nothing
                             (generateModifiedPrograms prog)
```

Finally we need to modify our `interpret` function to tell us if the program terminates with
a value, or if it loops forever, the changes are slight but very easy to get wrong:

```
partial
doesItTerminate : {n : Nat} -> Program n -> ProgramState n -> Maybe Int
doesItTerminate prog (ip, state, hist) =
  let instruction = index ip prog
      newState = execute instruction state
      Just next = nextPointer ip instruction
      | Nothing => pure newState in
  if next `elem` hist
     then Nothing
     else doesItTerminate prog (next, newState, ip :: hist)
```

The difference is that we now return a `Maybe` value which indicate if the program terminates or
not. If the program loops, that is if we encounter a previously executed instructions, we return
`Nothing` if it terminates with a value, we return `Just` using the value computed.

We can now run this program with `findTerminating` and it will return us the value of the first
program it finds that terminates. And `Nothing` if none of the changes performed resulted in a
terminating program.


