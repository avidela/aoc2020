module Main

import System
import System.File
import Data.List
import Data.Strings

%default covering

-- a policy is defined by the minimum number of occurences,
-- the maximum number of occurences
-- and the letter subjet to those restrictions
record Policy where
  constructor MkPolicy
  minimum : Nat
  maximum : Nat
  letter : Char

-- cont is the number of element of a list after it's been filtered
count : (predicate : a -> Bool) -> List a -> Nat
count p = length . (filter p)

-- A valid password is a password which contain the correct amount of occurrences
-- of a letter defined by a Policy
isValidPassword : (Policy, List Char) -> Bool
isValidPassword (MkPolicy minimum maximum letter) xs =
  let c = count (== letter) xs in
      minimum <= c && c <= maximum

-- `map (map unpack)` changes `String` to `List Char` in order to process each character
-- in `isValidPassword`
countPasswords : List (Policy, String) -> Nat
countPasswords = count isValidPassword . map (map unpack)

-- parsing zzzzzz

parseFile : String -> Maybe (List (Policy, String))
parseFile input =
  let ls = lines input in
      traverse parsePair ls
  where
    parseRange : String -> Maybe (Nat, Nat)
    parseRange input = let (mn, mx) = Strings.break (=='-') input in
                     [| (parsePositive mn, parsePositive (assert_total $ strTail mx)) |]
    parseLetter : String -> Maybe Char
    parseLetter = head' . unpack
    parsePair : String -> Maybe (Policy , String)
    parsePair input = do let [range, letter, word] = words input
                           | _ => Nothing
                         (mn, mx) <- parseRange range
                         l <- parseLetter letter
                         pure (MkPolicy mn mx l, word)

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just parsed = parseFile fileContent
  putStrLn ("valid password count: " ++ show (countPasswords parsed))


