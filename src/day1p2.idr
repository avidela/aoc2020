module Main

import Data.List
import Data.Strings
import System
import System.File

findSum :  (zeroValue : Int) -> (firstValue : Int) -> List Int -> Maybe Int
findSum zeroValue firstValue = find (\secondValue => zeroValue + firstValue + secondValue == 2020)

findPair : (value : Int) -> List Int -> Maybe (Int, Int)
findPair value [] = Nothing
findPair value (x :: xs) = case findSum value x (x :: xs) of
                          Nothing => findPair value xs
                          (Just y) => Just (x , y)

findTriple : List Int -> Maybe (Int, Int, Int)
findTriple [] = Nothing
findTriple (x :: xs) = case findPair x (x :: xs) of
                            Nothing => findTriple xs
                            (Just (y, z)) => Just (x, y, z)

findNumber : List Int -> Maybe Int
findNumber ls = map (\(a, b, c) => a * b * c) (findTriple ls)

parseFile : String -> Maybe (List Int)
parseFile input = let lines = lines input in
                      traverse parsePositive lines

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let (Just parsed) = parseFile fileContent
  case findNumber parsed of
       Nothing => putStrLn "couldn't find a pair of number that sum to 2020"
       (Just prod) => putStrLn $ "found product" ++ show prod



