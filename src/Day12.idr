module Main

import System
import System.File
import Data.Strings

data Turn = Left | Right | Around

data Cardinal = N | S | E | W

data Move = C Cardinal Nat | T Turn | F Nat

-- The state of the boat is defined by its coordinates
-- and the direction its facing
BoatState : Type
BoatState = ((Int, Int), Cardinal)

addP : Num n => (a, b : (n, n)) -> (n, n)
addP (x, y) (z, w) = (x + z, y + w)

getVector : Cardinal -> Nat -> (Int, Int)
getVector N k = (0              , cast k)
getVector S k = (0              , negate (cast k))
getVector E k = (cast k         , 0)
getVector W k = (negate (cast k), 0)

changeDirection : Turn -> Cardinal -> Cardinal
changeDirection Around N = S
changeDirection Around S = N
changeDirection Around E = W
changeDirection Around W = E
changeDirection Left N = W
changeDirection Left S = E
changeDirection Left E = N
changeDirection Left W = S
changeDirection Right N = E
changeDirection Right S = W
changeDirection Right E = S
changeDirection Right W = N

moveBoat : Move -> BoatState -> BoatState
moveBoat (C c l) (coord, dir) = (getVector c l `addP` coord, dir)
moveBoat (T turn) (coord, dir) = (coord, changeDirection turn dir)
moveBoat (F k) (coord, dir) = (getVector dir k `addP` coord, dir)

solve : List Move -> Int
solve moves =
  let ((x, y), _) = foldr moveBoat ((0,0), E) moves in
      abs x + abs y

parseFile : String -> Maybe (List Move)
parseFile input = traverse (parseMove . unpack) (lines input)
  where
    parseMove : (List Char) -> Maybe Move
    parseMove ('N' :: n) = C N <$> parsePositive (pack n)
    parseMove ('S' :: n) = C S <$> parsePositive (pack n)
    parseMove ('W' :: n) = C W <$> parsePositive (pack n)
    parseMove ('E' :: n) = C E <$> parsePositive (pack n)
    parseMove ('L' :: n) = do num <- the (Maybe Int) (parsePositive (pack n))
                              T <$> case num of
                                   90 => pure Left
                                   180 => pure Around
                                   270 => pure Right
                                   _ => Nothing
    parseMove ('R' :: n) = do num <- the (Maybe Int) (parsePositive (pack n))
                              T <$> case num of
                                   90 => pure Right
                                   180 => pure Around
                                   270 => pure Left
                                   _ => Nothing
    parseMove ('F' :: n) = F <$> parsePositive (pack n)
    parseMove _ = Nothing

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just parsed = parseFile fileContent
  putStrLn ("Solution : " ++ show (solve parsed))
