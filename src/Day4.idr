module Main

import Data.SortedMap
import Data.Maybe
import Data.List
import Data.List1
import Data.Strings
import System
import System.File


count : (p : a -> Bool) -> List a -> Nat
count p = length . (List.filter p)

record PassportData where
  constructor MkPassport
  birthYear : String
  issueYear : String
  expirationYear : String
  height : String
  hairColor : String
  eyeColor : String
  passportID : String
  countryID : Maybe String

checkPassport : SortedMap String String -> Maybe PassportData
checkPassport dataMap = do
  byr <- lookup "byr" dataMap
  iyr <- lookup "iyr" dataMap
  eyr <- lookup "eyr" dataMap
  hgt <- lookup "hgt" dataMap
  hcl <- lookup "hcl" dataMap
  ecl <- lookup "ecl" dataMap
  pid <- lookup "pid" dataMap
  let cid = lookup "cid" dataMap
  pure $ MkPassport byr iyr eyr hgt hcl ecl pid cid

countValidPassports : List (SortedMap String String) -> Nat
countValidPassports passports = count isJust (map checkPassport passports)

processPassports : String -> List (SortedMap String String)
processPassports input =
  let lines = lines input
      passportFields = extractPassport lines
   in map fromList passportFields
      where
        getKeyValue : String -> (String, String)
        getKeyValue input = let (l , r) = break (==':') input in
                                (l, assert_total $ strTail r)
        -- get the list of passport compotnents out of a list of lines
        getFields : List String  -> List (String, String)
        getFields = map getKeyValue . concatMap words

        extractPassport : List String -> List (List (String, String))
        extractPassport lines = let passportLines = split (=="") lines in
                                    map getFields (forget passportLines)

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let passports = processPassports fileContent
  putStrLn ("valid password count: " ++ show (countValidPassports passports))










