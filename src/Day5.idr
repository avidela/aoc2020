module Main

import System
import System.File
import Data.List
import Data.Strings

pow : Nat -> Nat -> Nat
pow x 0 = 1
pow x (S k) = x * (x `pow` k)

toNumber : List Nat -> Nat
toNumber binary = 
  sum $ zipWith (*) (reverse binary) (map (\x => 2 `pow` x) [0 .. (length binary)])

computeID : List Char -> Nat
computeID = toNumber . map (\case 'F' => Z
                                  'L' => Z
                                  _   => 1)

max : Ord a => (ls : List a) -> {auto check : NonEmpty ls} -> a
max [x] = x
max (x :: (y :: xs)) = Prelude.max x (Main.max (y :: xs))

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let (s :: ss) = map (computeID . unpack) $ lines fileContent
  putStrLn ("maximum ID: " ++ show (Main.max (s :: ss)))


