module Main

import System
import System.File
import Data.String.Parser
import Data.String.Parser.Expression
import Data.Strings
import Debug.Trace

data Expr = Num Integer | Add Expr Expr | Mult Expr Expr

evaluate : Expr -> Integer
evaluate (Num x) = x
evaluate (Add x y) = evaluate x + evaluate y
evaluate (Mult x y) = evaluate x * evaluate y

parseMult : Parser (Expr -> Expr -> Expr)
parseMult = const Mult <$> string " * "

parseAdd : Parser (Expr -> Expr -> Expr)
parseAdd = const Add <$> string " + "

operators : OperatorTable Expr
operators = [ [Infix parseAdd AssocLeft]
            , [Infix parseMult AssocLeft]
            ]

parseExpr : Parser Expr
parseExpr = buildExpressionParser Expr operators ((Num <$> integer) <|> parens parseExpr)

parseAll : forall a. Parser a -> String -> Either String a
parseAll p = (fst <$>) . Parser.parse p

testExpr : String -> Either String Integer
testExpr = (evaluate <$>) . parseAll parseExpr

parseFile : String -> Either String (List Expr)
parseFile = traverse (parseAll parseExpr) . lines

solve : List Expr -> Integer
solve = sum . map evaluate

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Right parsed = parseFile fileContent
    | Left err => printLn err
  putStrLn ("Solution : " ++ show (solve parsed))
