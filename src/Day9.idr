module Main
import System
import System.File
import Data.Strings
import Data.Vect
import Data.List
import Data.Nat
import Debug.Trace

data BigZipper : Nat -> Type -> Type where
  MkZipper : (prev : List a) -> (curr : Vect n a) -> (next : List a) -> BigZipper n a

fromList : (n : Nat) -> List a -> Maybe (BigZipper n a)
fromList k xs = pure $ MkZipper [] !(toVect k (take k xs)) (drop k xs)

nextz : BigZipper (S n) a -> BigZipper (S n) a
nextz (MkZipper prev curr []) = MkZipper prev curr []
nextz (MkZipper prev (y :: ys) (x :: xs)) =
  MkZipper (y :: prev) (rewrite sym $ plusCommutative n 1 in ys ++ [x]) xs

checkSum : {n : Nat} -> Vect n Int -> Int -> Maybe Int
checkSum xs x = find (==x) (allSums (toList xs) [])
  where
    allSums : List Int -> List Int -> List Int
    allSums [] acc = acc
    allSums (x :: xs) acc = allSums xs (acc ++ map (+ x) xs)

findNumber : BigZipper 25 Int -> Maybe Int
findNumber (MkZipper prev curr []) = Nothing
findNumber zip@(MkZipper prev curr (x :: xs)) =
  case checkSum curr x of
       Nothing => Just x
       (Just _) => findNumber (nextz zip)

parseFile : String -> Maybe (BigZipper 25 Int)
parseFile input =
  let intList = traverse parsePositive (lines input) in
      intList >>= fromList 25

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just parsed = parseFile fileContent
  putStrLn ("fail number: " ++ show (findNumber parsed))
