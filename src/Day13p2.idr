module Main

import System
import System.File
import Data.List
import Data.List1
import Data.Vect
import Data.Strings
import Data.Nat
import Data.Maybe
import Debug.Trace


traceID : Show a => a -> a
traceID x = trace (show x) x

input : List (Maybe Integer)
input = [Just 17,Nothing , Just 13, Just 19]

wikiInput : List (Maybe Integer)
wikiInput = [Just 3,Nothing , Nothing, Just 4, Just 5]

findMult : Int -> Int -> Int
findMult target n = (target `div` n) + 1

enumerate : List a -> List (a , Nat)
enumerate ls = zip ls [0 .. length ls]

GDCExt : Integer -> Integer -> (Integer, Integer)
GDCExt a b = let r = GDCHelper a b 1 0 0 1 in trace ("factors for " ++ show a ++ " and " ++ show b ++ " are " ++ show r) r
  where
    GDCHelper : (oldR, newR, oldS, newS, oldT, newT : Integer) -> (Integer, Integer)
    GDCHelper oldR 0 oldS newS oldT newT = (oldS, oldT)
    GDCHelper oldR newR oldS newS oldT newT =
      let q = oldR `div` newR in
          GDCHelper newR (oldR - q * newR) newS (oldS - q * newS) newT (oldT - q * newT)

modInv : Integer -> Integer -> Integer
modInv a b = fst $ GDCExt a b

pmod : Integer -> Integer -> Integer
pmod x y = if x < 0 then (x + y) `pmod` y else x `mod` y

chinese2 : (as, ns : List Integer) -> Integer
chinese2 as ns =
  let p = product ns
      nis = map (div p) ns
      crtM = zipWith (modInv) nis ns
      s = sum (zipWith3 (\a, b, c => a * b * c) as crtM nis)
   in s `pmod` p

partial
solve : List (Maybe Integer) -> Integer
solve numbers = let withIndex = enumerate numbers
                    filtered = catMaybes $ map (\(v, i) => map (\x => (x - cast {to=Integer} i,x)) v)
                                               withIndex
                 in chinese2 (map fst filtered) (map snd filtered)

parseFile : String -> Maybe (List (Maybe Integer))
parseFile input = let [n , nums] = lines input
                        | _ => Nothing in
                      do n' <- parsePositive n
                         let nums' = map parsePositive $ forget $ (split (==',') nums)
                         pure (nums')


partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just parsed = parseFile fileContent
  putStrLn ("Solution : " ++ show (solve parsed))
