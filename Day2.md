# Day 2 Part 1

Today's exercise is a simple filtering and counting function. Given a list of passwords
and for each of them a corresponding requirement we need to count the number of password
that pass the requirement.

Since idris doesn't have a `count` function, we need to implement one

```
count : (predicate : a -> Bool) -> List a -> Nat
count p = length . filter p
```

This means that counting amounts to getting the length of a filtered
list, with the filter removing elements that do not satisfy the predicate.

Now that we can count elements in a list we want to count the number of elements
that satify the policy. A function with this type should work

```
countPasswords : List (Policy, String) -> Nat
```

It takes a list of pairs of `Policy` and `String` and returns the number of
passwords that match the policy. We can implement this with `count` but we
also need a way to check the passwords are valid using the policy. So we are
going to use another function for that that will only perform the check:

```
isValidPassword : (Policy, List Char) -> Bool
```

We haven't defined our type `Policy` yet, but we know that it is composed of
3 things:

- the minimum number of occurences of a letter
- the maximum number of occurences of a letter
- the letter that should be within those constraints

For this we are going to use a record:

```
record Policy where
  constructor MkPolicy
  minimum : Nat
  maximum : Nat
  letter : Char
```

This record contains all three elements that define a suitable policy for our passwords,
now we just need to use it to check if our passwords are valid


```
isValidPassword : (Policy, List Char) -> Bool
isValidPassword ((MkPolicy minimum maximum letter), xs) =
  let c = count (== letter) xs in
      minimum <= c && c <= maximum
```

Unsurprisingly, it counts the number of occurences of `letter` by testing every letter in the
string with `== letter` and then checks that the count `c` is between `minimum` and `maximum`

You will notice two things, the firs tis tahtthe argument is a pair instead of being
`Policy -> List Char -> Bool`,

That is because it makes it easier to call later on when we are going ot check for every element in
our list of candidate passwords.

The second is that we use `List Char` instead of `String` and that is because `List Char`, though
less efficient than `String` allows us to use the normal list functions like `count` that we just
implemented.

Finally we can implement `countPasswords` by using `count` and `isValidPassword` together

```
countPasswords : List (Policy, String) -> Nat
countPasswords = count isValidPassword . map (map unpack)
```

`map (map unpack)` is a bit of pre-processing we do on the list of strings to transform every
password into a `List Char`, it has this type
`map (map unpack) : List (a, String) -> List (a, List Char)`. The inner `map` refers to the
`Functor` implementation on pair which maps the second element of the pair.
it has this type: `map : (a -> b) -> (x, a) -> (x, b)`.

Here you can see why `isValidPassword` has type `(Policy, List Char) -> Bool`, it's because
the list contains elements of type `(Policy, List Char) -> Bool`. If we could also use `uncurry`
to write `isValidPassword` in a more natural way:

```
isValidPassword : Policy -> List Char -> Bool
…

countPasswords : List (Policy, String) -> Nat
countPasswords = count (uncurry isValidPassword) . map (map unpack)
```

where `uncurry` transforms a function of 2 arguments into a function of 2 argument which is a pair.

The parsing in this program is quite complicated and frankly uninteresting. One noteworthy detail
that you will find in the code is that I make libera use of partial functions which is quite
unusual for a programming language that prides itself in correctness and proof guarantees.
This is because the scope of those challenges is very limited and the complexity introduced by
checking for all possible errors doesn't allow us to solve our problem more quickly. So lots of
functions might simply crash without notice.

# Part 2

For part 2 the gist of the logic is the same: count the number of password that satisfy a condition
however the condition is slightly different. We are going to reuse our `Policy` type, but now each
element has a slightly different meaning:

- `minimum` now means "the first index"
- `maximum` now means "the second index"
- `letter` still means "the letter we are interested in

The condition now becomes "the password is valid if the first index is equal to `letter`
or if the second index is equal to `letter` but they cannot be both equal to `letter`"

the type of `isValidPassword` doesn't change but its implementation does:

```
isValidPassword : (Policy, List Char) -> Bool
isValidPassword ((MkPolicy minimum maximum letter), word) =
  let firstMatches = unsafeIndex1 minimum word == letter
      secondMatches = unsafeIndex1 maximum word == letter
   in firstMatches /= secondMatches
```

Here we extract the first index with `unsafeIndex1` and test if this result is equal to `letter`.
We also extract the second index with `unsafeIndex1` and test if its result is equal to `letter`.

- If they are both true, this means both are equal to `letter` which means the password is not valid.
- If one of they is true, then the password is valid.
- If both are false, none of them are equal to `letter` which means the password is invalid.

In essence we are looking to check if `firstMatches` and `secondMatches` are _different_. If they
are then we're good, if they aren't then return `False`. This is exactly what `/=` does, it checks
if the two boolean values passed are _not the same_

I've introduced a function `unsafeIndex1` that allows us to extract the letter out of a list, given
its position in the list. The position start with `1`  that is why the name is postfixed with "1".
The implementation is quite straightforward, if unsafe:

```
-- This indexes a list from 1 and doesn't check for bounds
partial
unsafeIndex1 : Nat -> List a -> a
unsafeIndex1 (S Z) (x :: xs) = x
unsafeIndex1 (S k) (x :: xs) = unsafeIndex1 k xs
```

You will notice that the pattern matchin ignores a lot of valid cases, and that is why the function
is marked as "partial", "partial" means the funciton can fail if it reaches a case that is not
defined. The implementation returns the first element of a list if we are looking at the index `1`
(aka `S Z`), or it will recursively call itself with the rest of the list and the predecessor of the
index if it hasn't reached `1` yes (this is the `S k` case).

Idris code like this is strongly discouraged because it throws away all ability to write proofs
about such programs but in our case it's a good fit since we know that all strings are indexed
starting with `1` and that the input has been sanitised for us.

# Conclusion,

That's it! we can run and test those programs. I persoannly compiled them with

```
idris2 -o main Day2.idr
idris2 -o main2 Day2p2.idr
```
and ran them on my input with

```
build/exec/main input.txt
```

I've also streamed the entire process over at my [twitch channel](https://www.twitch.tv/identitygs/)
which I intend to do more of.

Don't hesistate to open an [issue](https://gitlab.com/avidela/aoc2020/-/issues) or
[Merge request](https://gitlab.com/avidela/aoc2020/-/merge_requests) if you have any question or
suggestion!




