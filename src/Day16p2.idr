module Main

import System
import System.File
import Data.SortedMap
import Data.SortedSet
import Data.Strings
import Data.List
import Data.Vect
import Data.List1
import Debug.Trace

Range : Type
Range = (Int, Int)

Ticket : Nat -> Type
Ticket n = Vect n Int

Rules : Nat -> Type
Rules n = Vect n (String, (Range, Range))

record Input (n : Nat) where
  constructor MkInput
  rules : Rules n
  myTicket : Ticket n
  nearby : List (Ticket n)

Show (Input n) where
  show (MkInput rules myTicket nearby) =
    show (rules) ++ (show myTicket) ++ (show nearby)

isValid : Int -> Range -> Bool
isValid x (y, z) = y <= x && x <= z

isValidTicket : List (Range) -> Ticket n ->  Bool
isValidTicket range = all (\n => any (isValid n) range)

RuleAssignement : Type
RuleAssignement = SortedMap String Nat

AssignedTicket : Nat -> Type -> Type
AssignedTicket n a = Vect n (List a)

getSolution : List (String, Int) -> Int
getSolution = product . map snd . filter (isPrefixOf "departure" . fst)

-- return the rules that can apply to each field
getValidRules : List (String, (Range, Range)) -> Ticket n ->  AssignedTicket n String
getValidRules rules ticket =
      map getFilteredRules ticket
   where
     getFilteredRules : Int -> List String
     getFilteredRules val =
       map fst $ filter (\(_, (r1, r2)) => isValid val r1 || isValid val r2) rules

Traversable List1  where
  traverse f (head ::: tail) = [| f head ::: traverse f tail |]

enumerated : Num i => Vect n a -> Vect n (i, a)
enumerated x = enumerateAcc x 0
  where
    enumerateAcc : Vect m a -> i -> Vect m (i, a)
    enumerateAcc [] x = []
    enumerateAcc (y :: xs) x = (x, y) :: enumerateAcc xs (x + 1)

sortBy : (a -> a -> Ordering) -> Vect n a -> Vect n a
sortBy f xs = believe_me $ Vect.fromList $ sortBy f (toList xs)

partial
filterDownAssignement : (Show a, Ord a, Eq a) => {n : Nat}
                     -> Vect m (AssignedTicket n a) -> (Vect n a)
filterDownAssignement tickets =
  let (S k ** rows) = filter (not . any isNil) tickets
      columns = transpose rows
      mapped = map (filterField . snd) (enumerated columns)
      indexedCandidates = sortBy (\l, r => length (snd l) `compare` length (snd r)) (enumerated mapped)
      filteredDown = mapCandidates indexedCandidates
      sortedCandidates = sortBy (\a, b => fst a `compare` fst b) filteredDown
   in map snd sortedCandidates
  where
    mapCandidates : forall l, a. Eq a => Vect l (Int, List a) -> Vect l (Int, a)
    mapCandidates [] = []
    mapCandidates ((index, [x]) :: xs) = (index, x) :: mapCandidates (map (map $ filter (/= x)) xs)

    filterField : Vect (S l) (List a) -> List a
    filterField (head :: tail) = foldr (\ls, ks => ks `intersect` ls) head tail

partial
solve : { n : Nat} -> Input n -> Int
solve (MkInput rules myTicket nearby) =
   let asVect = Vect.fromList (myTicket :: nearby)
       withRules = map (getValidRules (toList rules)) asVect
       uniq = filterDownAssignement $ map (getValidRules (toList rules)) asVect
    in getSolution (toList $ zip uniq myTicket)

parseFile : String -> Maybe (n ** Input n)
parseFile input = let lines = lines input
                      [rules, [_, ticket], (_ :: nearby)] = forget $ split (=="") lines
                        | _ => Nothing
                   in do (m ** rules) <- parseRules rules
                         ticket <- parseTicket m ticket
                         nearby <- parseNearby nearby m
                         pure (_ ** MkInput rules ticket nearby)
  where
    parseRange : String -> Maybe Range
    parseRange x = let [low, high] = forget $ split (=='-') x
                         | _ => Nothing
                    in [| (parsePositive low, parsePositive high) |]

    parseRule : String -> Maybe (String, (Range, Range))
    parseRule x = let [name, ranges] = forget $ split (==':') x
                        | _ => Nothing
                      [r1, _, r2] = words ranges
                        | _ => Nothing in
                      (name, ) <$> [| MkPair (parseRange r1) (parseRange r2) |]

    parseRules : List String -> Maybe (m ** Vect m (String, (Range, Range)))
    parseRules xs = do rules <- traverse parseRule xs
                       pure $ (_ ** fromList rules)

    parseTicket : (k : Nat) -> String -> Maybe (Ticket k)
    parseTicket size x = (traverse parsePositive $ forget $ split (==',') x)
                     >>= toVect size

    parseNearby : List String -> (k : Nat) -> Maybe (List (Ticket k))
    parseNearby lines size = traverse (parseTicket size) lines

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just (_ ** parsed) = parseFile fileContent
  putStrLn ("Solution : " ++ show (solve parsed))
