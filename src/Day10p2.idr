module Main

import System
import System.File
import Data.Strings
import Data.List
import Data.List1

||| Tail-recursive accumulator for factItr.
factAcc : Int -> Int -> Int
factAcc 0 acc = acc
factAcc n acc = factAcc (n - 1)$ n * acc

||| Iterative definition of factorial.
factItr : Int -> Int
factItr n = factAcc n 1

choose : Int -> Int -> Int
choose n k = foldl (\acc, i => (acc * (n + 1 - i)) `div` i) 1 [1 .. k]

choose' : Int -> Int -> Int
choose' n k = if k > n then 1
                       else n `choose` k

mapDifferences : List Int -> List Int
mapDifferences xs = zipWith (-) xs (0 :: xs)

countDifferences : List Int -> Int
countDifferences xs =
      let diffs = mapDifferences xs
          ones = cast $ length $ filter (==1) diffs
          thrs = cast $ length $ filter (==3) diffs
       in ones * ( 1 + thrs)

getOnes : List Int -> List Nat
getOnes = filter (>1) . map length . forget . splitOn 3

listOfCombinations : List Nat -> Int
listOfCombinations = product . map (`choose` 2) . map cast

parseFile : String -> Maybe (List Int)
parseFile input = sort <$> traverse parsePositive (lines input)

partial
solveInput : List Int -> Int
solveInput input  =
  let ones =  getOnes $ mapDifferences input in
      product $ map ((+ 1) . (`choose` 2) . cast) ones

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just parsed = parseFile fileContent
  putStrLn ("number of permutations: " ++ show (solveInput parsed))
