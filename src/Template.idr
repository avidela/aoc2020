module Main

import System
import System.File

solve : Nat -> Nat

parseFile : String -> Maybe Nat


partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just parsed = parseFile fileContent
  putStrLn ("Solution : " ++ show (solve parsed))
