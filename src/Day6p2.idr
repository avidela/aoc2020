module Main

import Data.List
import Data.List1
import Data.Strings
import System
import System.File

getGroups : String -> List (List (List Char))
getGroups = map (map unpack) . forget . split (== "") . lines

countEverything : (merge : List (List Char) -> List Char) -> String -> Nat
countEverything merge = sum . map (length . merge) . getGroups

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  putStrLn ("total tally intersect : " ++ show (countEverything intersectAll fileContent))
