module Main
import System
import System.File
import Data.Strings
import Data.Vect
import Data.List
import Data.List1
import Data.Nat
import Data.Fin
import Data.Either
import Debug.Trace

data BigZipper : Nat -> Type -> Type where
  MkZipper : (prev : List a) -> (curr : Vect n a) -> (next : List a) -> BigZipper n a

fromList : (n : Nat) -> List a -> Maybe (BigZipper n a)
fromList k xs = pure $ MkZipper [] !(toVect k (take k xs)) (drop k xs)

nextz : BigZipper (S n) a -> BigZipper (S n) a
nextz (MkZipper prev curr []) = MkZipper prev curr []
nextz (MkZipper prev (y :: ys) (x :: xs)) =
  MkZipper (y :: prev) (rewrite sym $ plusCommutative n 1 in ys ++ [x]) xs

allSums : List Int -> List Int -> List Int
allSums [] acc = acc
allSums (x :: xs) acc = allSums xs (acc ++ map (+ x) xs)

checkSum : {n : Nat} -> Vect n Int -> Int -> Maybe Int
checkSum xs x = find (==x) (allSums (toList xs) [])

findNumber : BigZipper 25 Int -> Maybe Int
findNumber (MkZipper prev curr []) = Nothing
findNumber zip@(MkZipper prev curr (x :: xs)) =
  case checkSum curr x of
       Nothing => Just x
       (Just _) => findNumber (nextz zip)

subList : List a -> (lo, hi : Nat) ->  List a
subList ls lo hi = take (hi `minus` lo) (drop lo ls)

sumRange : (lo, hi : Nat) -> List Int -> Int
sumRange lo hi xs = sum (subList xs lo hi)

parameters (target : Int, xs : List Int)
  findContiguous : (lo, hi : Nat)
                -> (Nat, Nat)
  findContiguous lo hi =
    let sum = sumRange lo hi xs  in
        case compare target sum of
             EQ => (lo, hi)
             GT => findContiguous lo (S hi)
             LT => findContiguous (S lo) hi

computeWeakness : List Int -> Maybe Int
computeWeakness = map (uncurry (+) . minMax) . fromList
  where
    minMax : Ord a => List1 a -> (a, a)
    minMax = foldl1 (\x => (x, x)) (\(mi, ma), y => (min mi y, max ma y))


findWeakness : (target : Int) -> List Int -> Maybe Int
findWeakness target xs = computeWeakness . uncurry (subList xs) $ findContiguous target xs 0 1

parseList : String -> Maybe (List Int)
parseList input = traverse parsePositive (lines input)

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just list = parseList fileContent
  let Just num = fromList 25 list >>= findNumber
  putStrLn ("weakness number: " ++ show (findWeakness num list))
