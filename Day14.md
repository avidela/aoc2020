# Day14 Part 1

Today's problem is another computer simulation problem, this time the
instructions are either:

- Change the current mask.
- update an address in memory.

Just like before our solving function will take a list of instructions,
an initial state and execute the program until it runs out of instructions
and then return the final state.

Our program state is a pair containing the current mask and the current
state of the memory. For the mask we are going to use a vector of 36
elements and each element will be either a `0`, `1` or an unspecified
value. This is the perfect opportunity to use `Maybe Bool` with `Nothing`
for "unspecified", `Just True` for "1" and `Just False` for "0".

The values we are manipulating are 36 bit binary numbers so for this purpose
we are also going ot define a type alias for vectors of 36 booleans.

The state of the memory is then a dictionary (or `SortedMap` in Idris) of
numbers for the addresses and `Binary` for the values. Since we need to
translate from decimal numbers into 36-bit binary numbers we also are
going to implement `fromBinary` and `toBinary` functions for numbers.

Finally to execute our program we need to define what the instructions are, for
this we are going to declare a data type with two fields. Either an instruction
is a new mask, or an instruction is a memory update.

So far this is what we have:

```
Mask : Type
Mask = Vect 36 (Maybe Bool)

Binary : Type
Binary = Vect 36 Bool

interface BinaryConversion t where
  fromBin : Binary -> t
  toBin : t -> Binary

data Instruction = NewMask Mask | MemOp Int Binary
```

Implementing `BinaryConversion` for `Int` and `Nat` is pretty boring so
I won't detail the implementation here.

Once we have a way to define the values we manipulate we can start implementing
our "computing" function that will execute our list of instructions. This function
that we will call `runProgram` takes a list of instructions, a program state
and returns the final memory state.

```
runProgram : List Instruction -> (SortedMap Int Binary, Mask)
         -> SortedMap Int Binary
runProgram [] x = fst x
runProgram ((NewMask m) :: xs) (x, z) = runProgram xs (x, m)
runProgram ((MemOp addr val) :: is) (mem, mask) =
  runProgram is (insert addr (applyMask mask val) mem, mask)
```

Its implementation is pretty staightforward, if we run out of instructions we retun the
memory state, if we find a new mask, we keep going but update the mask in our state. If
we find an memory update instruction we update the memory state and keep going.

Updating the memory state requires us to apply the mask before storing the value in memory.
Thankfully, thanks to out very simple encoding of masks, our implementation is almost too
easy to write: for every value of the mask, if its a `Nothing` we ignore it, if it a `Just`
we return the value of the mask.

```
applyMask : Mask -> Binary -> Binary
applyMask xs ys = zipWith (\m, b => fromMaybe b m) xs ys
```

The final step in order to solve the problem is to compute the sum of all values in memory,
we can achieve that with a simple fold over the memory dictionary:

```
solve : List Instruction -> Nat
solve instructions = foldr (\x, acc => acc + fromBin x) 0
                           (runProgram instructions (empty, replicate 36 Nothing))
```

This function takes a list of instructions, executes them using an initial state composed
of an empty memory and an empty mask, and folds over the resulting `SortedMap` by
interpreting every binary number into a `Nat` and summing them up.

Running `solve` on the parsed input will give us the correct answer. Note that while we haven't
seen any use of them we use `toBinary` functions to parse the input correctly and generate
a list of instructions that contain binary vectors rather than plain `Int`s.

# Part 2

Part 2 only requires a couple changes. Instead of using the mask to change the value inserted
in memory, we use it to generate a list of memory locations where to insert the given number.
This means that we need to update our `runProgram` function to generate all the memory
addresses from the mask and to store them. In addition we need a function to generate those
addresses given the current address and the current mask. Let us start with this one.

`applyMask` now  returns a list of addresses in binary `List Binary`, the way the mask
is applied is different too. Instead of ignoring the missing values and overwriting the
present ones, a `0` in the mask means "ignore" and a `1` means "overwrite". The missing values
in the mask are now used to generate a list of possible addresses. That means that the
mask `[x,0,0,x]` generates 4 addresses, 1 for each possible value of the two missing `x`
values: `[0,0,0,0]`, `[1,0,0,0]`, `[0,0,0,1]` and `[1,0,0,1]`. We implement the two stages,
masking the address and generating the new addresses, like so:

```
generateFloating : Vect n (Maybe Bool) -> List (Vect n Bool)
generateFloating [] = [[]]
generateFloating (Nothing :: xs) = let rec = generateFloating xs in
                                        map (True ::) rec ++ map (False ::) rec
generateFloating ((Just x) :: xs) = map (x :: ) (generateFloating xs)

applyMask : Mask -> Binary -> List Binary
applyMask mask bin =
  let masked = zipWith (\m, b => map (\case True => True; False => b) m) mask bin
   in generateFloating masked
```

`generateFloating` is responsible for generating the list of possible addresses,
we see that is simply calls itself recursively and store the result in `rec`
then duplicates the smaller recursive list and prepends `True` and `False` onto each
sublists. And finally concatenates the two sublists together, both representing
the universe where the current `x` was `True` and one where it was `False`.

`applyMask` overwrites the input when contains `Nothing` and when it contains
`Just True` and otherwise uses the value from the address. Then vector is used to
generate all the addresses.

The last step is to change our `runProgram` in order to transform our address into binary.
generate all the addresses from it and the mask. Then write the number we're instructed
to write onto all the generated addresses. This changes only the `MemOp` branch of the function
and looks like this:

```
runProgram ((MemOp addr val) :: is) (mem, mask) =
  let addresses : List Binary = applyMask mask (toBin addr ) -- generate addresses
      newMemory = foldr (\addr => insert (fromBin addr) val) mem addresses -- write
   in runProgram is (newMemory, mask) -- keep going
```

and again calling `solve` will run our program with the list of instruction given and
return the sum of all the addresses sorted in memory.

