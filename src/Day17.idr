module Main

import System
import System.File
import Data.List
import Data.Vect
import Data.Strings

data Cube = Active | Inactive

Eq Cube where
  Active == Active = True
  Inactive == Inactive = True
  _ == _ = False

Board : Nat -> Type -> Type
Board Z elem = elem
Board (S n) elem = List (Board n elem)

Coordinate : Nat -> Type
Coordinate n = Vect n Int

count : Eq a => (a -> Bool) -> List a -> Nat
count f = length . filter f

indexInt : Int -> List a -> Maybe a
indexInt 0 (x :: xs) = Just x
indexInt n (x :: xs) = indexInt (n - 1) xs
indexInt n _ = Nothing

indexN : Coordinate n -> Board n a -> Maybe a
indexN [] x = Just x
indexN (idx :: xs) board =
 indexInt idx board >>= indexN xs

getClosest : Board n a -> (curr, direction : Coordinate n) -> Maybe a
getClosest board curr direction =
  let newPosition = zipWith (+) curr direction in
      indexN newPosition board

zipWithIndex : List a -> List (Int, a)
zipWithIndex xs = zip ([0 .. (cast $ length xs)]) xs

genCoordN : (n : Nat) -> List (Coordinate n)
genCoordN 0 = [[]]
genCoordN (S k) = let rec = genCoordN k
                      n1 = map (-1::) rec
                      n2 = map ( 0::) rec
                      n3 = map ( 1::) rec in n1 ++ n2 ++ n3

zero : (n : Nat) -> Coordinate n
zero n = replicate n 0

getAdjacent : {n : Nat} -> (cordinates : Coordinate n) -> Board n a -> List a
getAdjacent coord board =
  let directions : List (Coordinate n)
      directions = filter (/= zero n) (genCoordN n)
   in catMaybes $ map (getClosest board coord) directions

updateCube : Cube -> (neighbors : List Cube) -> Cube
updateCube Active n = let actives = count (== Active) n in
                          if actives == 2 || actives == 3
                             then Active
                             else Inactive
updateCube Inactive xs = if count (==Active) xs == 3 then Active
                                                     else Inactive

playGame : Board 3 Cube -> Board 3 Cube
playGame board =
  map (\(i, row) => map (\(j, line) => map (\(l, elem) =>
                   updateCube elem (getAdjacent [i,j,l] board) )
      (zipWithIndex line))
      (zipWithIndex row))
      (zipWithIndex board)

iterate : Nat -> Board 3 Cube -> Board 3 Cube
iterate 0 board = board
iterate (S k) board = iterate k (playGame board)

emptyLayer : Board 2 Cube
emptyLayer = replicate 20 (replicate 20 Inactive)

countActives : Board 3 Cube -> Nat
countActives = count (== Active) . join . join

center : Board 2 Cube -> Board 2 Cube
center xs = replicate 7 (replicate 20 Inactive) ++
   map (\ls => replicate 6 Inactive ++ ls ++ replicate 7 Inactive) xs
   ++ replicate 6 (replicate 20 Inactive)

insertInput : Board 2 Cube -> Board 3 Cube
insertInput xs = let c = center xs in
  replicate 7 emptyLayer ++ pure c ++ replicate 8 emptyLayer

parseFile : String -> (Board 2 Cube)
parseFile = map parseLine . lines
  where
    parseLine : String -> (List Cube)
    parseLine = map (\case '#' => Active; _ => Inactive) . unpack


partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let parsed = parseFile fileContent
  putStrLn ("Solution : " ++ show (countActives $ iterate 6 (insertInput parsed)))
