# Day 4 Part 1

Today's exercise is pretty simple but involves one of the most boring tasks in programming:
parsing and validating some data.

The problem can be sumarised this way:

> Given a string representing a record, ensure each field of the record
> is present within the string.

We're gonna first define our record type

```
record PassportData where
  constructor MkPassport
  birthYear : String
  issueYear : String
  expirationYear : String
  height : String
  hairColor : String
  eyeColor : String
  passportID : String
  countryID : Maybe String
```

This represents each field that we are interested in. The fields have this format inside
the string : `field:value`, for a record to be valid, each of the fields need to appear
once. One way to think about this structure is as a list of pairs `List (String, String)` but
since our goal is to check that each field is valid, we are going to use `SortedMap String String`
and its `lookup` function instead.

Using `SortedMap String String` we can now represent our problem with the follwing type signature:

```
checkPassport : SortedMap String String -> Maybe PassportData
```

Arguably this is a bit more than the problem asks for. Indeed, we are only asked to
check if a passport is _valid_, we are not required to construct a passport out of the data
given to us.

However, it is good practice to _parse_ the data rather than simply return a boolean
that indicates if the data is valid or not. One of the benefits of parsing is that our programs
now tells us _in which way_ our input is valid. Constrast `validate : String -> Bool` with
`parse : String -> Maybe Passport` the first one does not tell us anything about the string
passed in argument, where the second one tells us that it represents a `Passport` (assuming it has
the correct format).

Building a `Passport` out of ` SortedMap` amounts to looking up each of the keys and failing if
we cannot find one. For this we are going to use _do-notation_ with `Maybe`

```
checkPassport : SortedMap String String -> Maybe PassportData
checkPassport dataMap = do
  byr <- lookup "byr" dataMap
  iyr <- lookup "iyr" dataMap
  eyr <- lookup "eyr" dataMap
  hgt <- lookup "hgt" dataMap
  hcl <- lookup "hcl" dataMap
  ecl <- lookup "ecl" dataMap
  pid <- lookup "pid" dataMap
  let cid = lookup "cid" dataMap
  pure $ MkPassport byr iyr eyr hgt hcl ecl pid cid
```

Here, each `<-` means "extract the value out of the maybe, and fail if it doesn't find any" so
`byr <- lookup "byr" dataMap` means "lookup the value associated with the key `"byr"` and put it
in the variable `byr`.

The last step `cid` simply uses the result of `lookup "cid"` without performing any check. In terms
of validation this step could be skipped, but for parsing it is essential in order to
define a correct value of `Passport`, since our `Passport` has a `countryID` field of type
`Maybe String`

Now that we can check that a `SortedMap` represents a `Passport` we can write a `countValidPassports`
function that will take a list of `SortedMap` and return how many of them actually define a valid
passport with

```
countValidPassports : List (SortedMap String String) -> Nat
countValidPassports passports = count isJust (map checkPassport passports)`
```

`isJust` is a function of the module `Data.Maybe` which returns `True` if a `Maybe` value is a `Just`.

The rest of the program is boring parsing busywork in order to extract a `SortedMap String String`
out of our `String` input. The function `processPassports : String -> List (SortedMap String String)`
is split into four parts:

- split the inputs into lines
- bundle each consecutive lines using empty lines as separator
- for each bundle of line, map it into a list of key-value pairs (1 for each field)
- once we have a list of lists of key-value pairs we can map accross to build a list of `SortedMap`
instead, which we will transform it into a `List (SortedMap String String)`.

Our main function simply reads the content of the file, transforms it into a
`List (SortedMap String String)` which in turns allows us to call `countValidPassports` to get
the number of valid passports.

# Part 2

In this second part we are asked the same thing except we need to filter our passports further by
checking that the sales are valid. There aren't many changes but they are a bit tedious. The most
important one is in `checkPassport` and in the `Passport` data declaration.

Our passport now takes `Int`s for every field that is a number, and we add additional parsing
functions to `checkPassport`

```
record PassportData where
  constructor MkPassport
  birthYear : Int
  issueYear : Int
  expirationYear : Int
  height : Int
  hairColor : String
  eyeColor : String
  passportID : Int -- List (Fin 10)with (_)
  countryID : Maybe String

checkPassport : SortedMap String String -> Maybe PassportData
checkPassport dataMap = do
  byr <- lookup "byr" dataMap >>= checkNumber 1920 2002 4
  iyr <- lookup "iyr" dataMap >>= checkNumber 2010 2020 4
  eyr <- lookup "eyr" dataMap >>= checkNumber 2020 2030 4
  hgt <- lookup "hgt" dataMap >>= checkHeight
  hcl <- lookup "hcl" dataMap >>= checkColor
  ecl <- lookup "ecl" dataMap >>= checkEyeColor
  pid <- lookup "pid" dataMap >>= checkNumber 0 999999999 9
  let cid = lookup "pid" dataMap
  pure $ MkPassport byr iyr eyr hgt hcl ecl pid cid
```

you will notice the only difference with before is the addition to `>>= *a function call*` which means
if you find a value, then parse if with the following function". For parsing numbers I've defined a
general `checkNumber` function that takes a lower bound, an upper bound and the number of digits expected
in that number. Got the 3 other fields I've written specific parsers that manipulate the string passed in
input and return it if it does correspond to a valid field value.

The parsing functions are as follows:


```
checkNumber : (lower, upper : Int) -> (digit : Nat) -> String -> Maybe Int
checkNumber lower upper digit str =
  let chars = unpack str in
      parsePositive str
      >>= (\i => toMaybe (lower <= i && i <= upper && (length chars == digit)) i)


checkEyeColor : String -> Maybe String
checkEyeColor "amb" = Just "amb"
checkEyeColor "brn" = Just "brn"
checkEyeColor "blu" = Just "blu"
checkEyeColor "gry" = Just "gry"
checkEyeColor "grn" = Just "grn"
checkEyeColor "hzl" = Just "hzl"
checkEyeColor "oth" = Just "oth"
checkEyeColor _ = Nothing

checkHeight : String -> Maybe Int
checkHeight str =
  let (digits, unit) = break (not . isDigit) str in
      case unit of
           "cm" => checkNumber 150 193 3 digits
           "in" => checkNumber 59 76 2 digits
           _ => Nothing

checkColor : String -> Maybe String
checkColor str = case unpack str of
                      ('#' :: color) => toMaybe (all isHexDigit color && length color == 6) str
                      _ => Nothing
```

It is worth noting that we are fortunate that the sizes in `cm` and `in` both have a consistent number
of digits, such that we can reuse `checkNumber`. In addition you will notice the use of `toMaybe` which
is also a function of the module `Data.Maybe` and returns a `Just` whenever the predicate is valid or
`Nothing` when it isn't, it has this type signature : `toMaybe : Bool -> a -> Maybe a`. I also use
standard library functoins like `isDigit` and `isHexDigit` in order to parse characters that are in the
range `0-9` and `0-9a-f` respectively.


## A note on debugging

While working on this, a small bug infiltrated my code and made me return the wrong number. In order to
debug it I wanted to write `printLn` statements to cehck where a valid passport was rejected, but how
can I do this when everything is pure? Well there is no way araound it: make it impure. So I changed the
signtature of `countValidPassport` to `List (SortedMap String String) -> IO Nat` and printed out
whenever a passport was rejected with the following implementation

```
countValidPassports passports = do
   array <- traverse (\x => let v = checkPassport x in when (isNothing v) (printLn x) *> pure v) passports
   pure $ count isJust array
```

The printing is handled by the line `when (isNothing v) (printLn x)` which prints `x` only when the
passport cannot be parsed and results in `Nothing`.
