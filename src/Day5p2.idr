module Main

import System
import System.File
import Data.List
import Data.Strings

%default total

pow : Nat -> Nat -> Nat
pow x 0 = 1
pow x (S k) = x * (x `pow` k)

-- F  B  F  B  B  F  F
-- 0  1  0  1  1  0  0
-- 64 32 16 8  4  2  1
-- 0  32 0  8  4  0  0
-- 32 + 8 + 4 +
toNumber : List Nat -> Nat
toNumber binary =
  sum $ zipWith (*) (reverse binary) (map (\x => 2 `pow` x) [0 .. (length binary)])

toBinary : (Char -> Nat) -> List Char -> List Nat
toBinary = map

computeID : List Char -> Nat
computeID = toNumber . toBinary (\case 'F' => Z
                                       'L' => Z
                                       _   => 1)

max : Ord a => (ls : List a) -> {auto check : NonEmpty ls} -> a
max [x] = x
max (x :: (y :: xs)) = Prelude.max x (Main.max (y :: xs))

-- credit @shinkage
findHole : List Nat -> Maybe Nat
findHole (x :: xs@(y :: ys)) =
  if S x /= y
     then Just (S x)
     else findHole xs
findHole _ = Nothing

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let seats = findHole $ sort $ map (computeID . unpack) $ lines fileContent
  putStrLn ("maximum ID: "  ++ show seats)


