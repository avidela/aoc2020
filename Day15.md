# Day 15 Part 1 & 2

Day 15 have us solve the same problem twice with two different inputs.
In this exercise we are asked to find the 2020th element of a list that
is generated with the following rules:

- The first n elements are given
- the `n+1`th element is difference between the index of the last element
and it index of it last appearance in the list.
- if the last element has never appeared in the list before, return `0`

The naive way to implement this would be to do a list lookup of the numbers
already generated, check if the last number is there, if it is, return the
index of its last occurence and then do `lastIndex - foundIndex` and add
this to the list.

The flaw in this approach is that we need to do a list lookup for every number
and that is notoriously slow. Instead we are going to store each element
in a dictionary with each key corresponding to a number we saw in the past
and each value being the last index it's been seen at.

This way, we can do dictionary lookup instead of list lookup. A missing
key in the dictionarry means the value has never been seend and a found
value means it has been seen and the value is the last index.

First of all we need to generate our list of numbers, in Idris we would do
that with an infinite stream. We initialise our infinite stream with our
starter list of elements and then we can keep asking for more elements
out of our list using `drop` and `head`

```
generateStream : List1 Int -> Stream Int

solve : List1 Int -> Nat -> Int
solve x k = Stream.head $ drop (k `minus` (S $ length x)) $ generateStream x
```

To solve the problem we drop the first `n` elements and take the next one with `head`
droping the first `n` elements requires some arithmetic because our stream starts
after the initial list. Actually it starts after the inital list, minus one as we will
see later.

Generating an infinite steram is usuall done this way: first generate the next element,
then prepend it to the rest of the infinite stream. This way, we will always have elements
to generate and the stream will never finish.

Generating the next element can be done given some information about the stream we've
previously generated. Namely we need the dictionary of numbers already seen and their
index, the last value we saw, and the index of the last value.

```
-- The dictionary that countains the elements as key and the index of their last
-- appearance as value
CountState : Type
CountState = SortedMap Int Int

next : (prevValue, prevIndex : Int) -> CountState -> Int
next prevValue prevIndex state =
  case lookup prevValue state of
       Just numIndex => prevIndex - numIndex
       Nothing => 0
```

Implementing `next` amounts to implementing the logic we wrote in prose a the begining,
that is, the next element is the difference between the last number's index and the index
of its last appearance. Or `0` if its the first time we see it. Again this could be shortened
as `maybe 0 (prevIndex -) (lookup prevValue state)`

Now that we have a way to generate the next element of our stream, we can generate
our stream proper. The stream will require access to the dictionary of previous
elements visited and the last value and its index. It will then call `next` in order
to find out what is the next element, and call itself recursively with the new state
(which now contains the last value we encountered) and the value we just computed as
"last value seen".

```
generateNumbers : CountState -> (prevValue, prevIndex: Int)  -> Stream Int
generateNumbers state prev prevIdx =
  let newNumber = next prev prevIdx state
      newState = insert prev prevIdx state in
      newNumber :: generateNumbers newState newNumber (1 + prevIdx)
```

Here the `::` operator appends an element to our infinite stream of values, and the
right side of it represents the rest of all the values left to generate.

Finally we can hook `generateNumbers` to `generateStream` to get our answer.
`generateStream` will simple do some processing on the initial list in order to build
the inital state and then call `generateNumbers` to return the infinite stream.

The pre-processing consists in extracting the last value of the list and its index,
and constructing the dictionary out of the first part of the list without its last
element. Those three values are then used as inital values for `generateNumbers`

```
generateStream : List1 Int -> Stream Int
generateStream xs = let last = last xs -- the last element
                        elems = dropLast (forget xs) -- the list without its last element
                        startingState = buildState (List.reverse elems) -- the inital state
                        index = (cast {to=Int} $ length elems) -- the index of the last element
                     in generateNumbers startingState last index
```

And with this we are done! Calling `generateStream` with a list of numbers generates
the list of numbers, and we can get the `nth` element by doing `drop` and `head`
on the infinite stream. There is a bit of arithmetic involved because the infinite
stream starts _after_ the inital list but otherwise the idea is simple:

```
solve : (inital : List1 Int) -> (count : Nat) -> Int
solve initial count = Stream.head $ drop (count `minus` (S $ length inital))
                                  $ generateStream initial
```
