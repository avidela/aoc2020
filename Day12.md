# Day 12 Part 1

In part one of today's exercise we need to move a cursor on a 2D plane and
then compute the sum of its coordinates.

The problem is phrased in terms of moving a boat using a list of instructions, moving
the boat can be done with one of three instructions: Moving toward a cardinal
direction, changing the way the boat is facing and moving foward.

For this we are going to declare a data type that represents the instructions
to follow, as well as one for cardinal directions and one for turns.

```
data Turn = Left | Right | Around

data Cardinal = N | S | E | W

data Move = C Cardinal Nat | T Turn | F Nat
```

`Move` are our instructions, we now need to define our boat and a function to move
our boat:

```
-- The state of the boat is captured by is coordinates and the direction its facing
BoatState : Type
BoatState = ((Int, Int), Cardinal)

moveBoat : Move -> BoatState -> BoatState
```

In order to move the boat we need to inspect the `Move` and correctly update its state.
If the boat faces east and is given a "forward 10" instructions we need to move it east
10 tiles. If its facing north, we need to move it north 10 tiles. If we see a "turn left"
instruction we need to keep its position the same but change is direction. If we see
a "North 10" instruction we need to move it north by 10 tiles without changing its direction.

In order to implement this we need a could of auxillary functions: One to add coordinates
together, one to change direction given a turn and an orientation, and finally, one to compute the movement
vector given a direction and a length.


```
addP : Num n => (a, b : (n, n)) -> (n, n)
addP (x, y) (z, w) = (x + z, y + w)

getVector : Cardinal -> Nat -> (Int, Int)
getVector N k = (0              , cast k)
getVector S k = (0              , negate (cast k))
getVector E k = (cast k         , 0)
getVector W k = (negate (cast k), 0)

changeDirection : Turn -> Cardinal -> Cardinal
changeDirection Around N = S
changeDirection Around S = N
changeDirection Around E = W
changeDirection Around W = E
changeDirection Left N = W
changeDirection Left S = E
changeDirection Left E = N
changeDirection Left W = S
changeDirection Right N = E
changeDirection Right S = W
changeDirection Right E = S
changeDirection Right W = N

moveBoat : Move -> BoatState -> BoatState
moveBoat (C c l) (coord, dir) = (getVector c l `addP` coord, dir)
moveBoat (T turn) (coord, dir) = (coord, changeDirection turn dir)
moveBoat (F k) (coord, dir) = (getVector dir k `addP` coord, dir)
```

Those implementation are pretty straight forward if not a bit verbose, specially for `changeDirection`.
A way to make this code shorter would be to encode the direction to `Int` and use modulo arithmetic
to change the direction. Bit this wouldn't make for idiomatic Idris code, indeed it would make the program
substantially harder to prove correct.

Now that we can move our boat given a single instruction we can finally solve our overall problem: follow
all of the given instructions and compute the sum of the coordinates to get our answer. Following
the instructions is simple a fold over the instructions using `((0, 0), E)` as initial state
and `moveBoat` as our state-updating function.

```
solve : List Move -> Int
solve moves =
  let ((x, y), _) = foldr moveBoat ((0, 0), E) moves in
      abs x + abs y
```

Calling `solve` after parsing our input gives us the correct answer, notice we use the absolute
value of each coordinate before adding them together!

# Part 2

Part 2 only requires a couple of changes, instead of using the orientation of the boat to move
forward, we are going to use the position of a waypoint which defines our vector for movement,
every cardinal movement now acts on the waypoint rather than the boat and every forward movement
moves the boat in the direction of the waypoint, multiplied by a scalar.

For this to work we only need to change two things, our state is now two coordinates, rather than a
coordinate and a direction. And our `moveBoat` function which now moves the waypoint and the boat
at the same time. To recap the rules:

- If we encounter a cardinal movement we move the waypoint but not the boat
- If we encounter a forward movement of `x` we move the boat by the vector defined
by the position of the waypoint multiplied by `x`.
- If we encounter a turn movement we rotate the waypoint around the boat by the angle
of the turn.

```
moveBoat : Move -> BoatState -> BoatState
moveBoat (C c l) (coord, wayp) = (coord, getVector c l `addP` wayp)
moveBoat (T turn) (coord, wayp) = (coord, turnWaypoint turn wayp)
moveBoat (F k) (coord, wayp) = (((cast k) `scalar` wayp) `addP` coord, wayp)
```

This implementation makes uses of a number of auxillary functions, including `getVector` and `addP` from
part 1. we've added `turnWaypoint` which updates the coordinates of the waypoint around the boat
by flipping and negating its coordinates accordingly:

```
turnWaypoint : Turn -> (Int, Int) -> (Int, Int)
turnWaypoint Left (x, y) = (negate y, x)
turnWaypoint Right (x, y) = (y, negate x)
turnWaypoint Around wayp = (-1) `scalar` wayp
```

We can not call again our `solve` function which hasn't changed and get the new manathan distance
using those new movement rules.
