# Day 11 Part 1

Thankfully for my time management, today's challenge was conceptually simple.

This problem is extremely similar to what is called a
[cellular automaton](https://en.wikipedia.org/wiki/Cellular_automaton), that is, a board and
a set of rules to update this board. Every update of the board is understood as being a
"unit of time". Those automatons have fascinating properties that emerge from their rules
and modifying them to find out new properties is half the fun (the other half is finding
interesting programs within a given set of rules).

Today's cellular automaton has the following rules:

- A cell is either `Empty`, `Free` or `Occupied`
- A cell that is `Empty` will never change its state
- A cell that is `Free` will become occpied if all its neighbors are not `Occupied`
- A cell that is `Occupied` will become `Free` if 4 of more of its neighbors are `Occupied`

We can model this behaviour with the following program:

```
data Tile = Empty | Free | Occupied

Eq Tile where
  Empty == Empty = True
  Free == Free = True
  Occupied == Occupied = True
  _ == _ = False

updateTile : Tile -> List Tile -> Tile
updateTile Empty neighbors = Empty -- empty never changes state
updateTile Free neighbors =
    if all (/=Occupied) neighbors then Occupied -- if all free then change to `Occupied`
       else Free                                -- otherwise do not change
updateTile Occupied neighbors =
  if count (==Occupied) neighbors >= 4 -- If we have more than 4 `Occupied`
     then Free                         -- we switch to `Free`
     else Occupied                     -- Else we do not change
```

Now that we have the rule to update our cellular automaton we need to apply it
to each element of our grid. For this we ard going to create a type alias `Board` that
is a list of list of tiles (one list contains the rows, and the other contains the elements
in each column), and operate on this board. Updating the `Board` is simply a function
from `Board` to `Board` that applies our `updateTile` function for each tile.

```
Board : Type
Board = List (List Tile)

playGame : Board -> Board
playGame board =
  map (\(i, row) => map (\(j, tile) => updateTile tile (getAdjacent i j board))
                        (zipWithIndex row))
      (zipWithIndex board)
```

This function is a bit more complicated than expected but we can decypher it line by line:

- The outer map iterates through each `row` and associates the index of the row with the
variable `i`
- The inner map iterates through each element of the current row and associates the index of
each element with the tile it is looking at.
- This nested map gives us access to both the tile and its position in the board as a 2d coordinate
which we use with `updateTile` and `getAdjacent`.

The coordinate here is important in order to get the list of adjacent tiles. The function to get
the adjacent tiles generates the 8 possibles directions to moves from
`[(1,1), (1, 0), (1, -1), (0, -1), (-1, -1), (-1, 0), (-1, 1), (0, 1)]` and then add each of
those direction to the current coordinate to get a list of adjacent coordinates, and then
attempts to retrieve the tiles at those coordinates:

```
mIndex : Nat -> List a -> Maybe a
mIndex 0 [] = Nothing
mIndex 0 (x :: xs) = Just x
mIndex (S k) [] = Nothing
mIndex (S k) (x :: xs) = mIndex k xs

toNat : Int -> Maybe Nat
toNat x = case compare x 0 of
               EQ => Just Z
               GT => S <$> toNat (x - 1)
               LT => Nothing

getCoord : (row, col : Int) -> Board -> Maybe Tile
getCoord row col xs = do getRow <- mIndex !(toNat row) xs
                         mIndex !(toNat col) getRow

plusC : (Int, Int) -> (Int, Int) -> (Int, Int)
plusC (a, b) (c, d) = (a + c, b + d)

getAdjacent : (row, col : Int) -> Board -> List Tile
getAdjacent row col board =
  let directions : List (Int, Int)
      directions =  [(x, y) | x <- [-1, 0, 1], y <- [-1, 0, 1], x /= 0 || y /= 0]
   in catMaybes $ map (\(x, y) => getCoord x y board) (map (plusC (row, col)) directions)
```

Arguably that's a lot of code but most of it should really be in the standard library for dealing
with list indices and manipulating Nats.

We've gone all the way to here but we haven't stated the problem clearly yet: Given this
cellular automata, run it until it reaches a fix point, when that happens, what is the number of
`Occupied` cells?

For this we are going to define a "fixpoint" `fix` that will repeatedly run a function on
an input until it does not change anymore:

```
partial
fix : Eq a => (a -> a) -> a -> a
fix f oldValue = let newValue = f oldValue in -- run once
                     if oldValue == newValue  -- check if we reached the fixpoint
                        then newValue         -- return the fixpoint if we have
                        else fix f newValue   -- keep going if we haven't
```

Here we have to assume `partial` since there is no guarantee that we reach a fixpoint to begin
with, so the function might run forever. Additionally, we might reach a cycle of repeating states
that won't be detected as a fixed point but is guaranteed to never make any progress.

In order to solve our problem we run our `fix` function with our `playGame` function and using
our input as initial state. If this functoin return, it will have found the state of the game
where playing additional rounds of the game doesnt change its state. From this state we can
count the number of `Occupied` cells.

```
partial
solve : Board -> Nat
solve board = let foundFixed = fix playGame board in
                  count (==Occupied) (concat foundFixed)
```

# Part 2

The problem for part 2 is almost identical except that instead of looking at the immediately
adjacent cells, we look at the first node that is visible from the 8 possible directions around
our cell. If a cell is completely surrounded, it doesn't change from part1 but if a cell is
next to an empty cell, we need to skip it and look at the next cell further in the same direction.

in order to implement that we are going to change our `getAdjacent` function such that it
searched for the next visible cell in each direction:


```
getClosestSeat : Board -> (pos, dir : (Int, Int)) -> Maybe Tile
getClosestSeat board position direction  =
  let newPos = position + direction in
  uncurry getCoord newPos board >>=
    \case Floor => getClosestSeat board newPos direction
          x => Just x

getAdjacent : (row, col : Int) -> Board -> List Tile
getAdjacent row col board =
  let directions : List (Int, Int)
      directions =  [(x, y) | x <- [-1, 0, 1], y <- [-1, 0, 1], x /= 0 || y /= 0]
   in catMaybes $ map (getClosestSeat board (row, col)) directions
```

`getClosestSeat` takes a board, a position and a direction and returns the next visible tile,
retuning `Nothing` if there is none. And that is the only change we need to make!


#Acknowledgements

Credits to [yugiohxlight](https://twitter.com/yugiohxlight) for the trick about getting the
surrounding cells. My original solution was way more complicated than this one!
