# Day 6 Part 1

Today's exercise was surprisigly easy, and like yesterday, it is mostly an exercise
in reading rather than in programming.

Given a list of lists the goal is to count how many non-repeating elements there
are after combining all the lists. Additionally, we are given a number of those
"lists of lists" and we need to sum up their totals.

In order to combine and count the number of non-repeating elements for a list of lists
we are going to write a function all `unionAll` which will combine every list into
a single big one and then use the function `nub : List a -> List a` to remove duplicates:

```
unionAll : Eq a => (List (List a)) -> List a
unionAll = nub . join
```

`join` is one of the monadic operations and allows you to _flatten_ a list of lists into
a single-layered list: `join : List (List a)) -> List a`, or more generally
`join : m (m a) -> m a`.

Our input is, as usual, a giant string that we need to parse. Thankfully the format is quite
easy:

- Each line is a `List Char`
- The file has groups of `List Char` that we need to parse into `List (List Char)`
- Each group in the file is separated by an empty line

Our parsing function needs then to perform those tasks in order to extract a `List (List (List Char)`
from a `String`, therefore its type signature is `getGroups : String -> List (List (List Char))`, and
its implementation is pretty straight forward and can be done entirely pointfree:

```
                      List of groups
                       |
                       |          group
                       v   /---------------\
getGroups : String -> List (List (List Char))
getGroups = map (map unpack) . forget . split (== "") . lines
                     ^             ^        ^            ^
                     |             |        |            |
                     |             |        |        1. get the lines
                     |             |        |
                     |             |     2. get the groups
                     |             |
                     |         3. Since `split` returns `List1` we need to
                     |            convert back to `List` with `forget`
                     |
                  4. Convert the strings from the groups into `List Char`

```

Finally, putting everything together amounts to mapping each group to the number of element
it has after using `unionAll` and getting the sum out of the list:

```
countEverything : String -> Nat
countEverything = sum . map (length . unionAll) . getGroups
```


# Part 2

For part two we are asked _exactly the same thing_ except we need to use `interesectAll`
instead of `unionAll`, thankfully this time the function is already available from
`Data.List`.

For the interest of the exercise we can abstract over which operation to perform
and give it as a parameter to `countEverything`:

```
countEverything : (merge : List (List Char) -> List Char) -> String -> Nat
countEverything merge = sum . map (length . merge) . getGroups . lines
```

and call it this way:

```
putStrLn ("total tally intersect : " ++ show (countEverything intersectAll fileContent))
```

Note that now calling the previous result can be achieved with

```
putStrLn ("total tally union : " ++ show (countEverything unionAll fileContent))
```

And we are done!
