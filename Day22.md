# Day 22 Part 1

With Day 22 we are closing in to the end of Advent of code. This challenge is another
simulation and the rules are pretty simple. We are simulating a game of cards where
two players reveal the top card of their deck and compare their values (each card is
just a number without a suit). The player with the higher number keeps both card
and puts them under their deck with the higher card above the lower card. This process
repeats until one of the decks is empty and the player with the full deck of cards
wins.

Since the deck behaves like a [queue](https://en.wikipedia.org/wiki/Queue_(abstract_data_type))
we are going to add functions `pop` and `push` to work with lists. `pop` returns both the
head and the tail of the list while `push` appends an element at the end.

```
push : a -> List a -> List a
push x = (++ [x])

pop : List a -> Maybe (a, List a)
pop [] = Nothing
pop (x :: xs) = Just (x, xs)
```

Now onto the game. In order to perform a round we need to look at the two decks and
`pop` their top cards. If one of the decks is empty, then the `pop` function will return
nothing, indicating that the other player won. If both player are able to draw a card,
we compare their values and check which one is bigger. The player with the higher
card will have its deck be `push`ed the two card at the back of their deck and the
game continues with a recursive call.

To make the implementation slightly cleaner we are going to use a [view](https://idris2.readthedocs.io/en/latest/tutorial/views.html?highlight=with).
Since we are not performing any dependent pattern matching this is just a way to make the
code slightly cleaner:

```
playGame : Decks -> Maybe (List Nat)
playGame (p1, p2) with (pop p1, pop p2) -- pop the top card from both decks
  playGame (p1, p2) | (Nothing, Nothing) = Nothing -- both decks are empty, error
  playGame (p1, p2) | (Nothing, (Just z)) = Just p2 -- player 2 wins
  playGame (p1, p2) | ((Just z), Nothing) = Just p1 -- player 1 wins
  playGame (p1, p2) | ((Just (e1, q1)), (Just (e2,q2))) =
      -- compare and perform a recursive call with the decks correctly updated to
      -- reflect who won this round
      if e1 > e2 then playGame (push e2 (push e1 q1), q2)
                 else playGame (q1, push e1 (push e2 q2))
```

`push e2 (push e1 q1)` means "push e1 on q1 first and then push e2" so that the winner card
is on top of the second card. in care player 1 wins.

The finaly result is obtained by zipping the winning deck with its index starting with `1`
and then taking the sum of all the products:

```
solve : List Nat -> Nat
solve xs = sum $ zipWith (*) (reverse xs) [1 .. S (length xs)]
```

And that was part 1, in part 2 we are going to make slight changes to the rules.

# Part 2

The additional rules of part 2 are as follow:

- When both players reveal a card, if the value of the card is smaller or equal to the
number of card remaining in the deck, start a nested game with only the first `n` and `m` cards
in each deck where `n` and `m` are the number on each revealed card. The winner of the
nested game is the winner of the round and adds both cards at the end of their deck, with
their own card first.
- When both games enter a state that has already been visited, player 1 wins. This is
to avoid infinite games.
- When none of those conditions apply, the rules are the same as before.

We now need two additional pieces of information: What are the previous moves already played
and who was the winner of the nested game just played? In order to track those two changes
we need to change our type signature:

```
data Player = P1 | P2

playGame : Decks -> SortedSet Decks -> Maybe (Player, List Nat)
```

We now take a `SortedSet` of previous deck configurations so that we can quickly lookup if
the current configuration has appeared in the past. We also return which player won the
game so that we can inspect the result of recursive games.

The logic very similar to "Part 1" except for two things. Before drawing a card we check
if our current configuration is in our history. If it is, `P1` wins, otherwise we proceed like
before by drawing a card and comparing their result.

The second change in the logic is that drawing cards does not proceed like before. Indeed
when the cards are drawn we need to check if their values are equal or smaller than the
size of their respective decks. If they are, then we start a new recursive game using
the top `n` and `m` cards of each deck (with an empty history). The winner of that
sub-game will then take both cards and insert them at the end of the deck, with their card
going in first.

If one of the cards has a number bigger than its deck then the game proceeds like before. In
order to impement this change we are going to implement two helper functions `normalGame`
and `pickWinner`. `normalGame` simply uses the same rules as before, the top card wins.
`pickWinner` inspects the result of a recursive game and picks the winner accordingly. With
those two functions we can check if we should enter a recursive game or if we should play
normally:

```
  -- case where we have not found the current state in history and both cards have been drawn.
  playGame now@(x, y) history | (False, Just (e1, q1), Just (e2, q2)) =
      if e1 <= length q1 && e2 <= length q2
         then playGame (take e1 q1, take e2 q2) empty -- play the recursive game
            >>= pickWinner e1 e2 (q1, q2) (insert now history) -- pass the result to our helper function
         else -- `insert now history` add the current state to the history of states
              normalGame e1 e2 (q1, q2) (insert now history)
  where
    normalGame : (e1, e2 : Nat) -> Decks -> SortedSet Decks -> Maybe (Player, List Nat)
    normalGame e1 e2 (q1, q2) history =
      if e1 > e2
         then playGame (push e2 (push e1 q1), q2) history
         else playGame (q1, push e1 (push e2 q2)) history

    pickWinner : (e1, e2 : Nat) -> Decks -> SortedSet Decks
                 -> (Player, List Nat) -> Maybe (Player, List Nat)
    pickWinner e1 e2 (q1, q2) history (P1, _) = -- Player 1 wins
      playGame (push e2 (push e1 q1), q2) history
    pickWinner e1 e2 (q1, q2) history (P2, _) = -- Player 2 wins
      playGame (q1, push e1 (push e2 q2)) history
```

You will notice the `with` has a third argument, unlike in Part 1. That we because we first
check to see if the current configuration has been stored in the history, and if it has we
make "Player 1" the winner.

```
playGame : Decks -> SortedSet Decks -> Maybe (Player, List Nat)
playGame now@(x, y) history with (contains now history, pop x, pop y)
  playGame now@(x, y) history | (True, _, _)  = Just (P1, x) -- `now` is in `history`
  playGame now@(x, y) history | (False, Nothing, Nothing) = Nothing
  playGame now@(x, y) history | (False, Just z, Nothing) = Just (P1, x)
  playGame now@(x, y) history | (False, Nothing, Just z) = Just (P2, y)
  ...
```

And that's it! For this I've implemented auxillary functions mostly because otherwise the code
was a bit hard to read with lots of `if then else` and `case` blocks. The logic of the
game isn't particularly hard but the control flow is a bit confusing. Thankfully the
`with` syntax helps with that too.
