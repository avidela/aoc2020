module Main

import System
import System.File
import Data.List
import Data.List1
import Data.Strings
import Data.Nat
import Data.Maybe
import Debug.Trace

findMult : Int -> Int -> Int
findMult target n = (target `div` n) + 1

findSmallest : Ord a => List (a, b) -> Maybe (a, b)
findSmallest [] = Nothing
findSmallest [x] = Just x
findSmallest ((x, y) :: z :: xs) = map (\(a, b) => if a < x then (a, b)
                                                            else (x, y))
                                       (findSmallest (z :: xs))


solve : (Int, List Int) -> Maybe Int
solve (target, numbers) =
  let computed = map (\x => (x * findMult target x) - target) numbers in
      uncurry (*) <$> findSmallest (zip computed numbers)

parseFile : String -> Maybe (Int, (List Int))
parseFile input = let [n , nums] = lines input
                        | _ => Nothing in
                      do n' <- parsePositive n
                         let nums' = catMaybes $ map parsePositive $ forget $ (split (==',') nums)
                         trace (show nums') (pure ())
                         pure (n', nums')


partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just parsed = parseFile fileContent
  putStrLn ("Solution : " ++ show (solve parsed))
