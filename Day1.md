# Advent of code 2020

Last year I tried doing the [advent of code](https://adventofcode.com) challenges but got bored midway through, hopefull this won't happen this year and in order to make it more interesting I will write a bit about every solution that I come up with, using Idris. This is text is aimed to be a reference for people wanting to get started with idris or functional programming and are familiar with haskell-like syntax. Even if you are not I'm sure reading through the code will teach you something about how I think about those problems.

The code is availabe in the `src` folder. Part 1 is [here](src/day1.idr)

# Day 1 Part 1

This first challenge is a nice warm up. It amounts to finding two numbers inside a list that add up to 2020.

In terms of functions we're looking at one with a signature that looks like this:

```idris
findPair : List Int -> Maybe (Int, Int)
```

where the list in argument is the input list and the `Maybe (Nat, Nat)` represents the two values found.

If this function is correctly implemented the following property should hold:

```idris
findPair input = Just (a, b) -> a + b = 2020
```

That is, if `findPair` finds two numbers `a` and `b`, then those two number sum up to 2020.

This program can be split into two goals:

1. Given a number find another number in the list that sums to 2020. Its type signature would be `findPair : Int -> List Int -> Maybe Int`
2. Iterate through the list and try every element with `findPair`. Effectively using every element as a potential candidate.

This is a good opportunity to tell everyone that I will not concern myself with _speed_ but will focus on _correctness_ instead. I know there are more efficient ways to go about those exercises, but let's be real. If I wanted to be fast, I'd be using C or assembly, not Idris.

## Finding our matching pair

Let's start with the first function `findPair : Int -> List Int -> Maybe Int`, given a number, and a list of candidates, we need to return the candidate that, once you add it to the firstValue, returns 2020.

For this we are simply use the function `find` which, given a predicate, returns the first element that matches it within a list:

```idris
find : (a -> Bool) -> List a -> Maybe a
```

This allows us to implement the first function as a call to `find`

```idris
findSum : (firstValue : Int) -> List Int -> Maybe Int
findSum firstValue = find (\secondValue => firstValue + secondValue == 2020)
```

## Iterating through the list

Now we need to iterate through our list and find the next element that sums to 2020. For this we are simply going to recursively explore the list, until we've exhausted all possibilities:

```idris
findPair : List Int -> Maybe (Int, Int)
-- if we ran through the list and found nothing, then return nothing
findPair [] = Nothing 
-- If we still have numbers to go through, try the first number
-- available, if it doesn't find anything, keep going.
findPair (x :: xs) = case findSum x (x :: xs) of
                          Nothing => findPair xs
                          (Just y) => Just (x , y)
```

Programers experienced with Idris and the like might want to refactor this code to use `maybe` rather than pattern matching. This version is here:

```idris
findPair' : List Int -> Maybe (Int, Int)
findPair' [] = Nothing
findPair' (x :: xs) = maybe (findPair' xs) (Just . (x,)) (findSum x (x :: xs))
 ```

it does the same thing, I personally find pattern matching easier to understand, but harder to read. The second one is easier to read, but harder to understand if you're not familiar with `maybe`. Pick the one you like best.

## Putting it together

That's it, now we just need to feed our function and multiply the two numbers found together. We can multiply both numbers with 

```idris
findNumber : List Int -> Maybe Int
findNumber ls = map (uncurry (*)) (findPair ls)
 ```
 
Now you can either copy paste your input inside your code and put it inside a variable. And the call `findNumber` using this variable as argument. Or you can write a small `main` function that will read a file on disk and parse it into the desired input. Here is my main function:

```idris
parseFile : String -> Maybe (List Int)
parseFile input = let lines = lines input in
                      traverse parsePositive lines

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let (Just parsed) = parseFile fileContent
  case fundNumber parsed of
       Nothing => putStrLn "couldn't find a pair of number that sum to 2020"
       (Just prod) => putStrLn $ "found product" ++ show prod
```

# Day 1 Part 2

The code for part 2 is [here](src/day1p2.idr).

The second part is a very simple extention, we need to find a _triple_ of number that add up to 2020 and then return the sum. Which means we need a new function 

```idris
findTriple : List Int -> Maybe (Int, Int, Int)
```
                            
This function will work in very similar ways as `findPair`, that is:

- it will return `Nothing` if we haven't found anything
- it will keep searching, using the next number as candidate, if we haven't found anything yet but still have numbers to go through
- it will return the triple we found if `findPair` found something

```idris
findTriple [] = Nothing
findTriple (x :: xs) = case findPair x (x :: xs) of
                            Nothing => findTriple xs
                            (Just (y, z)) => Just (x, y, z)
```

You will notice that we had to change `findPair` to take an extra argument, which is the partial sum that we are currently testing. With that the new signature of `findPair` is `findPair : (value : Int) -> List Int -> Maybe (Int, Int)`. And the change is that instead of simply adding the number we are looking at, and the number we are looking for, we also add the `value` passed in argument that represents the first candidate of the triple:

```idris
findSum :  (zeroValue : Int) -> (firstValue : Int) -> List Int -> Maybe Int
findSum zeroValue firstValue = find (\secondValue => zeroValue + firstValue + secondValue == 2020)

findPair : (value : Int) -> List Int -> Maybe (Int, Int)
findPair value [] = Nothing
findPair value (x :: xs) = case findSum value x (x :: xs) of
                          Nothing => findPair value xs
                          (Just y) => Just (x , y)
```

Finally we need to update our `findNumber` function in order to call `findTriple` rather than `findPair` and multiply the result.

```idris
findNumber : List Int -> Maybe Int
findNumber ls = map (\(a, b, c) => a * b * c) (findTriple ls)
```

Once this is done we can run our program on the command line with our input and we're golden!
                                                      
