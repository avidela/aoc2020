module Main

import System
import System.File
import Data.Strings
import Data.Vect
import Data.List
import Debug.Trace

%default total

data Instruction = Acc Int | Nop Int | Jmp Int

Show Instruction where
  show (Acc n)= "acc"
  show (Nop n)= "no-op"
  show (Jmp n)= "jump"

Program : Nat -> Type
Program n = Vect n Instruction

-- the state of the execution is a triple of
-- - the current function pointer
-- - the current accumulator value
-- - the history of instructions executed
ProgramState : Nat -> Type
ProgramState n = (Fin n, Int, List (Fin n))

-- safely add an `Int` to a `Fin n`
safeAdd : {n : Nat} -> Fin n -> Int -> Maybe (Fin n)
safeAdd x y = integerToFin (cast x + cast y) n

-- given a current instruction pointer, and an instruction get the new
-- instruction pointer
nextPointer : {n : Nat} -> (ip : Fin n) -> Instruction ->  Maybe (Fin n)
nextPointer ip (Acc x) = safeAdd ip 1
nextPointer ip (Nop x) = safeAdd ip 1
nextPointer ip (Jmp x) = safeAdd ip x

-- Given an instruction and a state, get the new state
execute : Instruction -> Int -> Int
execute (Acc x) acc = acc + x
execute (Nop x) acc = acc
execute (Jmp x) acc = acc

-- ip : instruction pointer
-- state : current state of the accumulator
partial
doesItTerminate : {n : Nat} -> Program n -> ProgramState n -> Maybe Int
doesItTerminate prog (ip, state, hist) =
  let instruction = index ip prog
      newState = execute instruction state
      Just next = nextPointer ip instruction
      | Nothing => pure newState in
  if next `elem` hist
     then Nothing
     else doesItTerminate prog (next, newState, ip :: hist)

fixup : Instruction -> Instruction
fixup (Acc x) = Acc x
fixup (Nop x) = Jmp x
fixup (Jmp x) = Nop x

generateModifiedPrograms : {n : Nat} -> Program n -> List (Program n)
generateModifiedPrograms prog =
  let indices : List (Fin n) = filter (\idx => case index idx prog of
                                                    (Acc _) => False
                                                    _ => True)
                                      (toList $ range {len=n}) in
      map (\i => updateAt i fixup prog) indices

partial
findTerminating : {n : Nat} -> Program (S n) -> Maybe Int
findTerminating prog = foldr (\prog, r  => maybe (doesItTerminate prog (0,0,[])) Just r)
                             Nothing
                             (generateModifiedPrograms prog)

parseFile : String -> Maybe (n ** Program n)
parseFile str = do ls <- traverse parseInstruction (lines str)
                   pure (_ ** fromList ls)
  where
    parseInstruction : String -> Maybe Instruction
    parseInstruction str = case words str of
                                ["nop", a] => Nop <$> parseInteger a
                                ["acc", a] => Acc <$> parseInteger a
                                ["jmp", a] => Jmp <$> parseInteger a
                                _ => Nothing

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Just (_ ** parsed@(_::_)) = parseFile fileContent
  putStrLn ("acc after first repetition: " ++ show (findTerminating parsed))


