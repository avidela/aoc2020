# Day 10 Part 1

Part 1 is actually extremely easy, the problem is also a reachability problem
except the data is very easy to parse and we are given a lot more information
about the problem, namely: Each node in the graph has to be used.

Let's recap the problem, we are given a set of number, and each of those numbers
have to have a "friend" that is either 1, 2 or 3 higher than itself (what we will call
later a "jump"). Those friendly
numbers define a sequence and the sequence is valid if we can go from 0 to the maximum.

One way to go about this would be to see this problem as a "reachability" problem
were a graph would have nodes for every number, and each node would have directed
edges toward compatibles numbers (number that are at most 3 higher).

Fortunately for us we don't have to go through this using reachability of graphs
because the question is "using all the numbers in our set, how many `1` jumps
and `3` jumps are there and return their product". The fact that we are forced
to use every number makes the reachability question useless: if the end point
is unreachable, then the question is meaningless, which means the data must be
well formed to make the graph reachable.

Knowing that, how do we get the number of `1` and `3` edges in this graph?
Since we have to use every single edge we can simply sort our array and
count the difference between every element and the next. Once we have our
array of difference we can count the number of `1` and `3` and multiply their result.

The code is pretty straight forward:

```
mapDifferences : List Nat -> List Nat
mapDifferences xs = zipWith minus xs (0 :: xs) -- we need to start at 0

countDifferences : List Nat -> Nat
countDifferences xs =
      let diffs = mapDifferences xs
          ones = length $ filter (==1) diffs
          thrs = length $ filter (==3) diffs
       in ones * (S thrs) -- the problem definition asks us to add a final 3
```

The main star of the show is `zipWith minus xs (0 :: xs)` which creates a list
of consecutive pairs, the order is flipped, such that the biggest element is the
first argument of the `minus` in order to get positive numbers after performing
the difference.

# Part 2

I solved part 2 wrong but the code gives the correct result. The goal of part 2
is to count the number of possible arrangement of the sequence of numbers such
that is stays valid. This problem is combinatorialy exponential so a brute-force
solution won't do. My first instinct was to count the number of possible "merges"
you can do in every subsequence of differences of `1`. Imagine we 2 differences of
`1` in sequence:

```
sequence: [0, 3, 4, 5, 8]
differences : [3, 1, 1, 3]
valid arrangements:
[0, 3, 4, 5, 8] [3, 1, 1, 3]
[0, 3, 5, 8]    [3, 2, 3]
```

There are only 2 possible arrangements, one is the original list and the other one
drops the `4`. Dropping the `5` is impossible as it would make the different with `8`
too great. If we try this same thing with an additional element we get:

```
sequence: [0,3,4,5,6, 9]
differences : [3,1,1,1, 3]
valid arrangements:
[0, 3, 4, 5, 6, 9] [3, 1, 1, 1, 3]
[0, 3, 5, 6, 9]    [3, 2, 1, 3]
[0, 3, 4, 6, 9]    [3, 1, 2, 3]
[0, 3, 6, 9]       [3, 3, 3]
```

So for 3 differences we get 4 possibles arrangements, with 4 differences of `1` we get:

```
sequence: [0,3,4,5,6,7, 10]
differences : [3,1,1,1,1,  3]
valid arrangements:
[0, 3, 4, 5, 6, 7, 10] [3, 1, 1, 1, 1, 3]
[0, 3, 5, 6, 7, 10]    [3, 2, 1, 1, 3]
[0, 3, 4, 6, 7, 10]    [3, 1, 2, 1, 3]
[0, 3, 4, 5, 7, 10]    [3, 1, 1, 2, 3]
[0, 3, 5, 7, 10]       [3, 2, 2, 3]
[0, 3, 4, 7, 10]       [3, 1, 3, 3]
[0, 3, 6, 7, 10]       [3, 3, 1, 3]
```

Which is 7 ways, to recap:

- for 2 numbers we have 2 possible arrangements. `(2 choose 2) + 1`
- for 3 numbers we have 4 possible arrangements. `(3 choose 2) + 1`
- for 4 numbers we have 7 possible arrangements. `(4 choose 2) + 1`

From which we can assume the general formula is `n choose 2 + 1` gives us the number
of arrangements we can get for each sub sequences of `1` differences. Multiplitying
each number with the next would give if the total number of arrangements.

This reasoning turns out to be incorrect, and does not account for longer subsequences
of `1`s. trying out this algorithm on a sequence for `[1..100]` (which is valid since the only
differences are `1`, this is a long streak of `1`) gives us `100 choose 2 + 1 = 4951`
but the correct number is `180396380815100901214157639`. The reason this `x choose 2 + 1`
works is pure coincidence, it works for short sequences but not longer ones.

The correct solution would do some sort of recursive "reachability"  test
saying "the number of arrangment to go from x to y is the sum of the number of ways to reach
the previous 3 numbers", creating a
[tribonacci sequence](https://mathworld.wolfram.com/TribonacciNumber.html).

