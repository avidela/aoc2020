module Main

import Data.SortedMap
import Data.SortedSet
import Data.Maybe
import Data.Strings
import Data.List
import Data.List1
import System
import System.File

Dag : Type
Dag = SortedMap String (SortedSet String)

-- map utilities
insertIfMissing : Eq a => (a , b) -> SortedMap a b ->  SortedMap a b
insertIfMissing (key, value) map =
  case lookup key map of
       Nothing => insert key value map
       Just v => map

update : key -> (updateFn : value -> value) -> SortedMap key value -> SortedMap key value
update key updateFn smap =
  let value = map updateFn $ lookup key smap in
      fromMaybe smap $ map (\v => insert key v smap) value

addParent : (parent : String) -> (node : String) -> Dag -> Dag
addParent parent node = update node (insert parent)

-- counting
setOfParents : Dag -> (element : String) -> SortedSet String -> SortedSet String
setOfParents dag elem acc =
  case lookup elem dag of
       Nothing => acc
       Just xs => if null xs
                     then acc
                     else let acc' = (acc `union` xs) in
                              concatMap (\child => setOfParents dag child acc') (xs `difference` acc)

numberOfParents : Dag -> (element : String) -> Nat
numberOfParents dag elem = length $ SortedSet.toList $ setOfParents dag elem empty

-- data mangement
construct : (parent : String) -> (children : List String) -> Dag -> Dag
construct parent children dag =
  let updatedMap = foldr (\key => insertIfMissing (key, empty)) dag (parent :: children)
      withParents = foldr (\child => addParent parent child ) updatedMap children in
      withParents

constructAll : List (String, List String) -> Dag
constructAll lines = foldr (uncurry construct) empty lines

-- parsing

parseChild : List String -> Maybe String
parseChild [n, a, b, c] = Just (a ++ b)
parseChild [a, b, c] = Just (a ++ b)
parseChild _ = Nothing

partial
getPair : (line : String) -> (String, List String)
getPair line =
  let (a :: b :: _ :: "contain" :: cs) = words line
      parent = concat [a,b]
      Just children = traverse parseChild
                        $ forget $ map Strings.words
                        $ Strings.split (==',') $ unwords cs
   in (parent, children)

partial
parseLines : String -> List (String, List String)
parseLines = map getPair . lines

-- main

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let parsed = constructAll $ parseLines fileContent
  putStrLn ("numberOf shiny gold: " ++ show (numberOfParents parsed "shinygold" ))
