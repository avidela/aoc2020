# Day 7 Part 1

For day 7 I'm only going to talk about part 1 for a number of reasons:

- This post is a bit late because I didn't have the time to write it on the day. Which means
the problem isn't as fresh in my memory.
- I remember throwing away most of part1 for part 2 so there isn't a cohesive continuity
beween part 1 and 2.
- I'm running out of time so I've got to make cuts somewhere.

Day 7, is essentially a graph problem, but it took me very long because parsing the
data was more difficult that previous problems. And beacuse I could not reuse my part 1 for
part 2 because of the data structure to represent graph was _upside down_. What does that mean?

In this problem we are asked to give us how many parents does one node in a graph has.
Nodes have parents and children and this "parenthood" relation represents which way the arrow
goes in our directed graph.

In order to simplify the question (I am not sure that worked) I decided to revert the arrows
and say "each node is _related_ to a set of other nodes" in our case the relation says who
are the parents. This way, counting the number of parents amounts to counting how many parents
a node has and how many parents each of its parents has recursively.
If we represent our directed graph with a type `Dag`, the type signature we are looking at is as
such:

```
numberOfParents : Dag -> (element : String) -> Nat
```

When computing the number of parents we need to ensure we are not counting them twice.
On way to avoid that is to use a `Set` to store the parents we "collected" and then count its
length. `numberOfParents` then requires to collect all the parents of the element given in
argument, and return its length:

```
numberOfParents : Dag -> (element : String) -> Nat
numberOfParents dag elem = length $ SortedSet.toList $ setOfParents dag elem empty
```

This makes a call to `setOfParents` which does the heavy lifting and collects all parents in
a `Set` (`SortedSet` in Idris), which we conver to a list and count using `length`.

What would be the signature of `setOfParents` then? Given the way we use it we can see
that we pass it the graph in first argument, the element in second argument and an `empty`
set in thrid argument, its type signature is then
`setOfParents : Dag -> (element : String) -> SortedSet String -> SortedSet String`

There set passed in argument is our accumulator of parents and `element` is the current
element from the set from which we need to collect the parents. In order to understand how
to implement this functions we need to understand what the `Dag` type is. Since Idris
does not have a graph library I've decided to represent our directed graph with a
dictionary (`SortedMap` in Idris) of `String` as keys and `SortedSet String` as values.
Which means each key is an element and each value is the set of parents of this element.

```
Dag : Type
Dag = SortedMap String (SortedSet String)
```

Now we can describe an algorithm for collecting all parents:

- First get all the immediate parents of the element by doing a `lookup` in our `SortedMap`
- If the element cannot be found, return the accumulator
- If the element has no parents, return the accumulator
- If the element exists and has parents, we obtain the updated list of parents by doing the
`union` of the accumulator and the new set of parents. Then, for each parents not in the
accumulator, we obtain their parents with a recursive call and add them to our accumulator.

I assume the way the problem was intended to be solved involved
iterating through each node of the graph and doing a reachability test
for the `element` we are looking for. Though my solution seemed more intuitive to come up with
it ended up harder to write:

```
setOfParents : Dag -> (element : String) -> SortedSet String -> SortedSet String
setOfParents dag elem acc =
  case lookup elem dag of
       Nothing => acc
       Just xs => if null xs
                     then acc
                     else let acc' = (acc `union` xs) in
                              concatMap (\child => setOfParents dag child acc') (xs `difference` acc)
```

The multiple branching turned out to be important, so was the `union` and `difference` operations to
ensure we are not doing duplicate work.

`numberOfParents` is now complete and calling it results in the answer to our problem.

# A word about parsing

This problem was a pain to parse, first of all, representing a graph in terms of text is not easy,
secondly, describing them in prose-like languge doesn't help, specially when the text contains
punctionation. And lastly, my data structure relied on post-processing the data in addition to
parsing it. Which wouldn't have happened if I had used a simpler graph representation from a
typical graph library and kept the parent-child relationship.

Parsing the list means transforming each line into a pair `(String, List String)` with the node
as first element and its children (not parents!) as second element. Such a parsing function
would have this type signature `String -> Maybe (List (String, List String))` which isn't oo
offensive by itself but the amount of work related to parsing it is disproportionate compared
to the amount of code require to solve the problem:


```
parseChild : List String -> Maybe String
parseChild [n, a, b, c] = Just (a ++ b)
parseChild [a, b, c] = Just (a ++ b)
parseChild _ = Nothing

partial
getPair : (line : String) -> (String, List String)
getPair line =
  let (a :: b :: _ :: "contain" :: cs) = words line
      parent = concat [a,b]
      Just children = traverse parseChild
                        $ forget $ map Strings.words
                        $ Strings.split (==',') $ unwords cs
   in (parent, children)

partial
parseLines : String -> List (String, List String)
parseLines = map getPair . lines
```

Since my data representation attempts to flip the arrows I also had to reinterpret the graph
with some additional data processing:

```
insertIfMissing : Eq a => (a , b) -> SortedMap a b ->  SortedMap a b
insertIfMissing (key, value) map =
  case lookup key map of
       Nothing => insert key value map
       Just v => map

update : key -> (updateFn : value -> value) -> SortedMap key value -> SortedMap key value
update key updateFn smap =
  let value = map updateFn $ lookup key smap in
      fromMaybe smap $ map (\v => insert key v smap) value

addParent : (parent : String) -> (node : String) -> Dag -> Dag
addParent parent node = update node (insert parent)

construct : (parent : String) -> (children : List String) -> Dag -> Dag
construct parent children dag =
  let updatedMap = foldr (\key => insertIfMissing (key, empty)) dag (parent :: children)
      withParents = foldr (\child => addParent parent child ) updatedMap children in
      withParents

constructAll : List (String, List String) -> Dag
constructAll lines = foldr (uncurry construct) empty lines
```

This all to say: pick your abstractions wisely both for your real job and for those exercises.

Most of the time, only hindsight can give you the information you need to make the right choice
and this is why those exercises are important. This is also why I am extremely adamant abour
refactoring code in production. Code exists in a context, not in a vacuum, and this context might
have changed with time but the code hasn't. In order to understand how to best work with a codebase
it is vital to have it match with today's assumptions and requirements.
