# AOC post-mortem

It is March 14th, it’s been 2 weeks since the last update on the AOC repository. This will be the last one. Let’s take this time to see what has been achieved, what hasn’t and why.

## What’s been done

During the month of December and a bit of January I wrote 20 blog posts for 20 solutions of 20 problems. Each is abound 1200 words in average, each blog post accompanies two files which solve each part of each challenge for each day. The full programs can be found in the `src` directory and usually can be compiled with `idris2 -p contrib` in order to import the `contrib` library. If you are a reader from the future, some of those might not work either because the language has changed or something in the library has changed. If that’s the case, I encourage you to either open an issue about them so that someone can fix the example programs or even better: open a Merge Request with the fix.

Overall I’ve had a lot of fun working on those problems every day, most of them also have a video component of myself going through the exercises during a livestream. I hope that having a written text, a code reference and a video component will help anyone who’s interested in getting started with Idris.

My goal was to provide solutions that were both intuitive and idiomatic in Idris. The reason for that was to allow beginners to see what regular Idris code looks like and how a complete program runs. As well as showcase how dependent types can help with programming. It turned out that this goal was much harder to achieve than I imagined! With that said, let’s dive into what’s not been done and why.

## Day 19 - Monster Messages

The problem to solve on day 19 was a simple parsing problem with a non-recursive grammar. There is a partial solution on the branch `day19` however the full solution for part 2 is missing.

This is because my goal for those exercises was to provide a full and idiomatic answer to each problem. A solution that comes naturally and that makes use of the best features of _idris_. However in this case it turned out that providing such a solution was much more difficult than anticipated because the default parsing library of idris does not allow to backtrack arbitrarily far. Let’s see why this is important.

The problem is to define a recursive grammar on the alphabet `a | b`, the grammar is defined in a file like this:

```haskell
0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"
```

This is read as “rule 0 is the grammar using rule 4 1 and 5 sequentially”. Rules with a pipe symbol `|` define alternatives. This is all pretty normal and familiar for someone who’s worked with parser generators and grammars before.

One way to implement this is to construct a gigantic tree out of this grammar and write a function that walks this tree to generate a parser for it. The parser only has 3 primitives: sequence, alternative and character checks.

This works for part 1 but not for part 2 since in part 2, the grammar is modified to become recursive. This means there is no finite data structure that describes the computation, we cannot generate a finite tree for it.

In order to deal with the look would be to make both the data structure and the construction of the parser function lazy. Since the function would only be called on finite input, constructing a data structure for the grammar would always terminate, and so would constructing the parser for it.

However this fails the requirement for those blog posts: intuitive and idiomatic. Laziness in Idris is not an idiomatic pattern and few people would come up with this intuitively (only haskell programmers basically). So I had to come up with something else.

The other solution is to parse the grammar into a parser combinator directly without passing by an intermediary data-structure. This avoids the need for lazily creating a possibly infinite data structure and _is_ idiomatic since parser combinators are frequently used in functional programming and in idris.

However, trying this out does not work because the idris parser consumes every successful parse. What we want to say is something like this:

```haskell
1 : 2 3 | 3 2
-- should parse to
p1 = try (p2 `seq` p3) <|> (p3 `seq` p2)
```

where `p1`, `p2`, `p3` are the parsers corresponding to each rule number in the grammar. The `try` function allows the parser to set this alternative as an arbitrary backtracking point in case ``p2 \`seq\` p3`` fails. Unfortunately the current implementation of parser combinators in idris does not allow arbitrary backtracking with `try`.

I guess I should have spent more time on it and implement `try` in the standard library but my time is already limited enough as is before I try fixing every single feature I want. Additionally, one could make the argument that arbitrary backtracking is _a bad idea™_ and should not be allowed. An argument that I don’t want to engage in.

As the language evolves and the ecosystem grows I expect issues like this to disappear. Specially after we get proper package management. But for now this part is stuck.

## Day 20

Day 20 is a problem about fitting puzzle pieces together. Part 1 already shows some tricky properties, pieces can be flipped and rotated arbitrarily, which means the search space for this problem is _huge_. Additionally, it turns out you do not need to solve the entire puzzle to figure out the solution, you only need to find the corners.

This part took me a while because of how tricky it is to understand what it means to be a “corner piece” and what it means to “fit together”. This is also a situation when dependent types are not of any use since the solution is purely based on searching the problem space. Nonetheless I solved it, 2 months later, but I did. I was hoping the second part would be less tricky but it turned out to be worse.

Part 2 requires to match a 2D pattern and then count how many tiles do not match the pattern. This is phrased in a very flavorful way in the problem set by saying that you need to "find the monsters" and then "evaluate if the waters are dangerous or not".

This is actually a very hard problem specially if you do it dependently typed. The simple version of it is already difficult: its basically regex matching. This is the same but in 2D. And after detecting the patterns you're not even done because you need to compute a new image out of the detected patterns and then diff the original image with the new one.

This isn't completely impossible engineering, it's difficult but not unsurmountable, what really makes it difficult is integrating dependent types in there.

Again, the goal was to be intuitive and idiomatic. Unfortunately, the intuitive and idiomatic way to do this in Idris greatly suffers from a lack of ergonomics. The intuitive and idiomatic way is to use vectors and matrices and perform operations on them to extract sub-matrices and sub-vectors, compare them and collect them. Once all the patterns have been matched as sub-matrices we need to rebuild a matrix of the correct size and then compare the resulting two matrices. All this slicing, comparing and rebuilding is _extremely tricky_ with dependent types because all sizes need to match and if they don't, you need to provide a proof that ensure that you checked they would match.

Unfortunately, that makes the Idris version needlessly complex compared to a programming language which does not benefit from dependent types. Granted, I could have done, but we would not have seen any benefits from dependent types like we've seen for week 17 (with 4D game of life).

## Day 21 - Allergen Assessment

Unfortunately Day21 is not a day I had time to work on at all. But what happened is pretty symptomatic of what happened with all the problem sets that haven't been solved. Day 21 is about matching a set of constraints with the given data, once again there isn't much that dependent types can do for us here, and this problem has already been solved for the challenge of day 16. I feel like I could work on it and finish it in a reasonable amount of time but doing so wouldn’t result in a new and interesting solution.

The fact that no intuitive and idiomatic implementation using dependent types exists makes this problem quite uninteresting to solve.

## Day 23

Day 23 is about modulo arithmetic, this problem is actually really fun to read and understand, however, once again, there is not much that dependent types can do for us. Part 1 is the same as part 1 except the input is bigger, turning the otherwise interesting problem into a runtime optimisation challenge.

Runtime optimisation challenges can be fun, but Idris2 is not the best language to work with. Since the definitions are so removed from the generated bytecode and the runtime is uncontrollable since it's garbage collected there isn't much you can do (except reducing the algorithmic complexity) to improve the performance of the program.

Other programming languages make those optimisations a lot more palatable but doing them in Idris will again result in unintuitive and non-idiomatic solutions, aka lots of IORefs and mutation.

## Conclusion

It is unfortunate that I didn't finish the 4 exercises mentioned, but after looking at them more
carefully I realize that most of the value of the advent of code challenge has been extracted and
that the remaining exercises would only make the project more
bulky rather than more interesting in its content. The only challenge I feel is a bit unfortunate to leave out is
the one on Day 20. I really hoped that we could get a very
nice abstraction out of it just like we did for Day 17. But handling vectors and matrices is still
a bit unwieldy. What this suggests to me is that there should be more work put into offering an
ergonomic and complete API for vectors and matrices. Likewise for parsers. Finally, some of those
exercises are better experienced as optimisation challenges rather than elegant implementation.
This is something that Idris _ought_ to do well at some point, but does not right now. Thankfully
this ties directly into my current projects which are to describe a type theory that allows for
first class manipulation of programming resources!
