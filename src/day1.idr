module Main

import Data.List
import Data.Strings
import System
import System.File

findSum :  (firstValue : Int) -> List Int -> Maybe Int
findSum firstValue = find (\secondValue => firstValue + secondValue == 2020)

findPair : List Int -> Maybe (Int, Int)
findPair [] = Nothing
findPair (x :: xs) = case findSum x (x :: xs) of
                          Nothing => findPair xs
                          (Just y) => Just (x , y)

findPair' : List Int -> Maybe (Int, Int)
findPair' [] = Nothing
findPair' (x :: xs) = maybe (findPair' xs) (Just . (x,)) (findSum x (x :: xs))

findNumber : List Int -> Maybe Int
findNumber ls = map (uncurry (*)) (findPair ls)

parseFile : String -> Maybe (List Int)
parseFile input = let lines = lines input in
                      traverse parsePositive lines

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let (Just parsed) = parseFile fileContent
  case findNumber parsed of
       Nothing => putStrLn "couldn't find a pair of number that sum to 2020"
       (Just prod) => putStrLn $ "found product" ++ show prod



