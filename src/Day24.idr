module Main

import System
import System.File
import Data.String.Parser
import Data.Strings
import Data.Vect
import Data.SortedSet

count : Foldable f => (a -> Bool) -> f a -> Nat
count g = foldr (\elem => if g elem then S else id) Z

data Direction = W | SE | SW | E | NE | NW

-- first is X axis, second in Y axis (NE-SW) and third is Z axis with (NW-SE)
Coordinate : Type
Coordinate = Vect 3 Int

toCoord : Direction -> Coordinate
toCoord W  = [0, 1, -1]
toCoord SE = [1, -1, 0]
toCoord SW = [1, 0, -1]
toCoord E  = [0, -1, 1]
toCoord NE = [-1, 0, 1]
toCoord NW = [-1, 1, 0]


Board : Type
Board = SortedSet Coordinate

flipCoord : Coordinate -> Board -> Board
flipCoord coord board = if contains coord board
                           then delete coord board
                           else insert coord board

add : Coordinate -> Coordinate -> Coordinate
add = zipWith (+)

walkInstructions : List Direction -> Coordinate
walkInstructions = foldr (add . toCoord) [0,0,0]

solve : List (List Direction) -> Nat
solve instructions =
  let coordinates = map walkInstructions instructions
      flipped = foldr flipCoord empty coordinates
   in length (SortedSet.toList flipped)


parseDirection : Parser Direction
parseDirection = map (const W) (char 'w')
             <|> map (const E) (char 'e')
             <|> map (const SE) (string "se")
             <|> map (const SW) (string "sw")
             <|> map (const NE) (string "ne")
             <|> map (const NW) (string "nw")

parseFile : String -> Either String (List (List Direction))
parseFile input =
  map (map fst) $ traverse (parse (some parseDirection <* eos)) (lines input)

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  let Right parsed = parseFile fileContent
    | Left err => printLn err
  putStrLn ("Solution : " ++ show (solve parsed))
