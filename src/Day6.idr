module Main

import Data.List
import Data.List1
import Data.Strings
import System
import System.File

unionAll : Eq a => (List (List a)) -> List a
unionAll = nub . join

getGroups : String -> List (List (List Char))
getGroups = map (map unpack) . forget . split (== "") . lines

countEverything : String -> Nat
countEverything = sum . map (length . unionAll) . getGroups

partial
main : IO ()
main = do
  [_, filename] <- getArgs
  Right fileContent <- readFile filename
  putStrLn ("total tally union: " ++ show (countEverything fileContent))

